Require Import R.FBool.member R.FBool.included R.FBool.exists R.FBool.forall R.FBool.is_empty R.FDomain.remove R.FDomain.union R.FDomain.add R.FDomain.intersection R.FDomain.difference R.FDomain.filter R.FDomain.partition R.FValue.fold R.FValue.max R.FValue.min R.FValue.size R.Extract.test_extraction R.CD.singleton R.CD.interval R.CD.empty.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(**************************************************************)
(**                 Extraction to bigint                      *)
(**************************************************************)

Extraction Language OCaml. (* au lieu de Ocaml, qui génère un warning *)
Set Extraction AccessOpaque.


Extract Constant structure.min_int => "Big.of_int min_int".
Require Import Coq.extraction.ExtrOcamlZBigInt.

Require Import ExtrOcamlBasic.

Extract Inlined Constant andb => "(&&)".
Extract Inlined Constant orb => "(||)".

Extraction "Extraction/domain_interval_test_bigint.ml" 
R.FBool.is_empty.is_empty 
R.FValue.size.size_domain 
R.FValue.max.max_domain 
R.FValue.min.min_domain
R.CD.create.unsafe_create
R.CD.create.create 
R.CD.values.values 
R.FBool.member.member
R.FBool.included.included 
R.FBool.included.included_optim 
R.FDomain.remove.remove 
R.CD.empty.empty 
R.CD.boolean.boolean 
R.CD.interval.interval 
R.CD.singleton.singleton
R.Prelude.structure.extractionOption
R.FDomain.intersection.intersection
R.FDomain.union.union
R.FDomain.difference.difference
R.FDomain.filter.filter 
R.FDomain.partition.partition 
R.FBool.exists.exist
R.FValue.fold.fold_left_domain 
for_all
min_domain 
max_domain
(*tests*)
small_list_0 small_domain_0 
small_list_1 small_domain_1
small_list_2 small_domain_2
small_list_3 small_domain_3
small_list_4 small_domain_4
big_list_0 big_domain_0 
big_list_1 big_domain_1
big_list_2 big_domain_2
big_list_3 big_domain_3
big_list_4 big_domain_4
sparse_set_list_0 sparse_set_domain_0 
sparse_set_list_1 sparse_set_domain_1
sparse_set_list_2 sparse_set_domain_2
sparse_set_list_3 sparse_set_domain_3
sparse_set_list_4 sparse_set_domain_4
compact_list_0 compact_domain_0 
compact_list_1 compact_domain_1
compact_list_2 compact_domain_2
compact_list_3 compact_domain_3
compact_list_4 compact_domain_4
bigest_domain
test_elem_x test_pos test_ff
.


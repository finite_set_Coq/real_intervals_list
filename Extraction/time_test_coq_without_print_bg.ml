open Domain_interval_test_bigint
   
 
let small_domain = 0;;
let big_domain = 1;;
let compact_domain = 2;;
let sparse_set = 3;;

let nb_function = 16;;
let id_function = ref 0;;
(*L'identifiant d'une fct est sa position dans name_function*)
let name_function =
  [|"is_empty";
    "size";
    "min";
    "max";
 (*   "unsafe_create";*)
    "create";
    "values";
    "member";
    "included";
    "remove";
    "intersection";
     "union" ; 
     "difference" ; 
     "for_all";
     "exist";
     "filter";
     "fold_left"
  |]
let nb_iter = 1000;;       

(** ********************************************************** **)
(**                  For measuring the time                    **)
(** ********************************************************** **)

open Unix

(** *********************************************** **)
(** References and functions for measuring the time **)
(** *********************************************** **)

let current = ref (times ()).tms_utime;;
let total = ref 0.0;;
let res = Array.make_matrix nb_function 5 0.0;;

let measure_time_open () = 
  current := (times ()).tms_utime;;

let measure_time_reset () = 
  print_string "reset\n ";
  total := 0.0;;
(*measure_time_open();; (** RAJOUT **)*)

let measure_time_get () = !total;;

let measure_time_close () = 
  let newc = (times ()).tms_utime in
  total := !total +. (newc -. !current);
  current := newc;;

let measure_time_open () = 
  current := (times ()).tms_utime;;

(** ********************************************************** **)
(**                     Beginning of tests                     **)
(** ********************************************************** **)

let stock_of_domain = Array.make_matrix 4 5 empty;;
let stock_of_list = Array.make_matrix 4 5 [];;


(** ********************** **)
(**       Small domain     **)
(** ********************** **)

(** Ces domaines possèdent moins de 10 valeurs, c'est-à-dire size <= 10. **)

stock_of_list.(small_domain).(0) <- small_list_0 ;;
stock_of_domain.(small_domain).(0) <- small_domain_0 ;;

stock_of_list.(small_domain).(1) <- small_list_1 ;;
stock_of_domain.(small_domain).(1) <- small_domain_1 ;;

stock_of_list.(small_domain).(2) <- small_list_2;;
stock_of_domain.(small_domain).(2) <- small_domain_2;;

stock_of_list.(small_domain).(3) <- small_list_3;;
stock_of_domain.(small_domain).(3) <- small_domain_3;;


stock_of_list.(small_domain).(4) <- small_list_4;;
stock_of_domain.(small_domain).(4) <- small_domain_4;;


(** ********************** **)
(**       Big domain       **)
(** ********************** **)

(** Ces domaines possèdent plus de 100 valeurs, c'est-à-dire size >= 100. **)

stock_of_list.(big_domain).(0) <- big_list_0;;
stock_of_domain.(big_domain).(0) <- big_domain_0;;

stock_of_list.(big_domain).(1) <- big_list_1;;
stock_of_domain.(big_domain).(1) <- big_domain_1;;

stock_of_list.(big_domain).(2) <- big_list_2;;
stock_of_domain.(big_domain).(2) <- big_domain_2;;

stock_of_list.(big_domain).(3) <- big_list_3;;
stock_of_domain.(big_domain).(3) <- big_domain_3;;

stock_of_list.(big_domain).(4) <- big_list_4;;  
stock_of_domain.(big_domain).(4) <- big_domain_4;;

(** ********************** **)
(**       Sparse-set       **)
(** ********************** **)

(** Ces domaines possèdent des valeurs éparpillées (size = 100) **)

stock_of_list.(sparse_set).(0) <- sparse_set_list_0;;
stock_of_domain.(sparse_set).(0) <- sparse_set_domain_0;;

stock_of_list.(sparse_set).(1) <- sparse_set_list_1;;
stock_of_domain.(sparse_set).(1) <- sparse_set_domain_1;;

stock_of_list.(sparse_set).(2) <- sparse_set_list_2;;
stock_of_domain.(sparse_set).(2) <- sparse_set_domain_2;;

stock_of_list.(sparse_set).(3) <- sparse_set_list_3;;
stock_of_domain.(sparse_set).(3) <- sparse_set_domain_3;;

stock_of_list.(sparse_set).(4) <- sparse_set_list_4;;
stock_of_domain.(sparse_set).(4) <- sparse_set_domain_4;;


(** ********************** **)
(**    Compact domain      **)
(** ********************** **)
(** Ces domaines possèdent peu d'espaces (size = 100) **)

stock_of_list.(compact_domain).(0) <- compact_list_0;;
stock_of_domain.(compact_domain).(0) <- compact_domain_0;;

stock_of_list.(compact_domain).(1) <- compact_list_1;;
stock_of_domain.(compact_domain).(1) <- compact_domain_1;;

stock_of_list.(compact_domain).(2) <- compact_list_2;;
stock_of_domain.(compact_domain).(2) <- compact_domain_2;;

stock_of_list.(compact_domain).(3) <- compact_list_3;;
stock_of_domain.(compact_domain).(0) <- compact_domain_3;;

stock_of_list.(compact_domain).(4) <- compact_list_4;;
stock_of_domain.(compact_domain).(4) <- compact_domain_4;;



(** ********************** **)
(**  Fonctions génériques  **)
(** ********************** **)

let print_list_with_notations l =
  let rec print_list l = match l with
    |[] -> ""
    |e::f -> string_of_int e ^ ";" ^ print_list f in
  "[" ^ print_list l ^ "]\n";;

(** ************* **)
(**  ... -> bool  **)
(** ************* **)

let test_time_bool f nb_iter carac_domain =
  print_string("Fonction numéro : " ^ string_of_int(!id_function) ^ "\n");
  measure_time_reset ();
  for i = 1 to nb_iter do
    for j = 0 to 4 do
      f (stock_of_domain.(carac_domain).(j));
    done;
  done;
  measure_time_close ();
  let total = measure_time_get () in
res.(!id_function).(carac_domain) <- total /. (float_of_int (5*nb_iter))
(*res.(!id_function).(carac_domain) <- total*)
;;

(** ********** **)
(**  ... -> Z  **)
(** ********** **)

let test_time_int f nb_iter carac_domain =
  print_string("Fonction numéro : " ^ string_of_int(!id_function) ^ "\n");
  measure_time_reset ();
  for i = 1 to nb_iter do
    for j = 0 to 4 do
      f (stock_of_domain.(carac_domain).(j));
    done;
  done;
  measure_time_close ();
  let total = measure_time_get () in
res.(!id_function).(carac_domain) <- total /. (float_of_int (5*nb_iter))
(*res.(!id_function).(carac_domain) <- total*)
;;

(** *************** **)
(**  ... -> list Z  **)
(** *************** **)

let test_time_list_Z f nb_iter carac_domain =
  print_string("Fonction numéro : " ^ string_of_int(!id_function) ^ "\n");
  measure_time_reset ();
  for i = 1 to nb_iter do
    for j = 0 to 4 do
     f (stock_of_domain.(carac_domain).(j));
    done;
  done;
  measure_time_close ();
  let total = measure_time_get () in
res.(!id_function).(carac_domain) <- total /. (float_of_int (5*nb_iter))
(*res.(!id_function).(carac_domain) <- total*);;

(** ********** **)
(**  ... -> t  **)
(** ********** **)

let test_time_t f nb_iter carac_domain =
  print_string("Fonction numéro : " ^ string_of_int(!id_function) ^ "\n");
  measure_time_reset ();
  for i = 1 to nb_iter do
    for j = 0 to 4 do
    f (stock_of_domain.(carac_domain).(j));
    done;
  done;
  measure_time_close ();
  let total = measure_time_get () in
res.(!id_function).(carac_domain) <- total /. (float_of_int (5*nb_iter))
(*res.(!id_function).(carac_domain) <- total*)
;;

(** ********************************* **)
(** PART A : Operations for elt_list  **)
(** ********************************* **)

(* is_empty_list,size_domain, max_domain...,is_singleton,get_value : get_min est déjà en temps cte *)

(** *************** **)
(**     is_empty    **)
(** *************** **)

test_time_bool is_empty nb_iter small_domain;;
test_time_bool is_empty nb_iter big_domain;;
test_time_bool is_empty nb_iter compact_domain;;
test_time_bool is_empty nb_iter sparse_set;;

(** *************** **)
(**       size      **)
(** *************** **)
id_function := 1;;
test_time_int size nb_iter small_domain;;
test_time_int size nb_iter big_domain;;
test_time_int size nb_iter compact_domain;;
test_time_int size nb_iter sparse_set;;

(** *************** **)
(**       min       **)
(** *************** **)
id_function := 2;;
test_time_int min_domain nb_iter small_domain;;
test_time_int min_domain nb_iter big_domain;;
test_time_int min_domain nb_iter compact_domain;;
test_time_int min_domain nb_iter sparse_set;;

(** *************** **)
(**       max       **)
(** *************** **)
id_function := 3;;
test_time_int max_domain nb_iter small_domain;;
test_time_int max_domain nb_iter big_domain;;
test_time_int max_domain nb_iter compact_domain;;
test_time_int max_domain nb_iter sparse_set;;

(** *************** **)
(**  unsafe_create  **)
(** *************** **)

let test_time_list_t f nb_iter carac_domain =
  print_string("Fonction numéro : " ^ string_of_int(!id_function) ^ "\n");
  measure_time_reset ();
  for i = 1 to nb_iter do
    for j = 0 to 4 do
     f (stock_of_list.(carac_domain).(j));
    done;
  done;
  measure_time_close ();
  let total = measure_time_get () in
res.(!id_function).(carac_domain) <- total /. (float_of_int (5*nb_iter))
(*res.(!id_function).(carac_domain) <- total*)
;;
(*
id_function := 4;;
test_time_list_t unsafe_create nb_iter small_domain;;
test_time_list_t unsafe_create nb_iter big_domain;;
test_time_list_t unsafe_create nb_iter compact_domain;;
test_time_list_t unsafe_create nb_iter sparse_set;;
 *)
(** ************** **)
(**     create     **)
(** ************** **)

id_function := 4;;
test_time_list_t create nb_iter small_domain;;
test_time_list_t create nb_iter big_domain;;
test_time_list_t create nb_iter compact_domain;;
test_time_list_t create nb_iter sparse_set;;

(** ******************************** **)
(** PART C : Classical operations    **)
(** ******************************** **)


(** *************** **)
(**      values     **)
(** *************** **)
id_function := 5;;
test_time_list_Z values nb_iter small_domain;;
test_time_list_Z values nb_iter big_domain;;
test_time_list_Z values nb_iter compact_domain;;
test_time_list_Z values nb_iter sparse_set;;

(** *************** **)
(**      member     **)
(** *************** **)

id_function := 6;;
test_time_bool (member test_elem_x) nb_iter small_domain;;
test_time_bool (member test_elem_x) nb_iter big_domain;;
test_time_bool (member test_elem_x) nb_iter compact_domain;;
test_time_bool (member test_elem_x) nb_iter sparse_set;;

(**
(** **************** **)
(**     included     **)
(** **************** **)  
id_function := 7;;
test_time_bool (included (stock_of_domain.(small_domain).(3))) nb_iter small_domain;;
test_time_bool (included (stock_of_domain.(big_domain).(3))) nb_iter big_domain;;
test_time_bool (included (stock_of_domain.(compact_domain).(3))) nb_iter compact_domain;;
test_time_bool (included (stock_of_domain.(sparse_set).(3))) nb_iter sparse_set;;
**)

(** **************** **)
(**     included_optim  **)
(** **************** **)  
id_function := 7;;
test_time_bool (included_optim (stock_of_domain.(small_domain).(3))) nb_iter small_domain;;
test_time_bool (included_optim (stock_of_domain.(big_domain).(3))) nb_iter big_domain;;
test_time_bool (included_optim (stock_of_domain.(compact_domain).(3))) nb_iter compact_domain;;
test_time_bool (included_optim (stock_of_domain.(sparse_set).(3))) nb_iter sparse_set;;
                  
(** **************** **)
(**      remove      **)
(** **************** **)
id_function := 8;; 
test_time_t (remove test_elem_x) nb_iter small_domain;;
test_time_t (remove test_elem_x) nb_iter big_domain;;
test_time_t (remove test_elem_x) nb_iter compact_domain;;
test_time_t (remove test_elem_x) nb_iter sparse_set;;

(** **************** **)
(**      exists      **)
(** **************** **)
id_function := 18;;

(** ******************************** **)
(** PART D : **)
(** ******************************** **)

(** ******************************** **)
(** PART E : Enumeration             **)
(** ******************************** **)

(** ******************** **)
(**   fold_right_inter   **)
(** ******************** **)
id_function := 19;;

(** **************** **)
(**    list_iter     **)
(** **************** **)  
id_function :=20;;
(** ***************** **)
(**   interval_iter   **)
(** ***************** **)
id_function := 21;;

(** **************** **)
(**    fold_inter    **)
(** **************** **)
id_function := 22;;


(** **************** **)
(**    fold_right    **)
(** **************** **)
id_function := 23;;

(** **************** **)
(**     intersection **)
(** **************** **)  
id_function := 9;;
test_time_t (intersection (stock_of_domain.(small_domain).(2))) nb_iter small_domain;;
test_time_t (intersection (stock_of_domain.(big_domain).(2))) nb_iter big_domain;;
test_time_t (intersection (stock_of_domain.(compact_domain).(2))) nb_iter compact_domain;;
test_time_t (intersection (stock_of_domain.(sparse_set).(2))) nb_iter sparse_set;;
 
(** **************** **)
(**     union        **)
(** **************** **)  
id_function := 10;;
test_time_t (union (stock_of_domain.(small_domain).(2))) nb_iter small_domain;;
test_time_t (union (stock_of_domain.(big_domain).(2))) nb_iter big_domain;;
test_time_t (union (stock_of_domain.(compact_domain).(2))) nb_iter compact_domain;;
test_time_t (union (stock_of_domain.(sparse_set).(2))) nb_iter sparse_set;;

(** **************** **)
(**     difference    **)
(** **************** **)  
id_function := 11;;
test_time_t (difference (stock_of_domain.(small_domain).(2))) nb_iter small_domain;;
test_time_t (difference (stock_of_domain.(big_domain).(3))) nb_iter big_domain;;
test_time_t (difference (stock_of_domain.(compact_domain).(0))) nb_iter compact_domain;;
test_time_t (difference (stock_of_domain.(sparse_set).(2))) nb_iter sparse_set;;

(** **************** **)
(**   for_all        **)
(** **************** **)
id_function := 12;;
test_time_bool (for_all test_pos) nb_iter small_domain;;
test_time_bool (for_all test_pos) nb_iter big_domain;;
test_time_bool (for_all test_pos) nb_iter compact_domain;;
test_time_bool (for_all test_pos) nb_iter sparse_set;;

(** **************** **)
(**   exist          **)
(** **************** **)
id_function := 13;;
test_time_bool (exist test_pos) nb_iter small_domain;;
test_time_bool (exist test_pos) nb_iter big_domain;;
test_time_bool (exist test_pos) nb_iter compact_domain;;
test_time_bool (exist test_pos) nb_iter sparse_set;;

(** **************** **)
(**     filter       **)
(** **************** **)  
id_function := 14;;
test_time_t (filter test_pos)  nb_iter small_domain;;
test_time_t (filter test_pos) nb_iter big_domain;;
test_time_t (filter test_pos) nb_iter compact_domain;;
test_time_t (filter test_pos)  nb_iter sparse_set;;

(** **************** **)
(**     fold_left    **)
(** **************** **)  
id_function := 15;;
test_time_int (fold_left_domain test_ff Big.zero)  nb_iter small_domain;;
test_time_int (fold_left_domain test_ff Big.zero) nb_iter big_domain;;
test_time_int (fold_left_domain test_ff Big.zero) nb_iter compact_domain;;
test_time_int (fold_left_domain test_ff Big.zero)  nb_iter sparse_set;;

(** ********************************************************** **)
(**                            Layout                          **)
(** ********************************************************** **)

let nb_star = 156;;
let nb_space_tot = 43;;
let nb_function_A = 3;;
let nb_function_B = 6;;
let nb_function_C = 10;;
let nb_function_D = 0;;
let nb_function_E = 5;;
let nb = ref nb_function_A;;

let ligne nb_star =
  for i = 1 to nb_star do
     print_string ("*");
  done;
  print_string("\n");;

let espace nb_space =
  for i = 1 to nb_space do
     print_string (" ");
  done;;

let espace_compl nb_space_tot len_string =
  let nb = nb_space_tot - len_string in 
  for i = 1 to nb do
     print_string (" ");
  done;;

let centrer nb_space_tot string =
  let nb = nb_space_tot - String.length(string) in 
  for i = 1 to (nb/2) do
     print_string (" ");
  done;
  print_string(string);
  espace_compl nb_space_tot (String.length(string)+(nb/2)+1);;
  (*for i = 1 to (nb/2) do
     print_string (" ");
  done;;*)

let print_res id_function =
  for i = 0 to 3 do
    if (res.(!id_function).(i) = 0.0) then
      begin
        centrer 28 ("_");
        print_string ("*");
      end
    else
      begin
        centrer 28 (string_of_float(res.(!id_function).(i)));
        print_string ("*");
      end
  done;
  print_string ("\n");;

let print_entete =
  print_string("\n\n");
  ligne nb_star;                                                                              (*En_tête*)
  espace nb_space_tot;
  print_string ("*");
  centrer 28 "Small domain";
  print_string ("*");
  centrer 28 "Big domain";
  print_string ("*");
  centrer 28 "Compact domain";
  print_string ("*");
  centrer 28 "Sparse-set";
  print_string ("*\n");
  ligne nb_star;                                                                              (*2ème ligne*)
  print_string("Number of iterations : " ^ string_of_int(nb_iter));
  espace_compl nb_space_tot (String.length ("Number of iterations : " ^ string_of_int(nb_iter)));
  print_string ("*\n");
  ligne nb_star;;

let print_function borne1 borne2 =
  for i = borne1 to (borne1+borne2-1) do
    centrer (nb_space_tot+1) (name_function.(i));
    print_string ("*");
    print_res (ref i);
  done;
  ligne nb_star;;
  (*
  let i = ref 0;
  while (!i<nb_function) do
    centrer (nb_space_tot+1) (name_function.(i));
    print_string ("*");
    print_res !i;
    i := !i + 1;
  done;
  ligne nb_star;;
  *)

  
(** Création du fichier .csv *)

let file = open_out "res_bigint.csv" in
output_string file "fonction;big_int/small;big_int/big;big_int/compact;big_int/sparse\n";
for i = 0 to nb_function - 1 do
output_string file (name_function.(i)^";"^
		    string_of_float(res.(i).(0))^";"^
                        string_of_float(res.(i).(1))^";"^
                          string_of_float(res.(i).(2))^";"^
                            string_of_float(res.(i).(3))^"\n");
done;
close_out file;;


(** Affichage du tableau **)

print_entete;
print_function 0 nb_function;;

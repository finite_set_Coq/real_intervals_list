Require Import R.Prelude.structure R.CD.create R.CD.values R.CD.empty R.CD.interval R.CD.boolean.

Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.


Definition  create_big_list b1 b2 := 
enum_and_conc ((b1, b2), []).

(** ********************** **)
(**       Small domain     **)
(** ********************** **)

(** These domains have less than 10 elements, i.e  size <= 10. **)

Definition small_list_0 := [-10].
Definition small_domain_0 := extractionOption _ (interval (-10) (-10)) empty.

Definition small_list_1 := [0 ; 1].
Definition small_domain_1 := boolean.

Definition small_list_2 := [1;2;3;4;5;6;7;8;9;10].
Definition small_domain_2 := extractionOption _ (interval 1 10) empty.

Definition small_list_3 := [5;9;6;3;1;5;8;9;7;5].
Definition small_domain_3 :=  create small_list_3.

Definition small_list_4 := [5;6;2;1;4;7;7;7;4;4;1;852].
Definition small_domain_4 :=  create small_list_4.

(** ********************** **)
(**       Big domain       **)
(** ********************** **)

(** These domains contain more than 100 values, ie size >= 100. **)

Definition big_list_0 := create_big_list 1 1000.
Definition big_domain_0 := create big_list_0.

Definition big_list_1 := (create_big_list (-50) 0) ++ (create_big_list 20 150).
Definition big_domain_1 :=  create big_list_1.

Definition big_list_2 := (create_big_list 20 1500) ++ (create_big_list (-150) 0).
Definition big_domain_2 := create big_list_2.

Definition big_list_3 := (create_big_list 20 150) ++ (create_big_list (-50) 0) ++ 
(create_big_list 220 300) ++ (create_big_list 3 10) ++ (create_big_list 166 215) ++ (create_big_list (-150) (-70)).
Definition big_domain_3 := create big_list_3.

Definition big_list_4 := (create_big_list (-60) 50) ++ (create_big_list 220 300) ++ (create_big_list 68 215) ++ (create_big_list (-350) (-70)).
Definition big_domain_4 := create big_list_4.

(** ********************** **)
(**       Sparse-set       **)
(** ********************** **)

(**  here size = 100 **)

Definition sparse_set_list_0 := (create_big_list (-1000) (-951)) ++
                          (create_big_list 0 0) ++
                            (create_big_list 299 300) ++
                              (create_big_list 3 10) ++
                                (create_big_list 201 215) ++
                                  (create_big_list 1000 1024).
Definition sparse_set_domain_0 := create sparse_set_list_0.

Definition sparse_set_list_1 := (create_big_list (-1900) (-1881)) ++
                         (create_big_list (-150) (-121)) ++
                           (create_big_list 2954 3000) ++
                            (create_big_list 215 215)++
                             (create_big_list 10 10)++
                            (create_big_list 215 215)++
                                 (create_big_list (-70) (-70)).
Definition sparse_set_domain_1 := create sparse_set_list_1.

Definition sparse_set_list_2 := (create_big_list 1451 1500)++(create_big_list (-149) (-50)).
Definition sparse_set_domain_2 := create sparse_set_list_2.

Definition sparse_set_list_3 := (create_big_list 2100 2100)++
                          (create_big_list 0 0)++
                            (create_big_list (-600) (-600))++
                              (create_big_list 10 10)++
                                (create_big_list 5701 5795)++
                                  (create_big_list (-97) (-97)).
Definition sparse_set_domain_3 := create sparse_set_list_3.

Definition sparse_set_list_4 :=(create_big_list (-600) (-551))++
                          (create_big_list 253 300)++
                            (create_big_list 68 68)++
                              (create_big_list (-70) (-70)).
Definition sparse_set_domain_4 := create sparse_set_list_4.

(** ********************** **)
(**    Compact domain      **)
(** ********************** **)
(** These domains contain very few holes (size = 100) **)

Definition compact_list_0 := create_big_list 1 100.
Definition compact_domain_0 := create compact_list_0.


Definition compact_list_1 := (create_big_list (-50)(-1))++(create_big_list 1 50)++(create_big_list (-50)(-1)).
Definition compact_domain_1 := create compact_list_1.

Definition compact_list_2 := (create_big_list 21 50)++(create_big_list (-50) 1)++(create_big_list 61 80).
Definition compact_domain_2 := create compact_list_2.

Definition compact_list_3 := (create_big_list 10 10)++(create_big_list (-100) (-2)).
Definition compact_domain_3 := create compact_list_3.

Definition compact_list_4 := (create_big_list (-160) (-101))++(create_big_list (-20) (-1))++(create_big_list (-50) (-31)).
Definition compact_domain_4 := create compact_list_4.

Definition bigest_domain := 
let ocaml_min_int := -4611686018427387904 in
let ocaml_max_int := 4611686018427387903 in
extractionOption _ (interval ocaml_min_int ocaml_max_int) empty.

Definition minint_mintintplus2_domain :=
let ocaml_min_int := -4611686018427387904 in
let ocaml_min_intplus2 := ocaml_min_int + 2 in
extractionOption _ (interval ocaml_min_int ocaml_min_intplus2) empty.

(** Some more tests
Eval compute in 
(size (operation_solver_remove.remove  0 bigest_domain)).

Eval compute in
(union boolean minint_mintintplus2_domain).

Eval compute in
(union minint_mintintplus2_domain boolean).

Eval compute in
(union boolean compact_domain_4).

Eval compute in
(union compact_domain_4 boolean
).

Eval compute in
(size (union boolean minint_mintintplus2_domain)).

Eval compute in 
(difference bigest_domain boolean).

Eval compute in 
(difference minint_mintintplus2_domain (singleton (-4611686018427387904))).

Eval compute in 
(let ocaml_max_int := 4611686018427387903  in
(difference bigest_domain (extractionOption _ (interval ocaml_max_int  ocaml_max_int) empty)
)).
**)


(***************)
Definition test_elem_x := 10.
Definition test_pos x := x >? 100.
Definition test_ff x y  := x + y.


(***************)


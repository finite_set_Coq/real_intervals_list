Require Import List ZArith Lia.
Import ListNotations.
Open Scope Z_scope.

(*********************************************************************)
(** *               Extension of the Standard Library                *)
(*********************************************************************)
(*
       A : Lemmas about Sorted, In, NoDup and Last
       B : Analogy with Permutation: SameElt
       C : Lemmas about min_list and max_list
*)

(********************************************************)
(** ** A : Lemmas about Sorted, In, NoDup and Last      *)
(********************************************************)

Inductive Sorted : list Z -> Prop :=
| Sorted_nil : Sorted nil
| Sorted_singleton : forall e, Sorted (cons e nil)
| Sorted_cons : forall e h t, Sorted (h :: t) -> e <= h -> 
                              Sorted (e :: h :: t).

(** Note : NoDup, In and last are defined in the librairy named List: **)

(** https://coq.inria.fr/library/Coq.Lists.List.html **)

(*********************************)
(** *** Properties of Sorted     *)
(*********************************)

Lemma decreasingSort : forall a l, 
Sorted (a :: l) -> Sorted l.
Proof.
induction l.
- intro H. apply Sorted_nil.
- intro H. inversion H. assumption.
Qed.

Lemma simplSorted : forall a a0 l, 
Sorted (a :: a0 :: l) -> Sorted (a :: l).
Proof.
induction l; intro H.
- apply Sorted_singleton.
- apply Sorted_cons. inversion H. inversion H2. apply H7.
  inversion H. inversion H2. lia.
Qed.

Lemma sortedCons : forall l a, 
Sorted l -> (forall x, In x l -> a <= x) -> Sorted (a::l).
Proof.
induction l;intros z Hyp1 Hyp2.
- constructor.
- constructor. exact Hyp1. apply Hyp2. simpl. tauto.
Qed.

(******************************)
(** *** Properties of In      *)
(******************************)

Lemma simplNeqIn : forall x a (l:list Z), 
~ In x (a :: l) -> ~ In x l.
Proof.
induction l;intro H.
- apply in_nil.
- unfold not. intro. unfold not in H. apply H. simpl in H0. 
  decompose [or] H0; simpl; right.
     * left;assumption.
     * right;assumption.
Qed.

Lemma simplIn : forall x0 a (l:list Z), 
In x0 l -> In x0 (a :: l).
Proof.
induction l;intro H.
- contradiction.
- simpl in H. decompose [or] H;simpl; right.
     * left;assumption.
     * right;assumption.
Qed.

Lemma trivial2 : forall (a x0:Z) l, 
In x0 (a :: l) ->  x0 <> a -> In x0 l.
Proof.
induction l; intros H1 H2.
- simpl in H1. decompose [or] H1.
   + lia.
   + contradiction.
- simpl in H1;simpl. decompose [or] H1.
   + lia.
   + left;assumption.
   + right;assumption.
Qed.

Lemma differenceIn : forall (a b :Z) l, 
~ In a l -> In b l -> a<>b.
Proof.
intros.
elim (dec_eq a b); intro Heq. subst. 
  + contradiction. 
  + auto.
Qed.

(*********************************)
(** *** Properties of NoDup      *)
(*********************************)

Lemma decreasingNoDup : forall a (l:list Z), 
NoDup (a :: l) -> NoDup l.
Proof.
induction l; intro Hyp.
- apply NoDup_nil.
- inversion Hyp. assumption.
Qed.

Lemma simplNoDup : forall (a a0 : Z) l, 
NoDup (a :: a0 :: l) -> NoDup (a :: l).
Proof.
induction l; intro H.
- apply NoDup_cons. apply in_nil. apply NoDup_nil.
- inversion H. inversion H3. apply NoDup_cons.  
     * apply simplNeqIn with (x:=a) (a:=a0) in H2;assumption.
     * assumption.
Qed.

Lemma NoDup_inegality : forall (x y : Z) l, 
NoDup (x :: y :: l) -> x <> y.
Proof.
intros x y l HypNoDup.
assert({x = y} + {~ x = y}) as Hyp_dec by (apply Z.eq_dec).
destruct Hyp_dec.
  + subst. inversion HypNoDup. simpl in H1. tauto.
  + assumption.
Qed.

(*********************************)
(** *** Properties of Last       *)
(*********************************)

Lemma decreasingLast : forall (l:list Z) a y, l<> nil -> 
last (a :: l) y= last l y.
Proof.
induction l;intros z y Hyp.
- contradiction.
- auto.
Qed.

Lemma lastCons : forall A (l: list A) y z0,
last (z0::l) y = last l z0.
Proof.
induction l.
+ auto.
+ intros.
  replace (last (z0 :: a :: l) y) with (last (a :: l) y) ; auto.
  rewrite IHl . rewrite IHl. reflexivity.
Qed.

(***************************)
(** *** Major theorems     *)
(***************************)

Theorem th1 : forall l (a x0 :Z), 
NoDup (a :: l)-> In x0 l ->  a<>x0.
Proof.
intros.
inversion_clear H.
elim (dec_eq x0 a); intro Heq.
+ rewrite Heq in H0. contradiction.
+ auto.
Qed.

Theorem th2 : forall l a, 
Sorted (a :: l) -> forall x : Z, In x l -> a <= x.
Proof.
induction l; intros a0 H1 x H2.
- contradiction.
- simpl in H2. decompose [or] H2.
  + inversion H1. subst. auto.
  + apply IHl; auto. apply simplSorted in H1 ; assumption. 
Qed.

(************************************************)
(** **  B : Analogy with Permutation: SameElt   *)
(************************************************)

Definition SameElt l1 l2 : Prop :=
forall (x : Z), In x l1 <-> In x l2.

Lemma SameElt_nil : SameElt nil nil.
Proof.
unfold SameElt. intro x. split;intro H; assumption.
Qed.

Lemma SameElt_skip : forall (x : Z) (l l' : list Z), 
SameElt l l' -> SameElt (x :: l) (x :: l').
Proof.
intros x l l' H.
unfold SameElt. intro x0. split;intro H2.
- unfold SameElt in H. simpl in H2. decompose [or] H2.
      * unfold In. left. assumption.
      * apply in_cons. apply H. assumption.
- unfold SameElt in H. simpl in H2. decompose [or] H2.
      * unfold In. left. assumption.
      * apply in_cons. apply H. assumption.
Qed.

Lemma SameElt_swap : forall (x y : Z) (l : list Z), 
SameElt (y :: x :: l) (x :: y :: l).
Proof.
intros x y l. unfold SameElt. intro x0. split;intro H. 
- unfold In in H. decompose [or] H;unfold In.
      + right. left. assumption.
      + left. assumption.
      + right. right. assumption.  
- unfold In in H. decompose [or] H;unfold In.
      + right. left. assumption.
      + left. assumption.
      + right. right. assumption. 
Qed.

Lemma SameElt_trans : forall l l' l'' : list Z, 
SameElt l l' -> SameElt l' l'' -> SameElt l l''.
Proof.
intros l l' l'' H1 H2.
unfold SameElt in H1. unfold SameElt in H2. unfold SameElt.
intro x. split;intro H. 
    - apply H2. apply H1. assumption.
    - apply H1. apply H2. assumption.
Qed.

Lemma SameElt_sym : forall l1 l2, 
SameElt l1 l2 <-> SameElt l2 l1.
Proof.
intros l1 l2. split;intro H;unfold SameElt;unfold SameElt in H;intro x;
split;intro H2;apply H; assumption.
Qed.

Lemma decreasingSameElt : forall (s l : list Z) (z : Z),
  NoDup (z :: s) -> NoDup (z :: l) ->
  SameElt (z :: s) (z :: l) -> SameElt s l.
  Proof.
  unfold SameElt. intros s l z Hnozs Hnozl Hyp. inversion_clear Hnozs. 
  inversion_clear Hnozl. split ; intro Hinx. 
  + assert (x<>z) by (intro Hpb ; apply H ;  subst ;  assumption).
    assert (List.In x (z::s)) as Hyp2 by (simpl ; tauto).
    apply Hyp in Hyp2. destruct Hyp2. subst; contradiction. assumption.
  + assert (x<>z) by (intro Hpb ; apply H1 ;  subst ;  assumption).
    assert (List.In x (z::l)) as Hyp2 by (simpl ; tauto).
    apply Hyp in Hyp2. destruct Hyp2. subst; contradiction. assumption.
Qed.

Lemma SameElt_head_diff : forall a z s l, a <> z ->
NoDup (a :: s) -> NoDup (z :: l) ->
SameElt s l -> ~ SameElt (a :: s) (z :: l).
Proof.
unfold SameElt. 
intros a z s l HypDiff NoDup_s NoDup_l HypSame Hyp1.
assert(List.In a (a :: s) <-> List.In a (z :: l)) as Hyp_a 
  by apply Hyp1 with (x:=a).
destruct Hyp_a as [Hyp_a_1 Hyp_a_2].
simpl in Hyp_a_1. simpl in Hyp_a_2.
assert({List.In a l} + {~ List.In a l}) as H_dec
  by (apply in_dec;apply Z.eq_dec).
destruct H_dec.
  + apply NoDup_cons_iff in NoDup_s.
    destruct NoDup_s. apply HypSame in i. contradiction.
  + assert(z = a \/ List.In a l) as Hyp2 
      by (apply Hyp_a_1;tauto).
    decompose [or] Hyp2.
      - rewrite H in HypDiff. contradiction.
      - contradiction.
Qed.
        
Lemma SameElt_head_diff_Sorted : forall a z s l, a <> z ->
NoDup (a :: s) -> NoDup (z :: l) ->
Sorted (a :: s) -> Sorted (z :: l) ->
~ SameElt s l -> ~ SameElt (a :: s) (z :: l).
Proof.
intros. intro Hyp. inversion_clear H0. inversion_clear H1.
pose proof (decreasingSort _ _ H2) as Hs.
pose proof (decreasingSort _ _ H3) as Hl.
assert (List.In z s) as HHz.
      assert (List.In z (a::s)) by (apply (Hyp z); simpl; tauto).
      destruct H1 . contradiction . assumption.
    assert (List.In a l) as HHa.
      assert (List.In a (z::l)) by (apply (Hyp a); simpl; tauto).
      destruct H1 .  subst ; contradiction. assumption.
    pose proof (th2 _ _ H2 z HHz) .
    pose proof (th2 _ _ H3 a HHa) . lia.
Qed.

Lemma SameElt_dec : forall s s', 
Sorted s -> Sorted s' ->
NoDup s -> NoDup s' ->
{SameElt s s'} + {~ SameElt s s'}.
Proof.
induction s.
  + intros s' HypSort_s HypSort_s' HypNoDup_s HypNoDup_s'.
    case_eq(s');intros.
     * left. apply SameElt_nil.
     * right. unfold SameElt. intro HypF.
       assert(List.In z [] <-> List.In z (z :: l)) as HypZ
         by apply HypF. simpl in HypZ. tauto.
  + intros s' HypSort_s HypSort_s' HypNoDup_s HypNoDup_s'.
    case_eq(s');intros.
     * right. unfold SameElt. intro HypF.
       assert(List.In a (a :: s) <-> List.In a []) as HypA
         by apply HypF. simpl in HypA. tauto.
     * destruct (Z.eq_dec a z).
         - subst. assert({SameElt s l} + {~ SameElt s l}) 
           as HypDec_List by (apply IHs;             
                 apply decreasingSort in HypSort_s;
                 apply decreasingSort in HypSort_s';
                 apply decreasingNoDup in HypNoDup_s;
                 apply decreasingNoDup in HypNoDup_s';
                 assumption).
           destruct HypDec_List. 
              left. apply SameElt_skip;assumption.
              right. intro Hyp. 
              pose proof (decreasingSameElt _ _ _ HypNoDup_s HypNoDup_s' Hyp).
              contradiction.
         - subst.
           assert({SameElt s l} + {~ SameElt s l}) 
            as HypDec_List by (apply IHs;
                 apply decreasingSort in HypSort_s;
                 apply decreasingSort in HypSort_s';
                 apply decreasingNoDup in HypNoDup_s;
                 apply decreasingNoDup in HypNoDup_s';
                 assumption).
           destruct HypDec_List;subst.
           right. apply SameElt_head_diff;assumption.
           right. apply SameElt_head_diff_Sorted;assumption.
Defined.

(**********************************************************)
(** **   C : Lemmas about min_list and max_list           *)
(**********************************************************)

(*********************************)
(** *** Properties of min_list   *)
(*********************************)

Fixpoint min_list (val_def : Z) (l :list Z) := match l with
|nil => val_def
|cons t nil => t
|cons t q => Z.min t (min_list val_def q)
end.

Lemma rewriteIn : forall l a z (x:Z), 
In x (z :: a :: l) -> a = x \/ In x (z :: l) .
Proof.
simpl ; intros ; tauto.
Qed.

Lemma mini_sortedList : forall l a, 
Sorted (a::l) -> forall x, In x (a::l) -> a <= x.
Proof.
induction l;intros z Hyp1 x Hyp2.
- simpl in Hyp2. decompose [or] Hyp2;(lia||contradiction).
- apply rewriteIn in Hyp2. decompose [or] Hyp2.
    + inversion Hyp1. lia.
    + apply IHl. apply simplSorted in Hyp1. assumption. assumption.
Qed.

Lemma decreasingMin_list : forall l a c y, 
c<=a -> min_list y (c :: a :: l) = min_list y (c :: l).
Proof.
induction l;intros.
- simpl. unfold Z.min. case_eq(c ?= a);intro;(auto||congruence).
- simpl. assert(Z.min c a0 = c) as Hyp by (apply Z.min_l;lia).   
  case_eq(l);intros;rewrite Z.min_assoc;rewrite Hyp;reflexivity.
Qed.

Lemma mini_sortedList_min_list : 
forall l y c, Sorted (c::l) -> min_list y (c :: l) = c.
Proof.
induction l;intros y c Hyp1.
- auto.
- assert(forall x, In x (c::a::l) -> c <= x) 
     by (apply mini_sortedList with (a:=c) (l:=a::l); assumption).
  assert(min_list y (c :: l) = c) 
    by (apply IHl; apply simplSorted in Hyp1; assumption).
  inversion Hyp1.
  rewrite  decreasingMin_list;assumption.
Qed.

Lemma mini_def : forall l y x, In x l -> min_list y l <= x.
Proof. 
induction l;intros y x Hyp.
- simpl in Hyp. contradiction.
- case_eq(l);intro.
    + simpl in Hyp. decompose [or] Hyp. 
          * simpl. lia.
          * subst. contradiction.
    + intros. simpl in Hyp. decompose [or] Hyp.
          * simpl. subst. apply Z.le_min_l.          
          * subst. simpl. simpl in IHl. 
            apply Z.le_trans with (m:=  match l0 with
                                     | [] => z
                                     | _ :: _ => Z.min z (min_list y l0)
                                   end). 
            apply Z.le_min_r. apply IHl with (y:=y). 
            simpl in H0. tauto.
Qed.

(*********************************)
(** *** Properties of max_list   *)
(*********************************)

Fixpoint max_list (val_def : Z) (l :list Z) := match l with
|nil => val_def
|cons t nil => t
|cons t q => Z.max t (max_list val_def q)
end.

Lemma decreasingMax_list : forall l a c y, Sorted(c :: a :: l) -> 
max_list y (c :: a :: l) = max_list y (a :: l).
Proof.
induction l;intros.
- simpl. inversion H. apply Z.max_r;assumption.
- simpl. assert(Z.max c a0 = a0) as Hyp.
  inversion H. apply Z.max_r. assumption. rewrite Z.max_assoc. 
  rewrite Hyp;reflexivity.
Qed.

Lemma maxi_sortedList_max_list : forall l y,
Sorted l -> max_list y l = last l y.
Proof.
induction l;intros y Hyp.
- auto.
- case_eq(l);intro z.
    * auto.
    * intros q Hyp2.
      rewrite decreasingLast. rewrite decreasingMax_list.
      rewrite <- Hyp2. apply IHl.
        apply decreasingSort with (a:=a);assumption.
        rewrite Hyp2 in Hyp;assumption.
        discriminate.
Qed.
Require Import ZArith Lia.
Open Scope Z_scope.

(*********************************************************************)
(** *                    Additional tactics                          *)
(*********************************************************************)
(*
       A : Additional lemmas about logic
       B : Tactics about equalities et inequalities
             - Zbool_lia_goal
             - Zbool_lia
       C : Tactic on inductive type constructor
             - Zbool_constructor
       D : Tactics about minimum and maximum
             - researchSimplificationMin_goal
             - researchSimplificationMax_goal
             - Zminmax_lia_goal
             - researchSimplificationMin
             - researchSimplificationMax
             - Zminmax_lia
*)

(*******************************************)
(** ** A : Additional lemmas about logic   *)
(*******************************************)
(*
	Lemma Zle_bool_imp_le n m : (n <=? m) = true -> (n <= m).
	Lemma Zle_is_le_bool n m : (n <= m) <-> (n <=? m) = true.
	Lemma Zge_is_le_bool n m : (n >= m) <-> (m <=? n) = true.
	Lemma Zlt_is_lt_bool n m : (n < m) <-> (n <? m) = true.
	Lemma Zgt_is_gt_bool n m : (n > m) <-> (n >? m) = true.
	Lemma Zlt_is_le_bool n m : (n < m) <-> (n <=? m - 1) = true.
	Lemma Zgt_is_le_bool n m : (n > m) <-> (m <=? n - 1) = true.

    Negated variants of the specifications

	Lemma leb_nle x y : x <=? y = false <-> ~ (x <= y).
	Lemma leb_gt x y : x <=? y = false <-> y < x.
	Lemma ltb_nlt x y : x <? y = false <-> ~ (x < y).
	Lemma ltb_ge x y : x <? y = false <-> y <= x.

    Properties of the deprecated Zeq_bool

	Lemma Zeq_is_eq_bool x y : x = y <-> Zeq_bool x y = true.
	Lemma Zeq_bool_eq x y : Zeq_bool x y = true -> x = y.
	Lemma Zeq_bool_neq x y : Zeq_bool x y = false -> x <> y.
	Lemma Zeq_bool_if x y : if Zeq_bool x y then x=y else x<>y.
*)

Lemma Zeq_is_neq_bool : forall a b, 
a<>b <-> (Zeq_bool b a) = false.
Proof.
intros a b.
split;intro Hyp.
- case_eq(Zeq_bool b a).
    * intros Hyp2. 
      apply Zeq_bool_eq in Hyp2. lia.
    * auto.
- apply Zeq_bool_neq in Hyp. lia.
Qed.

Lemma decompose_ineg : forall z z0 x, 
z <= x <= z0 <-> (z <= z0) /\ (z = x \/ z+1 <= x <= z0).
Proof.
split;intro Hyp.
- case(dec_eq z x); intro Hyp2.
   * split.
        + lia.
        + left. assumption.
   * split.
        lia.
        right. decompose [and] Hyp. split.
          + lia.
          + assumption.
- decompose [and] Hyp. decompose [or] H0;lia.
Qed.

(******************************************************)
(** ** B : Tactics about equalities et inequalities   *)
(******************************************************)

(** This tactic translates the current goal from bool to Prop. **)

(** For instance, ?x <? ?y (bool) will become ?x < ?y (Prop) **)

(** and Zeq_bool ?x ?y (bool) will become ?x = ?y (Prop) **)

(** Of course x and y are in Z. **)

Ltac Zbool_lia_goal :=
repeat match goal with
 | [ |- (?n <? ?m) = true ] =>                     (* < *)
       (apply (Zlt_is_lt_bool n m) )
 | [ |- (?n <? ?m) = false ] => 
       (apply Z.ltb_ge)

 | [ |- (?n >? ?m) = true ] =>                     (* > *)
       (apply (Zgt_is_gt_bool n m))
 | [ |- (?n >? ?m) = false ] => 
       (rewrite Z.gtb_ltb ; apply Z.ltb_ge)

 | [ |- (?n >=? ?m) = true ] =>                    (* >= *)
       (rewrite Z.geb_leb; apply (Zge_is_le_bool n m) )
 | [ |- (?n >=? ?m) = false ] => 
       (rewrite Z.geb_leb ; apply Z.leb_gt )

 | [ |- (?n <=? ?m) = true ] =>                    (* <= *)
       (apply (Zle_is_le_bool n m))
 | [ |- (?n <=? ?m) = false ] => 
       (apply Z.leb_gt )

 | [ |- (Zeq_bool ?b ?a) = true ] =>               (* Zeq_bool *)
       (apply Zeq_is_eq_bool )
 | [ |- (Zeq_bool ?b ?a) = false ] => 
       (apply Zeq_is_neq_bool )
end.

(** This tactic works on all hypothesis of the context like Zbool_lia_goal. **)

Ltac Zbool_lia :=
repeat match goal with
 | [ H : (?n <? ?m) = true  |- ?P ] =>             (* < *)
       (apply (Zlt_is_lt_bool n m) in H )
 | [ H : (?n <? ?m) = false  |- ?P ] => 
       (apply Z.ltb_ge in H )

 | [ H : (?n >? ?m) = true |- ?P ] =>              (* > *)
       (apply (Zgt_is_gt_bool n m) in H)
 | [ H : (?n >? ?m) = false  |- ?P ] => 
       (rewrite Z.gtb_ltb in H ; apply Z.ltb_ge in H)

 | [ H : (?n >=? ?m) = true  |- ?P ] =>            (* >= *)
       (rewrite Z.geb_leb in H ; 
        apply (Zge_is_le_bool n m) in H )
 | [ H : (?n >=? ?m) = false  |- ?P ] => 
       (rewrite Z.geb_leb in H ; apply Z.leb_gt in H )

 | [ H : (?n <=? ?m) = true  |- ?P ] =>            (* <= *)
       (apply (Zle_is_le_bool n m) in H)
 | [ H : (?n <=? ?m) = false  |- ?P ] => 
       (apply Z.leb_gt in H )

 | [ H : (Zeq_bool ?b ?a) = true  |- ?P ] =>       (* Zeq_bool *)
       (apply (Zeq_bool_eq b a) in H )
 | [ H : (Zeq_bool ?b ?a) = false  |- ?P ] => 
       (apply (Zeq_bool_neq b a) in H )
end.

(**************************************************)
(** ** C : Tactic on inductive type constructor   *)
(**************************************************)

Ltac Zbool_constructor := Zbool_lia; constructor;(lia||assumption).

(***********************************************)
(** ** D : Tactics about minimum and maximum   *)
(***********************************************)

Ltac researchSimplificationMin_goal a b :=
Zbool_lia ; repeat match goal with
 | [ H : (a <= b) |- ?P ] => apply Z.min_l in H ; rewrite H ;                   (* <= *)
                             apply Z.min_l_iff in H         
 | [ H : (b <= a) |- ?P ] => apply Z.min_r in H ; rewrite H ;
                             apply Z.min_r_iff in H
 | [ H : (b >= a) |- ?P ] => apply Z.ge_le in H ; apply Z.min_l in H ;           (* >= *)
                             rewrite H ; apply Z.min_l_iff in H ;
                             apply Z.le_ge in H                 
 | [ H : (a >= b) |- ?P ] => apply Z.ge_le in H ; apply Z.min_r in H ;
                             rewrite H ; apply Z.min_r_iff in H ; 
                             apply Z.le_ge in H
 | [ H : (a < b) |- ?P ] => assert(a < b) as Hbis by assumption ;                (* < *)
                            apply Z.lt_le_incl in Hbis ; apply Z.min_l in Hbis ;   
                            rewrite Hbis ; clear Hbis                  
 | [ H : (b < a) |- ?P ] => assert(b < a) as Hbis by assumption ;
                            apply Z.lt_le_incl in Hbis ; apply Z.min_r in Hbis ;
                            rewrite Hbis ; clear Hbis
 | [ H : (b > a) |- ?P ] => assert(b > a) as Hbis by assumption ;                (* > *)
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;  
                            apply Z.min_l in Hbis ; rewrite Hbis ; clear Hbis
 | [ H : (a > b) |- ?P ] => assert(a > b) as Hbis by assumption ;
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;
                            apply Z.min_r in Hbis ; rewrite Hbis ; clear Hbis
 | [ H : (b = a) |- ?P ] => rewrite H ; rewrite Z.min_id                         (* = *)
 | [ H : (a = b) |- ?P ] => rewrite H ; rewrite Z.min_id
end.

Ltac researchSimplificationMax_goal a b :=
Zbool_lia ; repeat match goal with
 | [ H : (a <= b) |- ?P ] => apply Z.max_r in H ; rewrite H ;                    (* <= *)
                             apply Z.max_r_iff in H         
 | [ H : (b <= a) |- ?P ] => apply Z.max_l in H ; rewrite H ;
                             apply Z.max_l_iff in H
 | [ H : (b >= a) |- ?P ] => apply Z.ge_le in H ; apply Z.max_r in H ;           (* >= *)
                             rewrite H ; apply Z.max_r_iff in H ;
                             apply Z.le_ge in H                 
 | [ H : (a >= b) |- ?P ] => apply Z.ge_le in H ; apply Z.max_l in H ;
                             rewrite H ; apply Z.max_l_iff in H ;
                             apply Z.le_ge in H
 | [ H : (a < b) |- ?P ] => assert(a < b) as Hbis by assumption ;                (* < *)
                            apply Z.lt_le_incl in Hbis ; apply Z.max_r in Hbis ;
                            rewrite Hbis ; clear Hbis
 | [ H : (b < a) |- ?P ] => assert(b < a) as Hbis by assumption ;
                            apply Z.lt_le_incl in Hbis ; apply Z.max_l in Hbis ;
                            rewrite Hbis ; clear Hbis
 | [ H : (b > a) |- ?P ] => assert(b > a) as Hbis by assumption ;                (* > *)
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;   
                            apply Z.max_r in Hbis ; rewrite Hbis ; clear Hbis
 | [ H : (a > b) |- ?P ] => assert(a > b) as Hbis by assumption ;
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;
                            apply Z.max_l in Hbis ; rewrite Hbis ; clear Hbis
 | [ H : (b = a) |- ?P ] => rewrite H ; rewrite Z.max_id                         (* = *)
 | [ H : (a = b) |- ?P ] => rewrite H ; rewrite Z.max_id
end.

Ltac Zminmax_lia_goal :=
repeat match goal with
 | [ |- context[Z.min ?a ?b] ] =>
      ( assert(a <= b) as Zminmax_lia_goal_triviale by (lia) ; 
        researchSimplificationMin_goal a b
        (* ; destruct Zminmax_lia_goal_triviale *) )
 | [ |- context[Z.min ?a ?b] ] =>
      ( assert(b <= a) by (lia) ; researchSimplificationMin_goal a b)
 | [ |- context[Z.max ?a ?b] ] =>
      ( assert(a <= b) by (lia) ; researchSimplificationMax_goal a b)
 | [ |- context[Z.max ?a ?b] ] =>
      ( assert(b <= a) by (lia) ; researchSimplificationMax_goal a b)
      
 | [ |- context[Z.min ?a ?b] ] =>
      ( researchSimplificationMin_goal a b )
 | [ |- context[Z.max ?a ?b] ] =>
      ( researchSimplificationMax_goal a b )
      
 | [ H : context[Z.max ?a ?c <= ?b] |- ?a <= ?b ] =>
      ( apply Z.max_lub_l with (m:=c) ; tauto )
 | [ H : context[Z.max ?c ?a <= ?b] |- ?a <= ?b ] =>
      ( apply Z.max_lub_r with (n:=c) ; tauto )
 | [ H : context[?b <= Z.min ?a ?c] |- ?b <= ?a ] =>
      ( apply Z.min_glb_l with (m:=c) ; tauto )
 | [ H : context[?b <= Z.min ?c ?a] |- ?b <= ?a ] =>
      ( apply Z.min_glb_r with (n:=c) ; tauto )
end.

Ltac researchSimplificationMin a b Hyp :=
Zbool_lia ; repeat match goal with
 | [ H : (a <= b) |- ?P ] => apply Z.min_l in H ; rewrite H in Hyp ;             (* <= *)
                             apply Z.min_l_iff in H         
 | [ H : (b <= a) |- ?P ] => apply Z.min_r in H ; rewrite H in Hyp ;
                             apply Z.min_r_iff in H
 | [ H : (b >= a) |- ?P ] => apply Z.ge_le in H ; apply Z.min_l in H ;           (* >= *)
                             rewrite H in Hyp ; apply Z.min_l_iff in H ;
                             apply Z.le_ge in H                 
 | [ H : (a >= b) |- ?P ] => apply Z.ge_le in H ; apply Z.min_r in H ;
                             rewrite H in Hyp ; apply Z.min_r_iff in H ; 
                             apply Z.le_ge in H
 | [ H : (a < b) |- ?P ] => assert(a < b) as Hbis by assumption ;                (* < *)
                            apply Z.lt_le_incl in Hbis ; apply Z.min_l in Hbis ;   
                            rewrite Hbis in Hyp ; clear Hbis                  
 | [ H : (b < a) |- ?P ] => assert(b < a) as Hbis by assumption ;
                            apply Z.lt_le_incl in Hbis ; apply Z.min_r in Hbis ;
                            rewrite Hbis in Hyp ; clear Hbis
 | [ H : (b > a) |- ?P ] => assert(b > a) as Hbis by assumption ;                (* > *)
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;  
                            apply Z.min_l in Hbis ; rewrite Hbis in Hyp ; clear Hbis
 | [ H : (a > b) |- ?P ] => assert(a > b) as Hbis by assumption ;
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;
                            apply Z.min_r in Hbis ; rewrite Hbis in Hyp ; clear Hbis
 | [ H : (b = a) |- ?P ] => rewrite H in Hyp ; rewrite Z.min_id in Hyp           (* = *)
 | [ H : (a = b) |- ?P ] => rewrite H in Hyp ; rewrite Z.min_id in Hyp
end.

Ltac researchSimplificationMax a b Hyp :=
Zbool_lia ; repeat match goal with
 | [ H : (a <= b) |- ?P ] => apply Z.max_r in H ; rewrite H in Hyp ;             (* <= *)
                             apply Z.max_r_iff in H         
 | [ H : (b <= a) |- ?P ] => apply Z.max_l in H ; rewrite H in Hyp ;
                             apply Z.max_l_iff in H
 | [ H : (b >= a) |- ?P ] => apply Z.ge_le in H ; apply Z.max_r in H ;           (* >= *)
                             rewrite H in Hyp ; apply Z.max_r_iff in H ;
                             apply Z.le_ge in H                 
 | [ H : (a >= b) |- ?P ] => apply Z.ge_le in H ; apply Z.max_l in H ;
                             rewrite H in Hyp ; apply Z.max_l_iff in H ;
                             apply Z.le_ge in H
 | [ H : (a < b) |- ?P ] => assert(a < b) as Hbis by assumption ;                (* < *)
                            apply Z.lt_le_incl in Hbis ; apply Z.max_r in Hbis ;
                            rewrite Hbis in Hyp ; clear Hbis
 | [ H : (b < a) |- ?P ] => assert(b < a) as Hbis by assumption ;
                            apply Z.lt_le_incl in Hbis ; apply Z.max_l in Hbis ;
                            rewrite Hbis in Hyp ; clear Hbis
 | [ H : (b > a) |- ?P ] => assert(b > a) as Hbis by assumption ;                (* > *)
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;   
                            apply Z.max_r in Hbis ; rewrite Hbis in Hyp ; clear Hbis
 | [ H : (a > b) |- ?P ] => assert(a > b) as Hbis by assumption ;
                            apply Z.gt_lt in Hbis ; apply Z.lt_le_incl in Hbis ;
                            apply Z.max_l in Hbis ; rewrite Hbis in Hyp ; clear Hbis
 | [ H : (b = a) |- ?P ] => rewrite H in Hyp ; rewrite Z.max_id in Hyp           (* = *)
 | [ H : (a = b) |- ?P ] => rewrite H in Hyp ; rewrite Z.max_id in Hyp
end.

Ltac Zminmax_lia :=
repeat match goal with
 | [ H0 : Z.min ?a ?b = ?a |- _ ] =>           (* MIN *)
      ( apply Z.min_l_iff in H0 )
 | [ H0 : Z.min ?a ?b = ?b |- _ ] =>
      ( apply Z.min_r_iff in H0 )
 | [ H0 : context[Z.min ?a ?b] |- _ ] =>
      ( researchSimplificationMin a b H0 )
 | [ H0 : Z.max ?a ?b = ?a |- _ ] =>           (* MAX *)
      ( apply Z.max_l_iff in H0 )
 | [ H0 : Z.max ?a ?b = ?b |- _ ] =>
      ( apply Z.max_r_iff in H0 )
 | [ H0 : context[Z.max ?a ?b] |- _ ] =>
      ( researchSimplificationMax a b H0 )
end.

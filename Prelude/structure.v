Require Import ZArith Lia.
Open Scope Z_scope.

Definition extractionOption (A : Type)  (op : option A) (val_def : A) :=
 match op with
  |None => val_def
  |Some x => x
 end.

Parameters min_int (*max_int*) : Z.

(*********************************************************************)
(** *                    Types and invariants                        *)
(*********************************************************************)

(***************************************************)
(** ** Type and invariant of real intervals list   *)
(***************************************************)

(** *** Type of elt_list *)
Inductive elt_list :=
 |Nil : elt_list
 |Cons : Z -> Z -> elt_list -> elt_list.

(** *** Invariant of elt_list *)
Inductive Inv_elt_list : Z -> elt_list -> Prop :=
 | invNil  : forall b, Inv_elt_list b Nil
 | invCons : forall (a b  j: Z) (q : elt_list),
     j <= a -> a <= b ->  Inv_elt_list (b+2) q ->
     Inv_elt_list j (Cons a b q).

(*************************************)
(** **    Lemmes on invariants       *)
(*************************************)

Theorem inv_elt_list_monoton : forall l y z, 
Inv_elt_list y l -> z <= y -> Inv_elt_list z l.
Proof.
induction 1; intro Hyp.
 - constructor.
 - constructor;(lia||assumption).
Qed.

Ltac Inv_monotony z := apply inv_elt_list_monoton 
                        with (y:=z);[assumption|lia].

Theorem inv_elt_list_restrict : forall l a b c d y, c <= d ->
((a<=c)/\(c<=b)) /\ ((a<=d)/\(d<=b)) ->
Inv_elt_list y (Cons a b l) -> Inv_elt_list y (Cons c d l).
Proof.
intros l a b c d y Hyp1 Hyp2 Hyp3.
inversion Hyp3. decompose [and] Hyp2.
constructor;[lia|assumption|Inv_monotony (b+2)].
Qed.

Lemma inv_elt_list_simpl : forall l z1 z2 z0 z y, 
Inv_elt_list y (Cons z1 z2 (Cons z z0 l)) -> 
Inv_elt_list y (Cons z1 z2 l).
Proof.
intros l z1 z2 z0 z y HInv.
inversion HInv. inversion H5.
constructor;[lia|lia|Inv_monotony (z0+2)].
Qed.

(***************************************************)
(** ** Size, max and min on real intervals list    *)
(***************************************************)

(** *** Size of elt_list *)
Fixpoint ps (s : Z) (l : elt_list) := match l with
|Nil => s
|Cons min max q => ps (max-min+1 + s) q
end.
Definition process_size (l : elt_list) := ps 0 l.

(** *** Maximum of elt_list *)
Fixpoint pm (m : Z) (l : elt_list) := match l with
|Nil => m
|Cons _ b q => pm b q
end.
Definition process_max (l : elt_list) := pm min_int l.

(** *** Minimum of elt_list *)
Definition get_min (l : elt_list) (e : Z) : Z := match l with
|Nil => e
|Cons a _ _ => a
end.

(**************************************)
(** ** Type and invariant of domain   *)
(**************************************)

(** *** Type of domain *)
Record t := mk_t {
  domain : elt_list ;
  size : Z ;
  max  : Z ;
  min  : Z }.

(** *** Invariant of domain *)
Definition Inv_t (d : t) := Inv_elt_list (min d) (domain d)
                              /\ (min d) = get_min (domain d) min_int
                              /\ (max d) = process_max (domain d)
                              /\ (size d) = process_size (domain d).

(**************************************)
(** ** Some measurement functions     *)
(**************************************)
Definition minus_pair (min_max : Z * Z ) :=
  let (min, max) := min_max in Z.abs_nat (max - min).

Definition minus_pair_other 
  (p_min_x_max_acc : ((Z->bool)*(Z*Z))*(Z*elt_list)) := 
    match p_min_x_max_acc with
     | ((p, (min, x)), (max, acc)) =>  Z.abs_nat (max - x)
    end.

Definition minus_triplet_gen (A : Type) (min_max_tail : (Z*Z)*A) := 
 match min_max_tail with ( (min, max), tail) => Z.abs_nat (max - min) end.

Definition minus_triplet := minus_triplet_gen (list Z).

Fixpoint elt_list_length (l : elt_list) : nat := match l with
 | Nil => 0
 | Cons _ _ q => 2 + elt_list_length q
end.

Lemma evenLength : forall (l : elt_list), Nat.even (elt_list_length l) = true.
Proof.
induction l.
 - simpl. reflexivity.
 - simpl. apply IHl.
Qed.

Definition decreasing_length (l1_l2 : elt_list*elt_list) : nat :=
  elt_list_length (snd l1_l2) + elt_list_length (fst l1_l2).
Require Import R.Prelude.structure R.Prelude.xStandardLib R.FBool.member R.FBool.included R.FBool.exists R.FBool.forall R.FBool.is_empty R.FDomain.union R.FDomain.difference R.FDomain.intersection R.FDomain.add R.FDomain.remove R.FDomain.filter R.FDomain.partition R.FValue.fold R.FValue.get_value R.FValue.size R.FValue.max R.FValue.min R.Prelude.tactics R.CD.values R.CD.empty R.CD.interval R.CD.singleton.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

Require Import FSetInterface.

Module Z_as_OT <: OrderedType.

  Definition t := Z.

  Definition eq := @eq t.
  Definition eq_refl := @refl_equal t.
  Definition eq_sym := @sym_eq t.
  Definition eq_trans := @trans_eq t.

  Definition lt := Z.lt.

  Lemma lt_trans : 
  forall x y z : t, lt x y -> lt y z -> lt x z.
  Proof Z.lt_trans.


  Lemma lt_not_eq : forall x y : t, lt x y -> ~ eq x y.
  Proof Z.lt_neq.
  

  Definition compare x y : Compare lt eq x y :=
    match Z.eq_dec x y with
    | left eqxy => EQ eqxy
    | right neqxy =>
      match not_Zeq_inf _ _ neqxy with
      | left ltxy => LT ltxy
      | right gtxy => GT gtxy
      end
    end.

  Definition eq_dec := Z.eq_dec.


End Z_as_OT.

Module Z_as_DecT <: DecidableType.

  Definition t := Z.
  Definition eq := @eq t.
  Definition eq_refl := @refl_equal t.
  Definition eq_sym := @sym_eq t.
  Definition eq_trans := @trans_eq t.

  Definition eq_dec := Z.eq_dec.
  
  Lemma eq_compat : forall x y, eq x y -> x = y.
  Proof.
  auto.
  Qed.
  
End Z_as_DecT.


Module WSListInterval <: WSfun (Z_as_DecT).

  Definition elt := Z.

  Record t_Fset := mk_t_Fset {
  set : structure.t ;
  wf : Inv_t set 
  }.

  Definition t := t_Fset.

  (*************************************)
  Definition elements s := R.CD.values.values (set s).

  Definition In e s := List.In e (elements s).
  
  Ltac rewrite_In_to_member s :=
   unfold In ; unfold elements ; destruct s ; simpl ;
   repeat rewrite <- member_spec;try auto.
   
  Ltac rewrites_In_to_member s s' :=
   unfold In ; unfold elements ; 
   destruct s; destruct s'; simpl;
   repeat rewrite <- member_spec; try auto.

  Ltac rewrite_In_to_member_bis :=
   unfold In ; unfold elements ; simpl ;
   repeat rewrite <- member_spec;try auto.
  
  Definition Equal s s' := forall a : elt, In a s <-> In a s'.

  Definition Subset s s' := forall a : elt, In a s -> In a s'.

  Definition Empty s := forall a : elt, ~ In a s.

  Definition For_all (P : elt -> Prop) s := forall x, In x s -> P x.

  Definition Exists (P : elt -> Prop) s := exists x, In x s /\ P x.

  Notation "s [=] t" := (Equal s t) (at level 70, no associativity).
  Notation "s [<=] t" := (Subset s t) (at level 70, no associativity).

  Definition eq := Equal.

  (*************************************)

  Definition mem x s := member x (set s).

  Definition equal s1 s2 := equal (set s1) (set s2).

(*  Definition subset s1 s2 := included (set s1) (set s2). *)

  Definition subset s1 s2 := 
     elt_list_included (domain (set s1), domain (set s2)).
  
  Definition empty :=
  let s := R.CD.empty.empty in
  mk_t_Fset s (empty_inv).

  Definition is_empty s := R.FBool.is_empty.is_empty (set s).

  Definition add x s :=
  let res := R.FDomain.add.add x (set s) in
  mk_t_Fset res (add_inv (set s) x (wf s)).

  Definition remove x s :=
  let res := R.FDomain.remove.remove x (set s) in
  mk_t_Fset res (remove_inv (set s) (wf s) x).

  Definition singleton x :=
  let s := R.CD.singleton.singleton x in
  mk_t_Fset s (singleton_inv x).

  Definition union s1 s2 :=
  let s := R.FDomain.union.union (set s1) (set s2) in
  mk_t_Fset s (union_inv (set s1) (set s2) (wf s1) (wf s2)).

  Definition inter s1 s2 :=
  let s := R.FDomain.intersection.intersection
  (set s1) (set s2) in
  mk_t_Fset s (intersection_inv (set s1) (set s2) (wf s1) (wf s2)).

  Definition diff s1 s2 :=
  let s := R.FDomain.difference.difference (set s1) (set s2) in
  mk_t_Fset s (difference_inv (set s1) (set s2) (wf s1) (wf s2)).

  Definition fold A f s e := 
     R.FValue.fold.fold_left A f (domain (set s)) e.

  Definition cardinal s := R.FValue.size.process_size_nat (domain (set s)).

  Definition filter p s := 
  let res := R.FDomain.filter.filter p (set s) in
  mk_t_Fset res (filter_inv p (set s) (wf s)).

  Definition for_all p s := 
     R.FBool.forall.for_all p (set s).

  Definition exists_ p s := exist p (set s).

  Definition partition p s := 
  let res := R.FDomain.partition.partition p (set s) in
  let a := mk_t_Fset (fst res) (filter_inv p (set s) (wf s)) in
  let b := mk_t_Fset (snd res) (filter_inv (fun x => negb (p x)) (set s) (wf s)) in (a,b).

  Definition choose s := R.FValue.get_value.get_value (set s).

  (* pour l'interface S*)
  Definition min_elt s := 
  match (domain (set s)) with
  | Nil => None 
  | _ => Some (structure.min (set s))
  end.
  
  Definition max_elt s := 
  match (domain (set s)) with
  | Nil => None 
  | _ => Some (structure.max (set s))
  end.
  (***********************************************)
  
  Lemma eq_dec : forall s s', {eq s s'} + {~ eq s s'}.
  Proof.
  intros s s'. unfold eq. unfold Equal.
  rewrites_In_to_member s s'.
  apply SameElt_dec ; 
    (apply values_Sorted||apply values_NoDup);assumption.
  Defined.

  
  (*******************************************************)
  Section Spec.

    Variables s s' s'' : t.
    Variables x y : elt.
  
    (* Specification of In *)
    Lemma In_1 :  Z_as_DecT.eq x y -> In x s -> In y s.
    Proof.
    rewrite_In_to_member s. intros Hyp1 Hyp2. 
    assert(y = x) as Hxy by auto; rewrite Hxy;assumption.
    Qed.


    (* Specification of eq *)
    Lemma eq_refl : eq s s.
    Proof.
    unfold eq. unfold Equal. tauto. 
    Qed.
   
    Lemma eq_sym : eq s s' -> eq s' s.
    Proof.
    unfold eq. unfold Equal.
    intros Hyp a.
    split;apply Hyp with (a:=a).
    Qed.
   
    Lemma eq_trans : eq s s' -> eq s' s'' -> eq s s''.
    Proof.
    unfold eq. unfold Equal.
    intros Hyp1 Hyp2 a.
    split;rewrite Hyp1;rewrite <- Hyp2;auto.
    Qed.

    (* Specification of mem *)
    Lemma mem_1 : In x s -> mem x s = true.
    Proof.
    rewrite_In_to_member s.
    Qed.

    Lemma mem_2 :  mem x s = true -> In x s. 
    Proof. 
    rewrite_In_to_member s.
    Qed.
   
    (* Specification of equal *)
    Lemma equal_1 : Equal s s' -> equal s s' = true.
    Proof.
    unfold equal. unfold Equal. rewrites_In_to_member s s'.
    intro Hyp.
    apply equal_spec_1; try assumption.
    intro x0.
    repeat rewrite member_spec; auto.
    Qed.
    
    Lemma equal_2 : equal s s' = true -> Equal s s'.
    Proof.
    unfold equal. unfold Equal. rewrites_In_to_member s s'.
    intros Hyp a. repeat rewrite <- member_spec; auto.
    apply equal_spec_2; try assumption.
    Qed.
    
    (* Specification of subset *)
    Lemma subset_1 : Subset s s' -> subset s s' = true.
    Proof.
    unfold subset. unfold Subset. 
    unfold In;unfold elements, values;destruct s;destruct s';simpl.
       intro HIn.
    destruct set0 ; destruct set1. 
    unfold included. simpl . simpl in HIn.
    unfold Inv_t in wf0; simpl in wf0; decompose [and] wf0.
    unfold Inv_t in wf1; simpl in wf1; decompose [and] wf1.
    apply elt_list_included_spec_1 
      with (l1:=domain) (l2 := domain0) (y1 := min) (y2 := min0) ; auto.
    intro x0.  rewrite elt_list_member_spec with (y := min) ; auto.
    rewrite elt_list_member_spec with (y := min0) ; auto. 
    Qed.
    
   (*Lemma subset_2 : subset s s' = true -> Subset s s'.
    Proof.
    unfold subset. unfold Subset. 
    unfold In;unfold elements;destruct s;destruct s';simpl.
    intros Hyp1 a.
    repeat rewrite <- member_spec; try auto.
    intro Hyp2.
    apply included_spec with (x:=a) in Hyp1; try auto.
    Qed.*)
    
    Lemma subset_2 : subset s s' = true -> Subset s s'.
    Proof.
    unfold subset. unfold Subset. 
    unfold In;unfold elements;destruct s;destruct s';simpl.
    intros Hyp1 a.
    repeat rewrite <- member_spec; auto.
    destruct set0 ; destruct set1. unfold member ; simpl. simpl in Hyp1.  
    unfold Inv_t in wf0; simpl in wf0; decompose [and] wf0.
    unfold Inv_t in wf1; simpl in wf1; decompose [and] wf1.
    intro Hyp2.
    eapply elt_list_included_spec_2 
       with (x:=a) (y1 := min) (y2 := min0) in Hyp1; auto. 
    auto. auto. auto.
    Qed.
    
    (* Specification of empty *)
    Lemma empty_1 : Empty empty.
    Proof.
    unfold Empty. unfold empty. auto.
    Qed.

    (* Specification of is_empty *)
    Lemma is_empty_1 : Empty s -> is_empty s = true.
    Proof.
    unfold Empty. unfold is_empty.
    destruct s. destruct set0. simpl.
    intro Hyp.
    case_eq(domain);intros;subst.
      + apply equiv_is_empty_elt_list_is_empty; try auto.
      + unfold In in Hyp. unfold elements in Hyp. 
        simpl in Hyp. 
        assert(List.In z
        (values
           {|
           domain := Cons z z0 e;
           size := size;
           structure.max := max;
           min := min |})) as HypKey. 
        rewrite <- member_spec;try auto.
        unfold member;simpl.
        unfold Inv_t in wf0. simpl in wf0. 
        decompose [and] wf0. inversion H.
        assert(z <=? z0 = true) as HypHidden
          by (Zbool_lia_goal;lia);rewrite HypHidden;Zbool_lia_goal;lia.
          apply Hyp in HypKey. contradiction.
    Qed.
    
    Lemma is_empty_2 : is_empty s = true -> Empty s.
    Proof.
    unfold Empty. unfold is_empty.
    rewrite_In_to_member s;unfold values.
    intros Hyp a. 
    apply equiv_is_empty_elt_list_is_empty in Hyp; try auto.
    apply elt_list_is_empty_spec in Hyp.
    rewrite Hyp. auto.
    Qed.

    (* Specification of add *)
    Lemma add_1 : Z_as_DecT.eq x y -> In y (add x s).
    Proof.
    unfold add. rewrite_In_to_member s;simpl.
      + intro. apply add_spec_1; try auto.
      + apply add_inv;assumption.
    Qed.
    
    Lemma add_2 : In y s -> In y (add x s).
    Proof.
    unfold add. rewrite_In_to_member s; simpl.
      + intro. apply add_spec_2; try auto.
      + apply add_inv;assumption.
    Qed.
    
    Lemma add_3 : ~ Z_as_DecT.eq x y -> 
    In y (add x s) -> In y s.
    Proof.
    unfold add. rewrite_In_to_member s; simpl.
      + intro. apply add_spec_3; try auto.
      + apply add_inv;assumption.
    Qed.
    
    (* Specification of remove *)
    Lemma remove_1 : Z_as_DecT.eq x y -> 
    ~ In y (remove x s).
    Proof.
    unfold remove. rewrite_In_to_member s; simpl.
      + intro.
        assert(x = y) as Heq by auto;rewrite Heq.
        rewrite remove_spec_1 with (v:=y) (d:=set0); 
          try auto.
      + apply remove_inv;assumption.
    Qed.
    
    Lemma remove_2 : ~ Z_as_DecT.eq x y -> 
    In y s -> In y (remove x s).
    Proof.
    unfold remove. rewrite_In_to_member s; simpl.
      + intro.
        rewrite remove_spec_2_3 
          with (v:=x) (y:=y) (d:=set0); 
            try auto.
      + apply remove_inv;assumption.
    Qed.
    
    Lemma remove_3 : In y (remove x s) -> In y s.
    Proof.
    unfold remove. rewrite_In_to_member s; simpl.
      + intro.
        destruct (Z_as_OT.eq_dec x y).
        - subst. rewrite remove_spec_1 in H; try assumption. discriminate.
        - rewrite <- remove_spec_2_3 
          with (v:=x) (y:=y) (d:=set0); 
            try auto.
      + apply remove_inv;assumption.
    Qed.
    
    (* Specification of singleton *)
    Lemma singleton_1 : In y (singleton x) -> 
    Z_as_DecT.eq x y.
    Proof.
    unfold singleton. rewrite_In_to_member_bis.
    apply singleton_spec_1.
    apply singleton_inv.
    Qed.
    
    Lemma singleton_2 : Z_as_DecT.eq x y -> 
    In y (singleton x).
    Proof.
    unfold singleton. rewrite_In_to_member_bis.
    apply singleton_spec_2.
    apply singleton_inv.
    Qed.

    (* Specification of union *)
    Lemma union_1 : In x (union s s') -> In x s \/ In x s'.
    Proof.
    unfold union. rewrites_In_to_member s s'; simpl.
      + intro. apply union_spec; try auto.
      + apply union_inv;simpl;assumption.
    Qed.
    
    Lemma union_2 : In x s -> In x (union s s').
    Proof.
    unfold union. rewrites_In_to_member s s'; simpl.
      + intro. apply union_spec; try auto.
      + apply union_inv;simpl;assumption.
    Qed.
    
    Lemma union_3 : In x s' -> In x (union s s').
    Proof.
    unfold union. rewrites_In_to_member s s'; simpl.
      + intro. apply union_spec; try auto.
      + apply union_inv;simpl;assumption.
    Qed.

    (* Specification of inter *)
    Lemma inter_1 : In x (inter s s') -> In x s.
    Proof.
    unfold inter. rewrites_In_to_member s s';
    unfold intersection ;  simpl.
      + intro. apply intersection_spec_1 with (s':=set1) ; try auto.
      + apply intersection_inv;simpl;assumption.
    Qed.
    
    Lemma inter_2 : In x (inter s s') -> In x s'.
    Proof.
    unfold inter. rewrites_In_to_member s s'.
    unfold intersection ;  simpl.
      + intro. apply intersection_spec_2 with (s:=set0) ; try auto.
      + apply intersection_inv;simpl;assumption.
    Qed.
 
    Lemma inter_3 : In x s -> In x s' -> 
    In x (inter s s').
    Proof.
    unfold inter. rewrites_In_to_member s s'.
    unfold intersection ;  simpl.
      + intros. apply intersection_spec_3 with (s:=set0) ; try auto.
      + apply intersection_inv;simpl;assumption.
    Qed.

    (* Specification of diff *)
    Lemma diff_1 : In x (diff s s') -> In x s.
    Proof.
    unfold diff. rewrites_In_to_member s s'; simpl.
      + intro. apply difference_spec_1
         with (s':=set1); try auto.
      + apply difference_inv;simpl;assumption.
    Qed.
    
    Lemma diff_2 : In x (diff s s') -> ~ In x s'.
    Proof.
    unfold diff. rewrites_In_to_member s s'; simpl.
      + intro. apply difference_spec_2 in H
         ; try auto. rewrite H;auto.
      + apply difference_inv;simpl;assumption.
    Qed.
    
    Lemma diff_3 : In x s -> ~ In x s' -> In x (diff s s').
    Proof.
    unfold diff. rewrites_In_to_member s s'; simpl.
      + intros. apply difference_spec_3; try auto.
        case_eq(member x set1);
           intro HypM;[contradiction|reflexivity].
      + apply difference_inv;simpl;assumption.
    Qed.
    
    (* Specification of fold *)
    Lemma fold_1 : forall (A : Type) (i : A) 
                          (f : elt -> A -> A),
        fold _ f s i = 
        fold_left (fun a e => f e a) (elements s) i.
    Proof.
    unfold fold. rewrite_In_to_member s; simpl.
    intros. apply fold_left_spec ; auto.
    Qed.
   
    (* Specification of cardinal *)
    Lemma cardinal_1 : cardinal s = length (elements s).
    Proof.
    unfold cardinal. unfold process_size_nat.
    destruct s;simpl. unfold Inv_t in wf0. decompose [and] wf0.
    rewrite size_elt_list_values with (y:=min set0);try auto.
    rewrite Zlength_correct. rewrite Nat2Z.id. reflexivity.
    Qed.

    Section Filter.

      Variable f : elt -> bool.

      (* Specification of filter *)
      Lemma filter_1 : compat_bool Z_as_DecT.eq f -> 
      In x (filter f s) -> In x s.
      Proof.
      unfold filter. rewrite_In_to_member s.
        + intros Hyp1 Hyp2. 
          apply filter_spec in Hyp2; try auto. tauto.
        + apply filter_inv; try auto.
      Qed.
      
      Lemma filter_2 : compat_bool Z_as_DecT.eq f -> 
      In x (filter f s) -> f x = true.
      Proof.
      unfold filter. rewrite_In_to_member s.
        + intros Hyp1 Hyp2. 
          apply filter_spec in Hyp2; try auto. tauto.
        + apply filter_inv; try auto.
      Qed.
      
      Lemma filter_3 : compat_bool Z_as_DecT.eq f -> 
      In x s -> f x = true -> In x (filter f s).
      Proof.
      unfold filter. rewrite_In_to_member s.
        + intros Hyp1 Hyp2 Hyp3. 
          apply filter_spec; try auto.
        + apply filter_inv; try auto.
      Qed.

      (* Specification of for_all *)
      Lemma for_all_1 :
            compat_bool Z_as_DecT.eq f ->
            For_all (fun x => f x = true) s -> 
            for_all f s = true.
      Proof.
      unfold For_all. rewrite_In_to_member s.
      intros Hyp1 Hyp2. apply for_all_spec; try auto.
      intros. apply Hyp2. 
      rewrite <- member_spec; try auto.
      Qed.
      
      Lemma for_all_2 :
            compat_bool Z_as_DecT.eq f ->
            for_all f s = true -> 
            For_all (fun x => f x = true) s.
      Proof.
      unfold For_all. rewrite_In_to_member s.
      intros Hyp1 Hyp2 x0. 
      rewrite <- member_spec; try auto.
      apply for_all_spec; try auto.
      Qed.

      (* Specification of exists *)
      Lemma exists_1 :
            compat_bool Z_as_DecT.eq f ->
            Exists (fun x => f x = true) s -> 
            exists_ f s = true.
      Proof.
      unfold Exists. rewrite_In_to_member s.
      intros Hyp1 Hyp2. apply exist_spec; try auto.
      Qed.
      
      Lemma exists_2 :
            compat_bool Z_as_DecT.eq f ->
            exists_ f s = true -> 
            Exists (fun x => f x = true) s.
      Proof.
      unfold Exists. rewrite_In_to_member s.
      intros Hyp1 Hyp2. apply exist_spec; try auto.
      Qed.

      (* Specification of partition *)
      Lemma partition_1 :
            compat_bool Z_as_DecT.eq f -> 
            Equal (fst (partition f s)) (filter f s).
      Proof.
      unfold Equal. intros Hyp a.
      unfold In. unfold elements. destruct s. simpl.
      tauto.
      Qed. 
      
      Lemma partition_2 :
            compat_bool Z_as_DecT.eq f ->
            Equal (snd (partition f s)) 
                  (filter (fun x => negb (f x)) s).
      Proof.
      unfold Equal. intros Hyp a.
      unfold In. unfold elements. destruct s. simpl.
      tauto.
      Qed.
 
    End Filter.

    (* Specification of elements *)
    Lemma elements_1 : In x s -> InA Z_as_DecT.eq x (elements s).
    Proof.
    rewrite_In_to_member s.
    rewrite member_spec; try auto.
    apply In_InA. split.
      + unfold Reflexive.  apply Z_as_DecT.eq_refl.
      + unfold Symmetric.  apply Z_as_DecT.eq_sym.
      + unfold Transitive. apply Z_as_DecT.eq_trans.
    Qed.
    
    Lemma compat_InA : forall x l, 
    InA Z_as_DecT.eq x l -> List.In x l.
    Proof. 
    induction 1 ; simpl ; auto.
    Qed.
     
    Lemma elements_2 : 
    InA Z_as_DecT.eq x (elements s) -> In x s.
    Proof.
    rewrite_In_to_member s.
    rewrite member_spec;  auto.
    intro Hyp.
    apply compat_InA ; assumption.
    Qed.
    
    Lemma compat_NoDupA :  forall l, NoDup l -> NoDupA Z_as_DecT.eq l.
    Proof.
    induction l; simpl; auto.
    intro Hyp ; inversion_clear Hyp. constructor ; auto. 
    intro HInA ; apply compat_InA in HInA ; contradiction.
    Qed.
    
    (* When compared with ordered sets, here comes the only  
       property that is really weaker: *)
    Lemma elements_3w : NoDupA Z_as_DecT.eq (elements s).
    Proof.
    apply compat_NoDupA.
    unfold elements. apply values_NoDup. 
    destruct s; assumption.
    Qed.

    (* Specification of choose *)
    Lemma choose_1 : choose s = Some x -> In x s.
    Proof.
    unfold choose. rewrite_In_to_member s.
    apply get_value_spec_1;try auto.
    Qed.
    
    Lemma choose_2 : choose s = None -> Empty s.
    Proof.
    unfold choose. intro. apply is_empty_2.
    apply get_value_spec_2;try auto.
    destruct s;assumption.
    Qed.
    
    End Spec.


(*
  Hint Transparent elt.
  Hint Resolve mem_1 equal_1 subset_1 empty_1
    is_empty_1 choose_1 choose_2 add_1 add_2 remove_1
    remove_2 singleton_2 union_1 union_2 union_3
    inter_3 diff_3 fold_1 filter_3 for_all_1 exists_1
    partition_1 partition_2 elements_1 elements_3w
    : set.
  Hint Immediate In_1 mem_2 equal_2 subset_2 is_empty_2 add_3
    remove_3 singleton_1 inter_1 inter_2 diff_1 diff_2
    filter_1 filter_2 for_all_2 exists_2 elements_2
    : set.
*)

End WSListInterval.

Module WSListIntervalbis <: WS with Module E := Z_as_DecT.
 Module E := Z_as_DecT.
 Include WSListInterval.
End WSListIntervalbis.
 
  Module SfunListInterval <: Sfun (Z_as_OT).
  Include WSListInterval.

  Section Spec.

    Variables s s' s'' : t.
    Variables x y : elt.
 
    Lemma choose_3 : choose s = Some x -> choose s' = Some y ->
    Equal s s' -> Z_as_DecT.eq x y.
    Proof.
    unfold choose. unfold get_value. case_eq(domain (set s)).
      + intros Hyp1 Hyp2. discriminate Hyp2.
      + case_eq(domain (set s')).
         * intros Hyp1 z z0 e Hyp2 Hyp3 Hyp4 Hyp5. discriminate Hyp4.
         * intros z z0 e Hyp1 z1 z2 e0 Hyp2 Hyp3 Hyp4 Hyp5.
           unfold Z_as_DecT.eq. inversion Hyp3. inversion Hyp4.
           apply equal_1 in Hyp5. unfold equal in Hyp5. subst.
           unfold equal.equal in Hyp5. 
           rewrite Hyp1 in Hyp5. rewrite Hyp2 in Hyp5.
           apply egality_head 
             with (y1:=min (set s)) (y2:=min (set s')) in Hyp5.
               - tauto.
               - assert(Inv_t (set s)) as HypInv
                    by (destruct s;simpl;assumption).
                 assert(Inv_elt_list (min (set s)) (domain (set s)))
                    as HypInv_elt_list
                    by (unfold Inv_t in HypInv;
                        decompose [and] HypInv; assumption).
                 rewrite <- Hyp2. assumption.
               - assert(Inv_t (set s')) as HypInv
                    by (destruct s';simpl;assumption).
                 assert(Inv_elt_list (min (set s')) (domain (set s')))
                    as HypInv_elt_list
                    by (unfold Inv_t in HypInv;
                        decompose [and] HypInv; assumption).
                 rewrite <- Hyp1. assumption.
    Qed.

    Lemma min_elt_1 : min_elt s = Some x -> In x s.
    Proof.
    unfold min_elt. case_eq(domain (set s)).
      + intros Hyp1 Hyp2. discriminate Hyp2.
      + intros z z0 e Hyp1 Hyp2.
        assert(Inv_t (set s)) as HypInv
          by (destruct s;simpl;assumption).
        assert(Inv_elt_list (min (set s)) (domain (set s)))
          as HypInv_elt_list
          by (unfold Inv_t in HypInv;
              decompose [and] HypInv; assumption).
        assert(min (set s) = z) as HypMin
          by (apply info_min with (z:=z0) (e0:=e); auto).
        assert(get_min (domain (set s)) min_int = z) 
          as HypGet_min 
          by (rewrite egality_min_get_min in HypMin; assumption).
        assert(elt_list_member (get_min (domain (set s)) min_int) (domain (set s)) = true).
           * apply list_min_in with (y:=(min (set s)));
               try assumption.
             rewrite Hyp1. intuition;discriminate H.
           * unfold In. unfold elements. unfold values.
             rewrite <- elt_list_member_spec 
                with (y:=(min (set s))); try assumption.
             rewrite HypGet_min in H.
   
             inversion Hyp2. rewrite HypMin. assumption.
    Qed.
      
    Lemma min_elt_2 : 
    min_elt s = Some x -> In y s -> ~ Z_as_OT.lt y x.
    Proof.
    unfold min_elt. case_eq(domain (set s)).
      + intros Hyp1 Hyp2 Hyp3. discriminate Hyp2.
      + intros z z0 e Hyp1 Hyp2 Hyp3.
        assert(Inv_t (set s)) as HypInv
          by (destruct s;simpl;assumption).
        assert(Inv_elt_list (min (set s)) (domain (set s)))
          as HypInv_elt_list
          by (unfold Inv_t in HypInv;
              decompose [and] HypInv; assumption).
        assert(get_min (domain (set s)) min_int = x) 
          by (inversion Hyp2;rewrite egality_min_get_min;auto).
          
        unfold In in Hyp3. unfold elements in Hyp3.
        unfold values in Hyp3.
        rewrite <- elt_list_member_spec
                with (y:=(min (set s))) in Hyp3; try assumption.
        
        assert(get_min (domain (set s)) (min (set s)) <= y)
          as Hyp4 by (rewrite prop_minimum with (x:=y);
                try (lia||assumption)).
        
        unfold Z_as_OT.lt. unfold not. intro HypFalse.
          
        assert(get_min (domain (set s)) min_int
             = get_min (domain (set s)) (min (set s)))
          as Hyp5 by (apply egality_get_min;
                      rewrite Hyp1;intuition;discriminate H0).    
        subst. lia.
    Qed.
             
    Lemma min_elt_3 : min_elt s = None -> Empty s.
    Proof.
    unfold min_elt. case_eq(domain (set s)).
      + intros Hyp1 Hyp2. unfold Empty.
        intros a HypIn.
        unfold In in HypIn. unfold elements in HypIn.
        unfold values in HypIn.
        rewrite Hyp1 in HypIn. simpl in HypIn. assumption.
      + intros z z0 e Hyp1 Hyp2. discriminate Hyp2.
    Qed.
   
  
   Lemma max_elt_1 : max_elt s = Some x -> In x s.
   Proof.
   unfold max_elt. case_eq(domain (set s)).
     + intros Hyp1 Hyp2. discriminate Hyp2.
     + intros z z0 e Hyp1 Hyp2.
       assert(Inv_t (set s)) as HypInv
         by (destruct s;simpl;assumption).
       assert(Inv_elt_list (min (set s)) (domain (set s)))
         as HypInv_elt_list
         by (unfold Inv_t in HypInv;
             decompose [and] HypInv; assumption).
       inversion Hyp2.
       rewrite egality_max_process_max;try assumption.
       unfold In. unfold elements. unfold values.
       rewrite <- elt_list_member_spec 
                with (y:=(min (set s))); try assumption.
       apply list_max_in with (y:=(min (set s))); try assumption.
       rewrite Hyp1. intuition. discriminate H.
   Qed.

   Lemma max_elt_2 : 
   max_elt s = Some x -> In y s -> ~ Z_as_OT.lt x y.
   Proof.
   unfold max_elt. case_eq(domain (set s)).
    + intros Hyp1 Hyp2 Hyp3. discriminate Hyp2.
    + intros z z0 e Hyp1 Hyp2 Hyp3.
      assert(Inv_t (set s)) as HypInv
        by (destruct s;simpl;assumption).
      assert(Inv_elt_list (min (set s)) (domain (set s)))
        as HypInv_elt_list
        by (unfold Inv_t in HypInv;
            decompose [and] HypInv; assumption).
          
      unfold In in Hyp3. unfold elements in Hyp3.
      unfold values in Hyp3.
      rewrite <- elt_list_member_spec
                with (y:=(min (set s))) in Hyp3; try assumption.
        
      assert(y <= process_max (domain (set s))) as Hyp4
        by (rewrite prop_maximum 
               with (x:=y) (l:=(domain (set s))) (y:=(min (set s))); 
               try (assumption||reflexivity)).
      rewrite <- egality_max_process_max in Hyp4; try assumption.
      inversion Hyp2. unfold Z_as_OT.lt. lia.
   Qed.

   Lemma max_elt_3 : max_elt s = None -> Empty s.
   Proof.
   unfold max_elt. case_eq(domain (set s)).
      + intros Hyp1 Hyp2. unfold Empty.
        intros a HypIn.
        unfold In in HypIn. unfold elements in HypIn.
        unfold values in HypIn.
        rewrite Hyp1 in HypIn. simpl in HypIn. assumption.
      + intros z z0 e Hyp1 Hyp2. discriminate Hyp2.
   Qed.
   
  Lemma compat_Sorted :  forall l, NoDup l -> xStandardLib.Sorted l -> Sorted Z_as_OT.lt l.
    induction l; simpl; auto.
    intros HNoD Hyp ; inversion Hyp. 
    + constructor ; auto. 
    + inversion_clear HNoD. constructor ; subst ; auto.
      constructor.  unfold Z_as_OT.lt, Z.lt.
      assert (~(a=h)) by
        (intro Pb; subst; simpl in H3 ; tauto). 
      apply Z.compare_nge_iff. lia.
    Qed.
   
   Lemma elements_3 : sort Z_as_OT.lt (elements s).
   Proof.
   assert(Inv_t (set s)) as HypInv
         by (destruct s;simpl;assumption).
   pose proof (values_Sorted  _ HypInv) as Hsorted. 
   pose proof (values_NoDup _ HypInv) as HnoD.
   unfold elements. 
   apply compat_Sorted ; auto.
   Qed.
   End Spec.
   
   (* Module LO := MakeListOrdering (Z_as_OT).*)
   
    Module MO:=OrderedTypeFacts Z_as_OT.

 Local Notation tt := (list Z_as_OT.t).
 

 Inductive lt_list : tt -> tt -> Prop :=
    | lt_nil : forall x s, lt_list nil (x :: s)
    | lt_cons_lt : forall x y s s',
       Z_as_OT.lt x y -> lt_list (x :: s) (y :: s')
    | lt_cons_eq : forall x y s s',
        Z_as_OT.eq x y -> lt_list s s' -> lt_list (x :: s) (y :: s').

 Definition ltl := lt_list.
 
 Lemma ltl_trans  : forall l l' l'', ltl l l' -> ltl l' l'' -> ltl l l''.
 Proof.
 unfold lt ; intros until 1 ; generalize l''. 
 induction H.
 + intros l1 Hyp; inversion Hyp; apply lt_nil.
 + intros l1 Hyp ; inversion_clear Hyp.
   - apply lt_cons_lt. apply Z_as_OT.lt_trans with (y:=y) ; auto.
   - apply lt_cons_lt. apply MO.lt_eq with (y:=y) ; auto.
 + intros l1 Hyp. inversion_clear Hyp.
   - apply lt_cons_lt. apply MO.eq_lt with (y:=y) ; auto.
   - unfold ltl in IHlt_list. apply lt_cons_eq ; auto. apply Z_as_OT.eq_trans with (y:=y) ; auto. 
 Qed.
 
 Lemma ltl_irrefl: forall l, ~ltl l l.
 Proof.
 induction l ; intros ; intro Hlt ; inversion Hlt.
 + apply MO.lt_antirefl in H0; auto. 
 + contradiction.
 Qed.
 
 Definition eqtt (s s' : tt) := forall x, (List.In x s) <-> (List.In x s').
 
 Definition compare_tt : forall (s s' : tt), 
 xStandardLib.Sorted s -> xStandardLib.Sorted  s' -> 
   (Compare ltl eqtt s s').
 Proof.
  induction s.
  intros; case s'. 
  - constructor 2 . unfold eqtt. intuition. 
  - repeat constructor.
  - intro s'; case s'.
   + constructor 3. constructor.
   + intros. 
     apply decreasingSort in H .  
     apply decreasingSort in H0. 
     case (Z_as_OT.compare a t0); intro Hcomp.
     * constructor 1 ; constructor 2 ; auto.
     * specialize (IHs l H H0). inversion_clear IHs. 
       constructor 1. constructor 3 ; auto.
       constructor 2. unfold eq. intro x ;
       unfold Z_as_OT.eq in Hcomp ; intuition.
       simpl in H2. destruct H2 ; simpl.
       left ; congruence.
       right ; apply H1 ; auto.
       simpl in H2. destruct H2 ; simpl.
       left ; congruence.
       right ; apply H1 ; auto.
       constructor 3. constructor 3 ; auto. apply Z_as_OT.eq_sym ; auto.
     * constructor 3. constructor 2 ; auto.
Defined.
  
   
 Definition lt s s' := ltl (elements s) (elements s').
   
 (* Specification of lt*)
   
   Lemma lt_trans  : forall s s' s'', lt s s' -> lt s' s'' -> lt s s''.
   Proof.
   unfold lt. intros. apply ltl_trans with (l':= elements s'); auto.
   Qed.
   
  Lemma lt_not_eq :  forall s s', lt s s' -> ~ eq s s'.
  unfold  lt, eq,  elements. simpl ; intros. intro.
  apply equal_1 in H0. destruct s ; destruct s'. unfold equal in H0 ; simpl in H0. simpl in H.
  apply equal_compat_eq_domain in H0 ; auto. unfold values in H. rewrite H0 in H. 
  apply ltl_irrefl with (l:=(elt_list_values (domain set1))) ; auto.
  Qed.
  
  Lemma compare : forall s s' : t, Compare lt eq s s'.
  Proof.
  destruct s; destruct s'. unfold eq, Equal, lt, elements.
  pose proof (values_Sorted set0 wf0) as Hyp1.
  pose proof (values_Sorted set1 wf1) as Hyp2.
  pose proof (compare_tt (values set0) (values set1) Hyp1 Hyp2) as Hcompare.
  inversion Hcompare.
  constructor 1 ; auto.
  constructor 2 . unfold In, elements ; simpl. auto.
  constructor 3 ; auto.
  Defined.  
End SfunListInterval.

Module SetListInterval <: S with Module E := Z_as_OT.
 Module E := Z_as_OT.
 Include SfunListInterval.
End SetListInterval.



#SOURCES=prelude.v structure.v creation.v operation_solver_member.v operation_solver_included.v operation_solver_remove.v operation_solver_fold.v operation_solver_exist_forall.v operation_solver_filter_partition.v operation_solver_basic.v operation_FSet_union.v  operation_FSet_difference.v equal.v  operation_FSet_intersection.v   bridge.v test_extraction.v extraction.v extraction_Z.v extraction_bigint.v


# KNOWNTARGETS will not be passed along to CoqMakefile
KNOWNTARGETS := CoqMakefile extra-stuff extra-stuff2
# KNOWNFILES will not get implicit targets from the final rule, and so
# depending on them won't invoke the submake
# Warning: These files get declared as PHONY, so any targets depending
# on them always get rebuilt
KNOWNFILES   := Makefile _CoqProject

.DEFAULT_GOAL := invoke-coqmakefile

CoqMakefile: Makefile _CoqProject
	$(COQBIN)coq_makefile -f _CoqProject -o CoqMakefile

invoke-coqmakefile: CoqMakefile
	$(MAKE) --no-print-directory -f CoqMakefile $(filter-out $(KNOWNTARGETS),$(MAKECMDGOALS))

.PHONY: invoke-coqmakefile $(KNOWNFILES)

####################################################################
##                      Your targets here                         ##

clean_all: clean_extraction
	make clean
	rm -f CoqMakefile CoqMakefile.conf .Makefile.d .lia.cache .depend_coq .*.aux

doc:
	coqdoc -g  --utf8  --glob-from glob_file $(SOURCES) -d doc

make_extraction:
	cd Extraction/ ; make ; cd ..

clean_extraction:
	cd Extraction/ ; \
	rm -f *.cmi *.cmo .*.aux .depend_coq \
	      domain_interval_test.ml   domain_interval_test.mli \
	      domain_interval_test_Z.ml domain_interval_test_Z.mli \
	      domain_interval_test_bigint.mli  domain_interval_test_bigint.ml ; \
	cd ..
####################################################################
# This should be the last rule, to handle any targets not declared above
%: invoke-coqmakefile
	@true

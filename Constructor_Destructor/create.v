Require Import R.Prelude.structure R.Prelude.tactics R.Prelude.xStandardLib R.CD.empty R.FBool.member.
Require Import List Omega ZArith Lia.
Import ListNotations.
Require Recdef.
Open Scope Z_scope.

Definition fstOption (coupleOption : option (Z*Z)) : Z := match coupleOption with
|None => min_int
|Some(a,b)  => a
end.

Definition sndOption (coupleOption : option (Z*Z)) : Z := match coupleOption with
|None => min_int
|Some(a,b)  => b
end.

Lemma simplfstOption : forall a b c,
fstOption (Some (a, b)) = fstOption (Some (a, c)).
Proof. intros a b c. auto. Qed.

(************************************)
(** * Definition                    *)
(************************************)
Fixpoint make (min last : Z) (Zl : list Z) :=
match Zl with
|nil => Cons min last Nil
|cons t q => if Zeq_bool t (last+1) then make min (last+1) q
             else Cons min last (make t t q)  (*else, last+1 < t, because Zl is sorted and without duplicate *)
end.

(* last_and_length
*   return the last element of the list and its size thanks to a couple
*)
Fixpoint last_and_length (Zl : list Z) :=
match Zl with
|nil => None
|cons t nil => Some (t,1)
|cons _ q => match (last_and_length q) with
             |None => None
             |Some (la, le) => Some (la, le+1)
             end
end.

Definition unsafe_create (Zl : list Z) := match Zl with
|nil => empty
|cons t q => let res := last_and_length Zl in
             let max := (fstOption res) in
             let size := (sndOption res) 
             in mk_t (make t t q) size max t
end.

Fixpoint inserer (x : Z) (Zl : list Z) := match Zl with
|nil => cons x nil
|cons t q => if Zeq_bool x t then Zl
             else if Zlt_bool x t then cons x Zl else cons t (inserer x q)
end.

Definition sortWithoutDup := fold_right inserer nil.

Definition create (Zl : list Z) : t := unsafe_create (sortWithoutDup Zl).

(************************************)
(** * Construction of the invariant *)
(************************************)

(** OBJECTIVE : Lemma create_inv : forall l, Inv_t (create l). (4)   **)

           (**  Proof.                                               **)
           
           (**  unfold create. intro l.                              **)
           
           (**  apply unsafe_create_inv.          (1)                **)
           
           (**  apply sortWithoutDup_Sorted.      (2)                **)
           
           (**  apply sortWithoutDup_NoDup.       (3)                **)
           
           (**  Qed.                                                 **)

(**************************************)
(** *** (1) : unsafe_create_inv.      *)
(**************************************)

(***********************************)
(** **** unsafe_create_inv_domain  *)
(***********************************)

Theorem elt_list_make_inv : 
forall l a b j, Sorted l -> NoDup l -> j <= a -> a <= b ->
(forall x, In x l -> b+1 <= x) -> Inv_elt_list j (make a b l).
Proof.
induction l;intros a0 b0 j H1 H2 H3 H4 H5; simpl.
- constructor ; auto. constructor.
- intros. simpl. inversion H2. case_eq (Zeq_bool a (b0 + 1)); intro Heq.
    * apply IHl; auto.
        + inversion H1. apply Sorted_nil. apply H10.
        + lia.
        + intros x0 H8.
          assert(In x0 (a :: l) -> b0 + 1 <= x0)
            by (apply H5 with (x:=x0)).
          apply Zeq_bool_eq in Heq. rewrite <- Heq in H9. inversion H.
          assert(b0 + 1 <= a) by lia.
          assert(a<>x0).
          apply th1 with (l:=l); try  assumption.  
          rewrite <- Heq. apply simplIn with (a:=a) in H8. 
          apply H9 in H8. lia.
    * apply invCons; auto.
      apply IHl; auto.
        + inversion H1. apply Sorted_nil. apply H10.
        + apply Zeq_bool_neq in Heq.
          assert(In a (a :: l) -> b0 + 1 <= a). apply H5 with (x:=a).     
          assert(b0 + 1 <= a). apply H8. apply in_eq. lia. 
        + lia.
        + apply Zeq_bool_neq in Heq. intros x0 H8. 
          assert(a<>x0) by (apply th1 with (l:=l); assumption). 
          assert(a<=x0) by (apply th2 with (l:=l); try assumption).  
          lia.
Qed.

Lemma unsafe_create_inv_domain : forall l, Sorted l -> NoDup l -> 
Inv_elt_list (min (unsafe_create l)) (domain (unsafe_create l)).
Proof.
unfold unsafe_create. unfold Inv_t.
induction l;intros Hyp1 Hyp2;simpl.
- apply invNil.
- apply elt_list_make_inv ; try lia.
  apply decreasingSort in Hyp1; assumption.
  inversion Hyp2; assumption.
  intros.
  assert (a <= x) by (apply (th2 l a Hyp1 x H)).
  assert (a <> x) by (apply (th1 l a x Hyp2 H)).
  lia.
Qed.

(**********************************)
(** **** unsafe_create_inv_min    *)
(**********************************)

Definition elt_list_head (val_def : Z) (l : elt_list) := match l with
| Nil => val_def
| Cons a b _ => a
end.

Lemma headMake : forall l a0 b, 
elt_list_head min_int (make a0 b l) = a0.
Proof.
induction l;intros a0 b.
- simpl. reflexivity.
- simpl. case_eq(Zeq_bool a (b + 1));intro Hyp1.
     * apply IHl with (b:=b+1).
     * simpl. reflexivity.
Qed.

Lemma simpl_get_min : forall l, 
get_min l 0 = elt_list_head 0 l. 
Proof.
induction l;simpl;auto.
Qed.

Lemma unsafe_create_inv_min : forall l, Sorted l -> NoDup l -> 
min (unsafe_create l) = get_min (domain (unsafe_create l)) min_int.
Proof.
induction l;intros Hyp1 Hyp2;simpl. 
- reflexivity. 
- assert(elt_list_head min_int (make a a l) = a) as Hyp3
    by (apply headMake). 
  assert(get_min (make a a l) min_int = 
         elt_list_head min_int (make a a l)) as Hyp4 
    by (induction l;simpl;auto). 
  rewrite Hyp4. rewrite Hyp3. reflexivity.
Qed.

(**********************************)
(** **** unsafe_create_inv_max    *)
(**********************************)

Lemma egality_last_and_length_fst : forall l, l<> [] -> 
fstOption (last_and_length l) = last l min_int.
Proof.
induction l;intro Hyp.
- contradiction.
- simpl. case_eq(l);intro b. 
     * auto.
     * intros l0 Hyp4. case_eq (last_and_length (b :: l0)).
          + intros a0 Hyp5. destruct a0. 
            rewrite simplfstOption with (a:=z) (b:=z0+1) (c:=z0). 
            rewrite <- Hyp5. rewrite <- Hyp4. apply IHl. 
            rewrite Hyp4. discriminate.
          + intro Hyp5. rewrite <- Hyp5. rewrite <- Hyp4. apply IHl. 
            rewrite Hyp4. discriminate.
Qed.

Lemma simpl_last_and_length_fst : forall a0 l, l <> [] -> 
fstOption (last_and_length (a0 :: l)) = fstOption (last_and_length (l)).
Proof.
intros b l Hyp.
rewrite egality_last_and_length_fst. 
    * rewrite egality_last_and_length_fst. 
         + apply decreasingLast. assumption.
         + assumption.
    * discriminate.
Qed.

Lemma calculus_make_pm : forall l a b y, a<=b -> 
pm y (make a b l) = last (a::b::l) min_int.
Proof.
induction l;intros z z0 y Hyp.
- auto.
- assert(pm y (if Zeq_bool a (z0 + 1) then make z (z0 + 1) l
               else Cons z z0 (make a a l)) 
       = pm y (make z z0 (a :: l))) as Hypsimpl 
               by (simpl;reflexivity);rewrite <- Hypsimpl.
  case_eq(Zeq_bool a (z0 + 1));intro Hyp2.
    * rewrite IHl with (a:=z) (b:=z0+1). 
          + Zbool_lia. rewrite Hyp2. auto.
          + lia.
    * assert(pm y (Cons z z0 (make a a l)) = pm z0 (make a a l)) as Hypsimpl2 by (simpl;reflexivity);rewrite Hypsimpl2. rewrite IHl. 
          + auto.
          + lia.
Qed.

Lemma relation_rec_max : 
forall a l, l <> nil -> Sorted (a::l) -> NoDup (a::l) -> 
               max (unsafe_create l) = max (unsafe_create (a::l)) 
/\ process_max (domain (unsafe_create l)) = process_max (domain (unsafe_create (a::l))).
Proof.
induction l;intros Hyp1 Hyp2 Hyp3.
- split;contradiction.
- split.
      * unfold unsafe_create. unfold max.
        assert(fstOption (last_and_length (a:: a0 :: l)) = 
               fstOption (last_and_length (a0 :: l))) 
          by (apply simpl_last_and_length_fst 
              with (l:=a0::l) (a0:=a); discriminate).
        rewrite H. reflexivity.
      * unfold unsafe_create. unfold domain. unfold max. induction l.
          + simpl. case_eq(Zeq_bool a0 (a + 1));intro;unfold   
                   process_max;unfold pm;Zbool_lia;lia.
          + unfold process_max. 
            rewrite calculus_make_pm 
              with (l:= a0::a1::l) (b:=a); try lia.
            rewrite calculus_make_pm 
              with (l:= a1::l) (b:=a0); try lia. 
            auto.
Qed.

Lemma unsafe_create_inv_max : forall l, Sorted l -> NoDup l -> 
max (unsafe_create l) = process_max (domain (unsafe_create l)).
Proof.
induction l;intros Hyp1 Hyp2. 
- auto.
- assert(l <> nil -> Sorted (a::l) -> NoDup (a::l) -> 
         max (unsafe_create l) = max (unsafe_create (a::l)) 
         /\ process_max (domain (unsafe_create l)) = 
            process_max (domain (unsafe_create (a::l)))) 
    by (apply relation_rec_max;assumption).
  case_eq(l). 
     * auto.
     * intros z l0 Hyp3. 
       assert(max (unsafe_create l) = max (unsafe_create (a :: l)) 
              /\ process_max (domain (unsafe_create l)) = 
                 process_max (domain (unsafe_create (a :: l)))) as Hyp4.
         apply H. rewrite Hyp3. discriminate. assumption. assumption.
      (* by (apply H;rewrite H0;discriminate;assumption; assumption). *)
       decompose [and] Hyp4. 
       rewrite <- Hyp3. rewrite <- H0. rewrite <- H1. 
       apply decreasingSort in Hyp1. 
       apply decreasingNoDup in Hyp2.
       rewrite IHl;(reflexivity||assumption).
Qed.

(**********************************)
(** **** unsafe_create_inv_size   *)
(**********************************)

Lemma last_and_length_diffNone : forall l l0 b, l = b :: l0 -> 
last_and_length (b :: l0) <> None.
Proof.
induction l;intros l0 b Hyp.
- inversion Hyp.
- simpl. case_eq(l0);intro z. 
     * discriminate.
     * intros l1 Hyp2. 
       case_eq(last_and_length (z :: l1));intro p.
         + intro Hyp3. destruct p. discriminate.
         + rewrite <- p at 1. apply IHl with (l0:=l1) (b:=z).
           congruence. 
Qed.

Lemma egality_last_and_length_snd : forall l, l<> [] -> 
sndOption (last_and_length l) = Zlength l.
Proof.
induction l;intro Hyp.
- contradiction.
- simpl. case_eq(l);intro b. 
     * simpl. reflexivity.
     * intros l0 Hyp4. case_eq (last_and_length (b :: l0)).
          + intros a0 Hyp5. destruct a0. simpl. 
            replace z0 with (sndOption (last_and_length (b :: l0))).
               rewrite Hyp4 in IHl. rewrite IHl. 
                   symmetry. rewrite Zlength_cons. lia. discriminate.
            rewrite Hyp5. simpl. reflexivity.
          + intro Hyp5. pose proof (last_and_length_diffNone _ _ _ Hyp4).
            contradiction.
Qed.

Lemma simpl_last_and_length_snd : forall a0 l, l <> [] -> 
sndOption (last_and_length (a0 :: l)) = 
sndOption (last_and_length (l)) + 1.
Proof.
intros b l Hyp.
rewrite egality_last_and_length_snd. 
   * rewrite egality_last_and_length_snd;try assumption. 
     rewrite Zlength_cons. lia.
   * discriminate.
Qed.

Lemma calculus_make_ps : forall l a b y, a<=b -> 
ps y (make a b l) = (b-a+1) + y + Zlength l.
Proof.
induction l;intros z z0 y Hyp;simpl.
- rewrite Zlength_nil. lia.
- case_eq(Zeq_bool a (z0 + 1));intro Hyp2.
    * rewrite IHl with (a:=z) (b:=z0+1). 
        + rewrite Zlength_cons. lia. 
        + lia.
    * rewrite Zlength_cons. simpl. rewrite IHl;lia.
Qed.

Lemma relation_rec_size : 
forall a l, Sorted (a::l) -> NoDup (a::l) -> 
size (unsafe_create l) + 1 = size (unsafe_create (a::l))
/\ process_size (domain (unsafe_create l)) + 1 = process_size (domain (unsafe_create (a::l))).
Proof.
induction l;intros Hyp1 Hyp2.
- split.
      * auto.
      * simpl. unfold process_size. unfold ps. lia.
- split.
      * unfold unsafe_create. unfold size.
        assert(sndOption (last_and_length (a:: a0 :: l)) 
             = sndOption (last_and_length (a0 :: l)) + 1) 
          by (apply simpl_last_and_length_snd 
                 with (l:=a0::l);discriminate).
        rewrite H. reflexivity.
      * unfold unsafe_create. unfold domain. unfold size.
        case_eq(l).
          + intro Hyp3. simpl. case_eq(Zeq_bool a0 (a + 1));intro;unfold process_size;unfold ps;lia.
          + intros a1 l0 Hyp3. unfold process_size. 
            rewrite calculus_make_ps with (l:= a0::a1::l0) (b:=a);
              try lia.
            rewrite calculus_make_ps with (l:= a1::l0) (b:=a0);
              try lia. 
            repeat rewrite Zlength_cons;lia.
Qed.

Lemma unsafe_create_inv_size : forall l, Sorted l -> NoDup l -> 
size (unsafe_create l) = process_size (domain (unsafe_create l)).
Proof.
induction l;intros Hyp1 Hyp2.
- auto.
- assert(Sorted (a::l) -> NoDup (a::l) -> 
         size (unsafe_create l) + 1 = size (unsafe_create (a::l))
         /\ process_size (domain (unsafe_create l)) + 1 = 
            process_size (domain (unsafe_create (a::l)))) 
     by (apply relation_rec_size;assumption).

  assert(size (unsafe_create l) + 1 = size (unsafe_create (a :: l)) 
         /\ process_size (domain (unsafe_create l)) + 1 = 
            process_size (domain (unsafe_create (a :: l)))) 
     by (apply H; assumption).
     
  decompose [and] H0. 
  rewrite <- H1. rewrite <- H2. 
  apply decreasingSort in Hyp1. 
  apply decreasingNoDup in Hyp2.
  rewrite IHl;(reflexivity||assumption).
Qed.

(****************************)
(** **** unsafe_create_inv  *)
(****************************)

Theorem unsafe_create_inv : forall l, 
Sorted l -> NoDup l -> Inv_t (unsafe_create l).
Proof.
intros l Hyp1 Hyp2. unfold Inv_t. repeat split.
- apply unsafe_create_inv_domain ;assumption.
- apply unsafe_create_inv_min    ;assumption.
- apply unsafe_create_inv_max    ;assumption.
- apply unsafe_create_inv_size   ;assumption.
Qed.

(*****************************************************************)
(** *** (2) : sortWithoutDup_Sorted                              *)
(*****************************************************************)

Lemma inserer_SameElt : 
forall h t, SameElt (inserer h t) (h :: t).
Proof.
intros. induction t as [|h' t'].
+ simpl. unfold SameElt. intro x. split; intro H1; assumption.
+ simpl. case_eq (Zeq_bool h h'). 
   * intro H1. unfold SameElt. intro x. split; intro H2. 
       - apply in_cons. assumption.       
       - apply in_inv in H2. decompose [or] H2. 
           Zbool_lia. rewrite <- H1. unfold In. left. assumption.
           assumption.
   * case_eq (h <? h'); intros H1 H2.
       - unfold SameElt. split; intro H; assumption. 
       - unfold SameElt. split; intro H.
           simpl in H. decompose [or] H.
               unfold In. right. left. assumption.
               unfold SameElt in IHt'. apply IHt' in H0. unfold In in H0.  
               decompose [or] H0.
                   unfold In. left. assumption.
                   apply in_cons. apply in_cons. unfold In. assumption.
           simpl in H. decompose [or] H;simpl. 
               right. apply IHt'. simpl. left. assumption. 
               left. assumption. 
               right. apply IHt'. apply in_cons. assumption.
Qed.

Theorem SameElt_sortWithoutDup : 
forall l, SameElt l (sortWithoutDup l).
Proof.
  intros. induction l as [|h t].
  + simpl. unfold SameElt. split; intro H; assumption.
  + assert(SameElt (h :: t) (h :: sortWithoutDup t)) 
      by (apply SameElt_skip ; assumption).
    simpl.
    apply SameElt_trans with (l':= (h :: sortWithoutDup t));
      try assumption. 
    apply SameElt_sym. apply inserer_SameElt.
Qed.

Lemma sortWithoutDup_preserve : 
forall e l, Sorted l -> Sorted (inserer e l).
Proof.
intros e l h. induction h; simpl. 
+ apply Sorted_singleton.
+ remember (Zeq_bool e e0) as bh2. symmetry in Heqbh2. destruct bh2.
   * constructor.
   * remember (Zle_bool e e0) as bh. symmetry in Heqbh. destruct bh.
       - assert( e <? e0 = true) as Hyp
           by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp.
         apply Sorted_cons. apply Sorted_singleton. 
         Zbool_lia. assumption.
       - assert( e <? e0 = false) as Hyp 
           by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp.  
         apply Sorted_cons. apply Sorted_singleton. 
         Zbool_lia;lia.
+ remember (Zeq_bool e e0) as bh2. symmetry in Heqbh2. destruct bh2.
   * constructor;assumption.
   * remember (Zle_bool e e0) as bh. symmetry in Heqbh. destruct bh.
       - assert( e <? e0 = true) as Hyp
           by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp. 
         apply Sorted_cons. apply Sorted_cons;assumption.
         Zbool_lia;lia.
       - assert( e <? e0 = false) as Hyp 
           by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp.  
         remember (Zeq_bool e h) as bh3. symmetry in Heqbh3. 
         destruct bh3. 
         apply Sorted_cons;assumption. 
         case_eq(e <? h); intro H2.
             repeat apply Sorted_cons;(assumption||Zbool_lia;lia).
             
             apply Sorted_cons;try lia. simpl in IHh. 
             rewrite Heqbh3 in IHh. rewrite H2 in IHh. auto.
Qed.

Theorem sortWithoutDup_Sorted : 
forall l, Sorted (sortWithoutDup l).
Proof.
  intros. induction l as [|h t];simpl.
  + apply Sorted_nil.
  + apply sortWithoutDup_preserve;assumption.
Qed.

(**************************************)
(** *** (3): sortWithoutDup_NoDup     *)
(**************************************)

Lemma sortWithoutDup_preserve_NoDup : forall e l,
  Sorted l -> NoDup l -> NoDup (inserer e l).
Proof.
induction l;intros Hyp1 Hyp2.
- apply NoDup_cons. unfold In. lia. apply NoDup_nil.
- simpl. case_eq(Zeq_bool e a);intro Hyp3.
   * assumption.
   * case_eq(e <? a);intro Hyp4.
       + constructor.
         unfold not. intro Hyp5. simpl in Hyp5. decompose [or] Hyp5.
             Zbool_lia;lia.
             Zbool_lia. apply th2 with (x:=e) in Hyp1. 
             lia. assumption. 
         assumption.
       + constructor.
         intro Hyp5. apply inserer_SameElt in Hyp5. simpl in Hyp5. 
         decompose [or] Hyp5. 
              Zbool_lia;lia. 
              inversion Hyp2. contradiction.
         apply IHl.
               apply decreasingSort in Hyp1;assumption.
               inversion Hyp2. assumption.
Qed. 

Theorem sortWithoutDup_NoDup : forall l, NoDup (sortWithoutDup l).
Proof.
induction l;simpl.
- constructor.
- apply sortWithoutDup_preserve_NoDup. 
       * apply sortWithoutDup_Sorted.
       * assumption.
Qed.

Theorem sortWithoutDup_spec : 
forall l, Sorted (sortWithoutDup l) 
       /\ SameElt l (sortWithoutDup l)
       /\ NoDup (sortWithoutDup l).
Proof.
intro l. split. 
  * apply sortWithoutDup_Sorted. 
  * split. apply SameElt_sortWithoutDup.
           apply sortWithoutDup_NoDup .
Qed.
(***********************)
(** *** (4) : Result   *)
(***********************)

Theorem create_inv : forall l, Inv_t (create l).
Proof.
unfold create. intro l.
apply unsafe_create_inv.
apply sortWithoutDup_Sorted.
apply sortWithoutDup_NoDup.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Lemma elt_list_member_make : forall l a b, a <= b -> 
forall y, elt_list_member y (make a b l) = true -> 
a <= y <= b \/ In y l.
Proof.
induction l ; simpl.
 + intros a b Hab y. case_eq (y <=? b) ; intros.
    - Zbool_lia ; lia. 
    - discriminate H0.
 + intros a0 h Ha0b y.
   case_eq (Zeq_bool a (h + 1)) ; intro Hcase.
    - intro Hyp. Zbool_lia. apply IHl in Hyp;try lia. 
      destruct Hyp.
         * assert (a0 <= y <= h \/ y = h+1) by lia.
           subst. destruct H0; auto.
         * tauto.
    - simpl. case_eq (y <=? h) ; intros Hcase1 Hyp; Zbool_lia.
         * left. lia.
         * apply IHl in Hyp; try lia.
           destruct Hyp;[right;left;lia|tauto;lia].
Qed.

Lemma elt_list_member_make_reverse : forall l a b,
Sorted l ->
NoDup l ->
a <= b ->
(forall x : Z, In x l -> b + 1 <= x) ->
forall y, a <= y <= b \/ In y l ->
elt_list_member y (make a b l) = true .
Proof.
induction l ; simpl.
 + intros a b Hsorted HnoDup Hab Hl y Hyp. 
   destruct Hyp ; try contradiction.
   case_eq (y <=? b) ; intros;Zbool_lia_goal;Zbool_lia;lia.
 + intros a0 b Hsorted HnoDup  Ha0b Hal y Hyp.
   pose proof (decreasingSort _ _  Hsorted) as Hsorted1.
   inversion HnoDup.
   case_eq (Zeq_bool a (b + 1)) ; intro Hcase.
     * assert (forall x0 : Z, In x0 l -> b + 1 + 1 <= x0) as Hinl.
       Zbool_lia. intros.
       assert(In x0 (a :: l) -> b + 1 <= x0)
         by apply Hal with (x:=x0).
       rewrite <- Hcase in H4. assert(b + 1 <= a) by lia.
       assert(a<>x0).
       apply th1 with (l:=l); try  assumption.  
       rewrite <- Hcase. apply simplIn with (a:=a) in H3. 
       apply H4 in H3. lia. decompose [or] Hyp.
        - apply IHl ; auto. lia. left ; lia.
        - Zbool_lia. apply IHl ; auto. lia. left. lia.
        - apply IHl ; auto. lia.
     * simpl. case_eq (y <=? b) ; intro Hcase1.
        - decompose [or] Hyp.
            Zbool_lia;Zbool_lia_goal;lia.
            assert (b+1 <= y) by auto.
            Zbool_lia;Zbool_lia_goal;lia.
            assert (b+1 <= y) by auto.
            Zbool_lia;Zbool_lia_goal;lia.
        - Zbool_lia.
          assert (forall x0 : Z, In x0 l -> a + 1 <= x0) 
            as Hinl.
            intros.
            assert(a<>x0). apply th1 with (l:=l);assumption.
            assert(a<=x0). apply th2 with (l:=l);try assumption. 
          lia.
          decompose [or] Hyp. 
            lia.
            apply IHl ; auto. lia. left ; lia.
            apply IHl ; auto. lia.
Qed.

Lemma elt_list_member_unsafe_create : forall (l : list Z) (y : Z),
Sorted l -> NoDup l ->
elt_list_member y
  (domain (unsafe_create l)) = true <-> In y l.
Proof.
induction l;intros ; simpl.
 + split. intro Hpb ; discriminate Hpb. tauto.
 + split ; intro Hyp.
    - apply elt_list_member_make in Hyp; try lia.
      destruct Hyp;[left|tauto];lia.
    - apply elt_list_member_make_reverse;try lia.
        * apply decreasingSort with (a:=a); assumption.
        * inversion H0; assumption. 
        * intros.
          assert (a <= x) by (exact (th2 l a H x H1)).
          assert (a <> x) by (exact (th1 l a x H0 H1)). lia.
        * destruct Hyp;[left;lia|tauto].
Qed.

Theorem create_spec : forall l, 
(forall y, member y  (create l) = true <-> In y l).
Proof.
unfold create. unfold member.
intros.
pose proof (sortWithoutDup_NoDup l) as HNodup.
pose proof (sortWithoutDup_Sorted l) as Hsorted.
pose proof (elt_list_member_unsafe_create (sortWithoutDup l) y Hsorted HNodup).
pose proof (SameElt_sortWithoutDup l y) as HSameElty.
tauto.
Qed.
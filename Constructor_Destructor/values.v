Require Import R.Prelude.tactics R.Prelude.xStandardLib R.Prelude.structure.
Require Import Recdef List ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Function enum_and_conc (min_max_tail : ((Z*Z)*list Z)) {measure minus_triplet }:=
match min_max_tail with
( (min, max), tail) =>
if Zgt_bool min max then tail
else if Zeq_bool min max then 
     cons min tail
     else cons min (enum_and_conc ((min+1,max), tail))
end.
Proof.
intros.
Zbool_lia. apply Zabs_nat_lt.
lia.
Defined.

Fixpoint elt_list_values (l : elt_list) := match l with
 | Nil => nil
 | Cons min max q => enum_and_conc (min, max, (elt_list_values q))
end.

Definition values (d : t) := elt_list_values (domain d).

(************************************)
(** * Specification                 *)
(************************************)



(********************************)
(** * Lemmas on enum_and_conc   *)
(********************************)
Lemma elim_enum_and_conc : forall triple z z0 l x,
In x (enum_and_conc triple ) -> triple = ((z, z0), l) -> 
(z <= x /\ x <= z0) \/ In x l.
Proof.
intros triple .
functional induction (enum_and_conc triple ).
+ intros. inversion H0.  right;congruence.
+ intros. inversion H0.
  simpl in H; destruct H. left. subst. Zbool_lia;lia.
  right ; congruence.
+ intros. inversion H0. simpl in H; destruct H. 
  subst. left. Zbool_lia;lia.
  assert (z + 1 <= x <= z0 \/ In x l). 
  apply IHl with(z:=z+1) (z0:=z0) (l:=l) (x:=x).
  assumption. subst; auto.
  destruct H1. left;lia. tauto.
Qed.

Lemma intro_enum_and_conc : forall triple z z0 l x,
triple = ((z, z0), l) -> (z <= x /\ x <= z0) \/ In x l -> 
In x (enum_and_conc triple ).
Proof.
intros triple.
functional induction (enum_and_conc triple );intros z z0 l0 x Hyp1 Hyp2.
+ inversion Hyp1. decompose [or] Hyp2. 
    * subst. Zbool_lia;lia.
    * assumption.
+ inversion Hyp1. decompose [or] Hyp2;simpl. 
    * left. Zbool_lia;lia.
    * right. assumption. 
+ inversion Hyp1.  decompose [or] Hyp2.
    * apply decompose_ineg in H. decompose [and] H. 
      decompose [or] H4;simpl. 
        - left. assumption.
        - right. subst. 
          apply IHl with (z1:=z+1) (z2:=z0) (l:=l0) (x:=x). 
          reflexivity. left. assumption.
    * simpl. right. subst. apply IHl with(z1:=z+1) (z2:=z0) (l:=l0).
     reflexivity. right. assumption.
Qed.

Lemma enum_and_conc_diffNil : forall z z0 l, 
z <= z0 -> enum_and_conc (z, z0, l) <> nil.
Proof.
intros z z0 l Hyp. rewrite enum_and_conc_equation. 
case_eq(z >? z0);intro Hyp2.
 - Zbool_lia;lia.
 - case_eq(Zeq_bool z z0);intro Hyp3;discriminate.
Qed.

(*********************************************)
(** * Lemmas on elt_list_values and values   *)
(*********************************************)
Lemma elt_list_values_diffNil : forall l y, Inv_elt_list y l -> 
elt_list_values l = nil -> l = Nil.
Proof.
intros l y Hyp1 Hyp2. case_eq(l);intros. 
- reflexivity.
- rewrite H in Hyp1. inversion Hyp1.
  rewrite H in Hyp2. simpl in Hyp2.
  assert(enum_and_conc (z, z0, elt_list_values e) <> nil) 
     by (apply enum_and_conc_diffNil; assumption). 
  contradiction.
Qed.


(*****************************************************************)
(** * Properties of enum_and_conc, elt_list_values and values    *)
(*****************************************************************)

(*********************************)
(** ** enum_and_conc is sorted   *)
(*********************************)
Theorem enum_and_conc_preserve_Sorted : forall triple l z z0, 
Sorted l -> (forall x, In x l -> z0 <= x) -> triple = (z,z0,l) -> 
Sorted (enum_and_conc triple).
Proof.
intros triple.
functional induction (enum_and_conc triple).
- intros. inversion H1. auto.
- intros. inversion H1. apply sortedCons. assumption.
  Zbool_lia. rewrite H3 in e1. rewrite H4 in e1. 
  rewrite <- e1 in H0. exact H0.
- intros. inversion H1. apply sortedCons.
  * subst. apply IHl with (z1:=z+1) (z2:=z0) (l0:=l);auto.
  * intros x Hyp_x. pose proof (elim_enum_and_conc (z + 1, z0, l) (z+1) z0 l x Hyp_x) (eq_refl (z + 1, z0, l)) as Hyp2. 
    decompose [or] Hyp2.
       + lia.
       + subst. assert(z0 <= x) by (apply H0; assumption).
         Zbool_lia; lia.
Qed.

(***********************************)
(** ** elt_list_values is sorted   *)
(***********************************)
Lemma in_enum_and_conc_le : forall triple l z z0, 
(forall x, In x l -> z0 <= x) -> z <= z0 -> triple = (z,z0,l) -> 
forall x, In x (enum_and_conc triple) -> z <= x.
Proof.
intros triple.
functional induction (enum_and_conc triple).
- intros. inversion H1. subst. Zbool_lia;lia.
- intros. inversion H1. subst. simpl in H2. decompose [or] H2.
      * lia.
      * Zbool_lia. rewrite e1. apply H. assumption.
- intros. inversion H1. simpl in H2. decompose [or] H2;subst. 
      * lia.
      * assert(z+1<=x) by (apply IHl with (l0:=l) (z1:=z+1) (z2:=z0); 
          try (assumption||reflexivity||Zbool_lia;lia)).
        lia.
Qed.

Lemma in_elt_list_values_le : forall r y z z0, 
Inv_elt_list y (Cons z z0 r) -> 
forall x, In x (elt_list_values r) -> z0 <= x.
Proof.
induction r;intros y b c Hyp1 x Hyp3.
- simpl in Hyp3. contradiction.
- simpl in Hyp3. apply in_enum_and_conc_le with (triple:=(c, z0, elt_list_values r)) (l:=elt_list_values r) (z0:=z0).
    * apply IHr with (y:=c+2) (z:=z). inversion Hyp1. assumption.
    * inversion Hyp1. inversion H5. lia.
    * reflexivity.
    * pose proof ((elim_enum_and_conc (z, z0, elt_list_values r)) z z0 (elt_list_values r) x Hyp3 (eq_refl (z, z0, elt_list_values r))).
    remember (c, z0, elt_list_values r) as triple.
    apply intro_enum_and_conc with (z:=c) (z0:=z0) (l:=elt_list_values r).
    auto.
    decompose [or] H.
      + left. inversion Hyp1. inversion H7. lia.
      + tauto.
Qed.

Theorem elt_list_values_Sorted : forall l y,  
Inv_elt_list y l -> Sorted (elt_list_values l).
Proof.
induction l;intros y Hyp;simpl.
   * constructor.
   * apply enum_and_conc_preserve_Sorted 
       with (l:=elt_list_values l) (z:=z) (z0:=z0).
       - apply IHl with (y:=z0+2).
         inversion Hyp. assumption.
       - intros x Hyp2. 
         apply in_elt_list_values_le 
           with (r:=l) (y:=y) (z:=z);assumption.
       - reflexivity.
Qed.

(**************************)
(** ** values is sorted   *)
(**************************)
Theorem values_Sorted : forall d, Inv_t d -> Sorted (values d).
Proof.
intros d Hd. unfold Inv_t in Hd. decompose [and] Hd.
apply elt_list_values_Sorted in H. auto.
Qed.

(**********************************************)
(** ** enum_and_conc is without duplicate     *)
(**********************************************)
Lemma in_elt_list_values_le_2 : forall l z0, 
Inv_elt_list (z0 + 2) l -> 
forall x, In x (elt_list_values l) -> z0 + 2 <= x.
Proof.
induction l;intros a Hyp1 x Hyp2.
- simpl in Hyp2. contradiction.
- simpl in Hyp2. 
  apply in_enum_and_conc_le 
    with (triple:=(a+2, z0, elt_list_values l)) 
         (l:=elt_list_values l) (z0:=z0).
    * intros x0 Hyp3. inversion Hyp1.
      assert(z0+2<=x0) 
        by (apply IHl with (z0:=z0);assumption).
      lia.
    * inversion Hyp1. lia.
    * reflexivity.
    * pose proof ((elim_enum_and_conc (z, z0, elt_list_values l)) z z0 (elt_list_values l) x Hyp2 (eq_refl (z, z0, elt_list_values l))).
    remember (a+2, z0, elt_list_values l) as triple.
    apply intro_enum_and_conc 
      with (z:=a+2) (z0:=z0) (l:=elt_list_values l).
      auto.
      decompose [or] H.
         + left. inversion Hyp1. inversion H7;lia.
         + tauto.
Qed.

Theorem enum_and_conc_preserve_NoDup : forall triple z z0 l,
Sorted l -> NoDup l -> (forall x, In x l -> z0 <= x) -> 
~ In z0 l ->triple = ((z, z0), l) -> NoDup (enum_and_conc triple).
Proof.
intros triple.
functional induction (enum_and_conc triple);
intros z z0 l Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
- inversion Hyp5. assumption.
- inversion Hyp5. subst. constructor.  
     * Zbool_lia. rewrite e1. assumption. 
     * assumption.
- inversion Hyp5. 
  assert(NoDup (enum_and_conc (min + 1, max, tail))) as Hyp6
     by (apply IHl with (z:=z+1) (z0:=z0) (l:=l);
            (assumption||subst;reflexivity)).
  subst. constructor.
     * intro Hyp7.
       apply elim_enum_and_conc with (z:=z+1) (z0:=z0) (l:=l) in Hyp7.
       decompose [or] Hyp7.
          + lia.
          + apply Hyp3 in H. Zbool_lia;lia.
          + reflexivity.
     * assumption.
Qed.

(***********************************************)
(** ** elt_list_values is without duplicate    *)
(***********************************************)
Theorem elt_list_values_NoDup : forall l y, 
Inv_elt_list y l -> NoDup (elt_list_values l).
Proof.
induction l;intros y Hyp;simpl.
- constructor.
- apply enum_and_conc_preserve_NoDup 
   with (z:=z) (z0:=z0) (l:=elt_list_values l).
     * apply elt_list_values_Sorted with (y:=z0+2).
       inversion Hyp. assumption.
     * apply IHl with (y:=z0+2).
       inversion Hyp. assumption.
     * intro. apply in_elt_list_values_le with (y:=y) (z:=z). assumption.
     * inversion Hyp. intro. 
       apply in_elt_list_values_le_2 with (z0:=z0) in H6. 
       lia. assumption.
     * reflexivity.
Qed.

(**************************************)
(** ** values is without duplicate    *)
(**************************************)
Theorem values_NoDup : forall d,  Inv_t d -> NoDup (values d).
Proof.
intros d Hd. unfold Inv_t in Hd. decompose [and] Hd.
apply elt_list_values_NoDup in H. auto.
Qed.
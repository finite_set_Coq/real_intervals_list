Require Import R.Prelude.tactics R.Prelude.structure R.CD.values.
Require Import List ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition interval_unsafe (min max : Z) : t :=
           mk_t (Cons min max Nil) (max - min + 1) max min.

Definition interval (min max : Z) := if Zgt_bool min max then None
                                     else Some (interval_unsafe min max).

(************************************)
(** * Construction of the invariant *)
(************************************)
Lemma interval_Some_le :  forall n p e, 
(interval n p) = Some e -> n <= p.
Proof. 
intros n p e. unfold interval. 
case_eq (n>?p) ; intros Heq H1.
- discriminate.
- Zbool_lia. assumption.
Qed.

Lemma interval_inv_domain : forall n p e, 
(interval n p) = Some e -> (domain e) = Cons n p Nil.
Proof.
intros n p e H2. unfold interval in H2. 
case (n>?p) in H2.
 - discriminate H2.
 - unfold interval_unsafe in H2. injection H2. intro H3. 
   rewrite <- H3. unfold domain. reflexivity.
Qed.

Lemma interval_inv_min : forall n p e, 
(interval n p) = Some e -> min e = n.
Proof.
intros n p e H2. unfold interval in H2. 
case (n>?p) in H2.
 - discriminate H2.
 - unfold interval_unsafe in H2. injection H2. intro H3. 
   rewrite <- H3. unfold min. reflexivity.
Qed.

Lemma interval_inv_max : forall n p e, 
(interval n p) = Some e -> max e = p.
Proof.
intros n p e H2. unfold interval in H2. 
case (n>?p) in H2.
 - discriminate H2.
 - unfold interval_unsafe in H2. injection H2. intro H3. 
   rewrite <- H3. unfold max. reflexivity.
Qed.

Lemma interval_inv_size : forall n p e, 
(interval n p) = Some e -> size e = p-n+1.
Proof.
intros n p e H2. unfold interval in H2. 
case (n>?p) in H2.
 - discriminate H2.
 - unfold interval_unsafe in H2. injection H2. intro H3. 
   rewrite <- H3. unfold size. reflexivity.
Qed.

Theorem interval_inv : forall n p e, 
(interval n p) = Some e -> Inv_t e.
Proof.
intros n p e H2. unfold Inv_t. 
rewrite (interval_inv_domain n p) ; auto. repeat split.
- rewrite (interval_inv_min n p); auto.
  apply invCons.
    * lia.
    * apply interval_Some_le with (e := e); assumption.
    * apply invNil.
- rewrite (interval_inv_min n p);  auto.
- rewrite (interval_inv_max n p);  auto.
- rewrite (interval_inv_size n p); auto.
  unfold process_size. unfold ps. lia.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Theorem interval_spec : forall d n p, interval n p = Some d ->
forall x, In x (values d) -> n <= x <= p.
Proof.
intros d n p Hyp1 x Hyp2. unfold interval in Hyp1. 
case (n>?p) in Hyp1.
- discriminate Hyp1.
- unfold interval_unsafe in Hyp1. injection Hyp1. intro Hyp3. 
  rewrite <- Hyp3 in Hyp2. unfold values in Hyp2. simpl in Hyp2.
  apply elim_enum_and_conc with (z:=n) (z0:=p) (l:=nil) in Hyp2.
  decompose [or] Hyp2.
     + assumption.
     + simpl in H. contradiction.
     + reflexivity.
Qed.
Require Import R.Prelude.structure R.CD.values.
Require Import ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition boolean : t :=  mk_t (Cons 0 1 Nil) 2 1 0.

(************************************)
(** * Construction of the invariant *)
(************************************)
Theorem boolean_inv : Inv_t (boolean).
Proof.
unfold Inv_t. unfold boolean. unfold domain. repeat split.
apply invCons. 
    + simpl. lia.
    + lia.
    + apply invNil.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Theorem boolean_spec : values (boolean) = cons 0 (cons 1 nil).
Proof. auto. Qed.
Require Import R.Prelude.structure.
Require Import ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition empty : t := mk_t Nil 0 min_int min_int.

(************************************)
(** * Construction of the invariant *)
(************************************)
Theorem empty_inv : Inv_t (empty).
Proof.
unfold Inv_t. unfold empty. unfold domain. split.
- apply invNil.
- split;tauto.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Lemma equiv_empty_Nil : forall d, Inv_t d -> 
domain d = Nil <-> d = empty.
Proof.
split;intro Hyp.
- unfold Inv_t in H. decompose [and] H. destruct d. unfold empty.
  rewrite Hyp in H2;simpl in H2;rewrite H2.
  rewrite Hyp in H1;unfold process_max in H1;unfold pm in H1;
  simpl in H1;rewrite H1.
  rewrite Hyp in H4;unfold process_size in H4;unfold ps in H4;
  simpl in H4;rewrite H4.  
  simpl in Hyp;rewrite Hyp. reflexivity.
- unfold Inv_t in H. decompose [and] H. unfold empty in Hyp. 
  rewrite Hyp. unfold domain. reflexivity. 
Qed.
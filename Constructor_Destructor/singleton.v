Require Import R.Prelude.tactics R.Prelude.structure R.CD.values R.CD.empty R.CD.interval R.FBool.member.
Require Import ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition singleton a := extractionOption t (interval a a) empty.

(************************************)
(** * Construction of the invariant *)
(************************************)
Theorem singleton_inv : forall x, Inv_t (singleton x).
Proof.
intro x.
unfold singleton. unfold extractionOption.
case_eq(interval x x).
  + intros t0 Hyp. 
    apply interval_inv with (n:=x) (p:=x) ; assumption. 
  + intro Hyp1. apply empty_inv.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Theorem singleton_spec_1 : forall x y, 
member y (singleton x) = true -> x = y.
Proof.
intros x y.
assert(x >? x = false) as Hyp by (Zbool_lia_goal;lia).
unfold singleton. unfold member. unfold interval.
rewrite Hyp;simpl. case_eq(y <=? x);intros.
  + Zbool_lia;lia.
  + inversion H0.
Qed.

Theorem singleton_spec_2 : forall x y,
x = y -> member y (singleton x) = true.
Proof.
intros x y.
assert(x >? x = false) as Hyp by (Zbool_lia_goal;lia).
unfold singleton. unfold member. unfold interval.
rewrite Hyp;simpl. case_eq(y <=? x);intros.
  + Zbool_lia_goal;Zbool_lia;lia.
  + Zbool_lia;lia.
Qed.

(************************************)
(** * Additional lemma              *)
(************************************)
Lemma singleton_size : forall a , 
size (singleton a) = 1.
Proof.
intro a. 
unfold singleton. unfold interval. case_eq(a >? a);intro. 
- Zbool_lia;lia.
- simpl. lia.
Qed.
Require Import R.Prelude.tactics R.Prelude.xStandardLib R.Prelude.structure R.CD.values R.FBool.member.
Require Import Recdef List ZArith Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
(* See Prelude/structure.v too. *)
Definition min_domain (d : t) := min d.

(************************************)
(** * Specification                 *)
(************************************)
Lemma borne_inf : 
forall l z z0 y, Inv_elt_list y (Cons z z0 l) -> 
            forall x, In x (elt_list_values l) -> z0 <= x.
Proof.
induction l;intros z1 z2 y Hyp1 x Hyp2;simpl in Hyp2. 
- contradiction.
- apply elim_enum_and_conc with 
  (l:= elt_list_values l) (z:=z) (z0:=z0) in Hyp2.
  decompose [or] Hyp2.
     * inversion Hyp1. inversion H6. lia.
     * apply IHl with (z0:=z2) (z:=z1) (y:=y);try assumption.
       apply inv_elt_list_simpl with (z0:=z0) (z:=z) in Hyp1;assumption.
     * reflexivity.
Qed.

Theorem min_elt_list_values : forall l y, Inv_elt_list y l -> 
get_min l y = min_list y (elt_list_values l).
Proof.
induction l;intros y Hyp.
- auto.
- simpl. rewrite enum_and_conc_equation. inversion Hyp. 
  case_eq(z >? z0);intros.
     + Zbool_lia;lia. 
     + assert(Sorted (elt_list_values l)) as HypSort 
       by (apply elt_list_values_Sorted with (y:=z0+2);assumption).
        case_eq(Zeq_bool z z0);intros.
           * rewrite mini_sortedList_min_list. auto.
             apply sortedCons. assumption. intros x Hyp2.
             
             assert(get_min l (z0+2) = 
                    min_list (z0+2) (elt_list_values l)) as Hyp4 
               by (apply IHl with (y:=z0+2);assumption).
             
             assert(min_list (z0+2) (elt_list_values l) <= x) as Hyp5
               by (apply mini_def;assumption).
             
             assert(z0<= x) as Hyp6
               by (apply borne_inf 
                   with (l:= l) (z:=z) (z0:=z0) (y:=y);assumption).
             lia.
           * rewrite mini_sortedList_min_list. auto.
             assert(Sorted (enum_and_conc (z + 1, z0, elt_list_values l))).
             
             apply enum_and_conc_preserve_Sorted 
             with (l:=elt_list_values l) (z:=z+1) (z0:=z0).
                assumption. 
                intros x Hyp2. 
                apply borne_inf 
                with (l:= l) (z:=z) (z0:=z0) (y:=y);assumption.
                reflexivity.
                
                apply sortedCons. assumption.
                intros x Hyp2.
                rewrite enum_and_conc_equation in H8.
                  case_eq(z+1 >? z0);intro.
                     Zbool_lia;lia.
                     rewrite H9 in H8. case_eq(Zeq_bool (z + 1) z0);intros.
                       rewrite H10 in H8.
                assert(z+1<=x). 
                   apply mini_sortedList with (x:=x) in H8. assumption.
                
                rewrite enum_and_conc_equation in Hyp2.
                rewrite H9 in Hyp2. rewrite H10 in Hyp2. assumption.
                lia.
                rewrite H10 in H8.
                
                apply mini_sortedList with (x:=x) in H8. lia.
                
                rewrite enum_and_conc_equation in Hyp2.
                rewrite H9 in Hyp2. rewrite H10 in Hyp2. 
                assumption.
Qed.

Lemma egality_min_get_min : forall d, 
Inv_t d -> min d = get_min (domain d) min_int.
Proof.
intros d Hyp. inversion Hyp. decompose [and] H0. assumption.
Qed.

(************************************)
(** * Some lemmas                   *)
(************************************)
Lemma egality_get_min : forall l a b, l <> Nil ->
get_min l a = get_min l b.
Proof.
intro l.
case_eq(l);intros.
  + contradiction.
  + simpl. reflexivity.
Qed.

Lemma list_min_in : forall l y,
Inv_elt_list y l -> l <> Nil -> 
elt_list_member (get_min l min_int) l = true.
Proof.
intro l. case_eq(l).
  + intros HypL y0 Hyp1 Hyp2. contradiction.
  + intros z z0 e HypL y0 Hyp1 Hyp2. simpl.
    case_eq(z <=? z0);intro.
      - Zbool_lia_goal. lia.
      - Zbool_lia.
        inversion Hyp1. lia.
Qed.
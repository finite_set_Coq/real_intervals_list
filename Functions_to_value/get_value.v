Require Import R.Prelude.structure R.FBool.member R.FBool.is_empty R.Prelude.tactics.
Require Import ZArith Lia.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition get_value d := match domain d with
 |Nil => None
 |Cons a b q => Some a
end.

(************************************)
(** * Specification                 *)
(************************************)
Theorem get_value_spec_1 :forall d v, Inv_t d -> 
get_value d = Some v -> member v d = true.
Proof.
intros d v Hd.
case_eq(domain d).
- intros Hyp1 Hyp2.
  unfold get_value in Hyp2. rewrite Hyp1 in Hyp2.
  discriminate Hyp2.
- intros z z0 e Hyp1 Hyp2.
  unfold member. rewrite Hyp1.
  simpl.
  unfold get_value in Hyp2. rewrite Hyp1 in Hyp2.
  injection Hyp2;intro Heq.
  case_eq(v <=? z0);intro Hyp3.
     * Zbool_lia_goal;lia.
     * unfold Inv_t in Hd. rewrite Hyp1 in Hd. decompose [and] Hd.
       inversion H. Zbool_lia;lia.
Qed.

Theorem get_value_spec_2 :forall d, Inv_t d -> 
get_value d = None -> is_empty d = true.
Proof.
intros d Hd.
case_eq(domain d).
- intros Hyp1 Hyp2. 
  rewrite equiv_is_empty_elt_list_is_empty; try assumption.
  rewrite Hyp1. auto.
- intros z z0 e Hyp1 Hyp2.
  unfold get_value in Hyp2. rewrite Hyp1 in Hyp2.
  discriminate Hyp2.
Qed.
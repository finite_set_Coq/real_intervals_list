Require Import R.Prelude.tactics R.Prelude.xStandardLib R.Prelude.structure R.CD.values R.FBool.member.
Require Import Recdef List ZArith Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
(* See Prelude/structure.v too. *)
Definition max_domain (d : t) := structure.max d.

(************************************)
(** * Specification                 *)
(************************************)
Lemma decreasingPM : forall l z z0, l <> Nil -> 
pm min_int (Cons z z0 l) = pm min_int l.
Proof.
induction l;intros.
- contradiction.
- auto. 
Qed.

Lemma decreasingProcess_max : forall l z z0, l <> Nil -> 
process_max (Cons z z0 l) = process_max l.
Proof.
induction l;intros.
- contradiction.
- unfold process_max. apply decreasingPM. assumption. (* OR auto. *)
Qed.

Lemma last_enum_and_conc : forall triple l z z0 y, z <= z0 ->
triple = (z, z0, l) -> 
last (enum_and_conc triple) y = last (z0::l) y.
Proof.
intros triple.
functional induction (enum_and_conc triple).
- intros. inversion H0. subst. Zbool_lia;lia.
- intros. inversion H0. Zbool_lia. subst. auto.
- intros. inversion H0. Zbool_lia. subst.
  rewrite decreasingLast. apply IHl with (l0:=l) (z1:=z+1) (z2:=z0).
  lia.
  reflexivity. 
  apply enum_and_conc_diffNil with (l:=l) (z:=z+1) (z0:=z0). lia.
Qed.

Lemma pm_last_elt_values: forall l y def,
Inv_elt_list y l ->
pm def l = last (elt_list_values l) def.
Proof.
induction l.
+ simpl ; auto.
+ simpl ; intros y def Hyp. inversion Hyp.
  rewrite last_enum_and_conc 
    with (z:=z) (z0:=z0) (l:= elt_list_values l); auto.
  rewrite lastCons.
  apply IHl with (y:=z0+2); auto.
Qed.

Theorem max_elt_list_values : forall l y, Inv_elt_list y l ->
process_max l = max_list min_int (elt_list_values l).
Proof.
intros.
assert(Sorted (elt_list_values l)) as HypSort
     by (apply elt_list_values_Sorted with (y:=y);assumption).
rewrite maxi_sortedList_max_list; auto.
unfold process_max.
apply pm_last_elt_values with (y:=y); auto.
Qed.

Lemma egality_max_process_max : forall d, 
Inv_t d -> structure.max d = process_max (domain d).
Proof.
intros d Hyp. inversion Hyp. decompose [and] H0. assumption.
Qed.

(************************************)
(** * Some lemmas                   *)
(************************************)
Lemma list_max_in : forall l y,
    Inv_elt_list y l -> l <> Nil -> 
    elt_list_member (process_max l) l = true.
Proof.
    intro l. induction l.
      + intros y0 Hyp1 Hyp2. contradiction.
      + intros y0 Hyp1 Hyp2. simpl.
        case_eq(process_max (Cons z z0 l) <=? z0);intro.
          - Zbool_lia_goal;Zbool_lia. inversion Hyp1.
            case_eq(l).
              * intro Hyp3. subst. unfold process_max.
                unfold pm. lia.
              * intros z1 z2 e Hyp3. subst.
                assert(z1 <=? z2 = true) as Hyp3
                  by (inversion H6;Zbool_lia_goal;lia).
                assert(elt_list_member z1 (Cons z1 z2 e) = true)
                  as Hyp4 
                  by (simpl;rewrite Hyp3;Zbool_lia_goal;lia).
               rewrite decreasingProcess_max in H.
               apply prop_maximum with (y:=z0+2) in Hyp4; 
                 (assumption||inversion H6;lia).
               intuition. discriminate H0.
           - case_eq(l);intros.
              * simpl. rewrite H0 in H.
                unfold process_max in H. unfold pm in H. 
                Zbool_lia;lia.
              * rewrite decreasingProcess_max. rewrite <- H0.
                apply IHl with (y:=z0+2).
                inversion Hyp1. assumption.
                rewrite H0. intuition. discriminate H1.
                intuition. discriminate H1.
Qed.
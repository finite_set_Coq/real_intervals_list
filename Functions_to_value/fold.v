Require Import R.Prelude.structure R.Prelude.tactics R.CD.values.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

Section Fold.

  Variable  A : Type.
  Variable f : Z -> A -> A.

  (**********************************)
  (** * Definition of fold_right    *)
  (**********************************)
  Function fold_inter (min_max_e : (Z*Z)*A) {measure (minus_triplet_gen A)}:=
  match min_max_e with
  ( (min, max), e) =>
    if Zgt_bool min max then e
    else if Zeq_bool min max then f min e
         else f min (fold_inter ((min+1,max), e))
  end.
  Proof.
  intros; simpl.
  apply Zabs_nat_lt.
  Zbool_lia ; lia.
  Defined.

  Fixpoint fold_right e l  := match l with
   |  Nil => e
   | Cons a b q => fold_inter  ((a, b),  fold_right e q)
  end.

  (***********************************)
  (** * Specification of fold_right  *)
  (***********************************)
  Lemma fold_enum_and_conc : forall (e : A) triple l z z0,
  triple = (z,z0,l) -> z<=z0->
  fold_inter (z, z0, List.fold_right f e l) =
  List.fold_right f e (enum_and_conc (z, z0, l)).
  Proof.
  intros e triple.
  functional induction (enum_and_conc triple);intros l z z0 Hyp1 Hyp2.
  - inversion Hyp1. Zbool_lia. lia.
  - inversion Hyp1. subst. rewrite fold_inter_equation.
    rewrite e1. rewrite e2. rewrite enum_and_conc_equation.
    rewrite e1. rewrite e2. simpl. reflexivity.
  - inversion Hyp1. subst. rewrite fold_inter_equation.
    rewrite e1. rewrite e2. rewrite enum_and_conc_equation.
    rewrite e1. rewrite e2. simpl. Zbool_lia. rewrite IHl;(reflexivity||lia).
  Qed.

  Lemma fold_elt_list : forall (e : A) (l :elt_list) y, Inv_elt_list y l ->
  fold_right e l = List.fold_right f e (elt_list_values l).
  Proof.
  induction l.
  - auto.
  - intros. simpl. inversion H. rewrite IHl with (y:=z0+2).
      * apply fold_enum_and_conc 
        with (triple:=(z, z0, elt_list_values l));[reflexivity|lia].
      * assumption.
  Qed.

  Theorem fold_right_spec : forall (e : A) (d : t),
  Inv_t d ->
  fold_right e (domain d) = List.fold_right f e (values d).
  Proof.
  intros. unfold values.
  unfold Inv_t in H. decompose [and] H.
  apply fold_elt_list with (y:=min d). assumption.
  Qed.

  (**********************************)
  (** * Definition of fold_left     *)
  (**********************************)
  Function fold_left_inter (min_max_e : (Z*Z)*A) {measure (minus_triplet_gen A)}:=
  match min_max_e with
  ( (min, max), e) =>
    if Zgt_bool min max then e
    else if Zeq_bool min max then f min e
         else fold_left_inter ((min+1, max), f min e)
  end.
  Proof.
  intros; simpl.
  apply Zabs_nat_lt.
  Zbool_lia ; lia.
  Defined.

  Fixpoint fold_left l e := match l with
     Nil => e
  | Cons a b q => fold_left q (fold_left_inter (a,b,e)) 
  end.

  Definition fold_left_domain e d := fold_left (domain d) e.

  (***********************************)
  (** * Specification of fold_left   *)
  (***********************************)
  Lemma fold_left_enum_and_conc : forall triple l z z0 e,
  triple = (z,z0,l) -> z<=z0->
  List.fold_left (fun (a0 : A) (e0 : Z) => f e0 a0)
    l (fold_left_inter (z, z0, e)) =
  List.fold_left (fun (a0 : A) (e0 : Z) => f e0 a0)
    (enum_and_conc triple ) e.
  Proof.
  intros triple.
  functional induction (enum_and_conc triple) ; intros l z z0 e Hyp1 Hyp2.
  - inversion Hyp1. Zbool_lia. lia.
  - inversion Hyp1. subst. simpl.
    rewrite fold_left_inter_equation.
    rewrite e0. rewrite e1. reflexivity.
  - inversion Hyp1. subst. simpl. 
    rewrite fold_left_inter_equation. 
    rewrite e1. rewrite e0. Zbool_lia. 
    rewrite <- IHl with (l0:=l) (z1:=z+1) (z2 := z0) ; (reflexivity||lia).
  Qed.

  Lemma fold_left_elt_list_fold_left : forall (l :elt_list) e y, 
  Inv_elt_list y l ->
  fold_left l e = List.fold_left (fun a e => f e a) (elt_list_values l) e.
  Proof.
  induction l ; auto.
  intros. simpl. inversion H.
  rewrite IHl with (y:=z0+2) ; try assumption.
  apply fold_left_enum_and_conc 
        with (triple:=(z, z0, elt_list_values l));[reflexivity|lia].
  Qed.

  Theorem fold_left_spec : forall (e : A) (d : t),
  Inv_t d ->
  fold_left (domain d) e = List.fold_left (fun a e => f e a) (values d) e. 
  Proof. 
  intros. unfold values.
  unfold Inv_t in H. decompose [and] H.
  apply fold_left_elt_list_fold_left with (y:=min d). assumption.
  Qed.


End Fold.
Require Import R.Prelude.tactics R.Prelude.structure R.CD.values.
Require Import Recdef List ZArith Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
(* See Prelude/structure.v too. *)
Definition size_domain (d : t) := size d.

(************************************)
(** * Specification                 *)
(************************************)
Lemma simplPS: forall l res1 res2, 
res2 + ps res1 l = ps (res2 + res1) l.
Proof.
induction l;intros res1 res2.
- auto.
- simpl. rewrite IHl with (res1:=z0 - z + 1 + res1) (res2:=res2).
  assert(res2 + (z0 - z + 1 + res1) = z0 - z + 1 + (res2 + res1)) 
    as Hyp by (lia); rewrite Hyp;reflexivity.
Qed.

Lemma split_process_size : forall l a b, 
process_size (Cons a b l) =
process_size (Cons a b Nil) + process_size (l).
Proof.
induction l;intros a b.
- unfold process_size. unfold ps. lia.
- rewrite IHl. 
  assert(process_size (Cons a b Nil) = b - a + 1) as Hyp2 
    by (unfold process_size ; unfold ps ; lia);rewrite Hyp2.
  unfold process_size.
  assert(ps 0 (Cons z z0 Nil) = z0 - z + 1) as Hyp3 
    by ( unfold ps ; lia);rewrite Hyp3. 
  simpl.
  assert(z0 - z + 1 + 0 = z0 - z + 1) 
    by (apply Z.add_0_r).
  assert(z0 - z + 1 + ps 0 l = ps (z0 - z +1) l) as Hyp4 
    by (rewrite simplPS ; rewrite Z.add_0_r ; reflexivity);rewrite Hyp4.
  assert(b - a + 1 + (z0 - z + 1) = b - a + 1 + z0 - z + 1) 
    by (lia). 
  assert(b - a + 1 + ps (z0 - z + 1) l = ps (b - a + 1 + z0 - z +1) l) 
    as Hyp5 
    by (rewrite simplPS ; rewrite H0 ; reflexivity);rewrite Hyp5.
  assert(z0 - z + 1 + (b - a + 1 + 0) = b - a + 1 + z0 - z + 1) as Hyp6
    by (lia);rewrite Hyp6;reflexivity.
Qed.

(* Special case of the previous one: simpler to use in practice *)
Lemma split_process_size_simpl : forall l z z0, 
process_size (Cons z z0 l) = z0-z+1 + process_size l.
Proof.
induction l;intros a b.
- unfold process_size. unfold ps. lia.
- unfold process_size. simpl. 
  rewrite simplPS with (res1:=z0 - z + 1 + 0) (res2:=b - a + 1).
  assert(z0 - z + 1 + (b - a + 1 + 0) = b - a + 1 + (z0 - z + 1 + 0))
    as Hyp by (lia);rewrite Hyp;reflexivity.
Qed.

Lemma size_enum_and_conc : forall triple l z z0,
z<= z0 -> 
triple = (z, z0, l) ->
z0-z+1+Zlength l = Zlength (enum_and_conc triple).
Proof.
intros triple.
functional induction (enum_and_conc triple).
- intros l z z0 Hyp1 Hyp2. 
  inversion Hyp2. subst. Zbool_lia;lia.
- intros l z z0 Hyp1 Hyp2.  
  inversion Hyp2. subst. Zbool_lia. rewrite e1.
  rewrite Zlength_cons. lia. 
- intros l z z0 Hyp1 Hyp2.  
  inversion Hyp2. subst. 
  rewrite Zlength_cons.
  rewrite <- IHl with (l0:=l) (z1:=z+1) (z2:=z0).
     * lia.
     * Zbool_lia;lia.
     * reflexivity.
Qed.

Theorem size_elt_list_values : forall l y, Inv_elt_list y l -> 
process_size l = Zlength (elt_list_values l).
Proof.
induction l;intros y Hyp.
- auto.
- simpl. inversion Hyp. 
  rewrite <- size_enum_and_conc 
  with (l:=elt_list_values l) (z:=z) (z0:=z0).
   * rewrite split_process_size_simpl. 
     rewrite IHl with (y:=z0+2);[reflexivity|assumption].
   * lia.
   * reflexivity. 
Qed.

Lemma egality_size_process_size : forall d, 
Inv_t d -> size d = process_size (domain d).
Proof.
intros d Hyp. inversion Hyp. decompose [and] H0. assumption.
Qed.

(************************************)
(** * Some lemmas                   *)
(************************************)
Lemma length_list :
    forall d, Inv_t d -> size d = Zlength (values d).
Proof.
intros d Hd.
rewrite egality_size_process_size.
   * simpl. unfold values.
     apply size_elt_list_values with (y:=min d).
     unfold Inv_t in Hd. decompose [and] Hd. assumption.
   * assumption.
Qed.

Lemma nat_process_size : forall e y, 
Inv_elt_list y e -> process_size e >=0.
Proof.
induction e;intros y Hyp.
- unfold process_size. unfold ps. lia.
- rewrite split_process_size_simpl. inversion Hyp.
  assert(process_size e >= 0) 
    by (apply IHe with (y:=z0+2);assumption).
  lia.
Qed.

(************************************)
(** * Process_size_nat              *)
(************************************)
Lemma always_null_ps : forall l y x, 
Zle_bool 0 x = true ->
Inv_elt_list y l ->
Zle_bool 0 (ps x l) = true.
Proof.
induction l;intros.
* unfold ps. Zbool_lia_goal;Zbool_lia;lia.
* Zbool_lia_goal;Zbool_lia. unfold ps. inversion H0.
  assert(0 <=? (z0 - z + 1 + x)=true) as Hyp
    by (Zbool_lia_goal;lia).
  apply IHl with (y:=y) (x:=z0 - z + 1 + x) in Hyp. Zbool_lia. 
  auto. Inv_monotony (z0+2).
Qed.

Lemma always_null_process_size : forall l y, 
Inv_elt_list y l ->
Zle_bool 0 (process_size l) = true.
Proof.
unfold process_size.
intros l y Hyp.
apply always_null_ps with (y:=y);
   [Zbool_lia_goal;lia|assumption]. 
Qed.

Definition process_size_nat l : nat := Z.to_nat (process_size l).
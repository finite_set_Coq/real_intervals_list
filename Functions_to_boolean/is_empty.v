Require Import R.Prelude.structure R.Prelude.tactics R.CD.empty R.CD.values R.FBool.member R.FValue.size.
Require Import Recdef List ZArith Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition elt_list_is_empty (l : elt_list) := match l with
 | Nil => true
 | Cons _ _ _ => false
end.

Definition is_empty (d : t) := Zeq_bool (size d) 0.

(************************************)
(** * Specification                 *)
(************************************)
Theorem elt_list_is_empty_spec :
forall l, elt_list_is_empty l = true <-> l = Nil.
Proof.
split.
- induction l;intros.
    + reflexivity.
    + simpl in H. discriminate.
- induction l;intros.
    + simpl. reflexivity.
    + simpl in H. discriminate H.
Qed.

Lemma equiv_is_empty_elt_list_is_empty :
forall d, Inv_t d -> 
is_empty d = true <-> elt_list_is_empty (domain d) = true.
Proof.
intros d Hd.
unfold Inv_t in Hd. decompose [and] Hd.
split.
- intro Hyp. case_eq(domain d);intro.
    * auto.
    * intros z0 e Hyp2. unfold is_empty in Hyp.
      rewrite egality_size_process_size in Hyp. 
      rewrite Hyp2 in Hyp. rewrite Hyp2 in H. inversion H.
      simpl in Hyp. rewrite split_process_size_simpl in Hyp.
      Zbool_lia. 
      assert(process_size e >= 0) 
        by (apply nat_process_size with (y:=z0+2);assumption).
      assert(z0 - z + 1 + process_size e > 0) 
        by (lia).
      lia. assumption.
- intro Hyp. apply elt_list_is_empty_spec in Hyp. 
  rewrite equiv_empty_Nil in Hyp;auto. 
  rewrite Hyp;auto.
Qed.

Theorem equiv_empty_values : 
  forall d, Inv_t d -> is_empty d = true <-> values d = nil.
Proof.
intros d Hd.
unfold Inv_t in Hd. decompose [and] Hd.
split.
- intro Hyp.
  apply equiv_is_empty_elt_list_is_empty in Hyp.
  apply elt_list_is_empty_spec in Hyp.
  unfold values. rewrite Hyp. reflexivity. auto.
- intro Hyp. 
  apply equiv_is_empty_elt_list_is_empty. auto.  
  apply elt_list_is_empty_spec.
  unfold values in Hyp. 
  apply elt_list_values_diffNil with (y:=min d) in Hyp;assumption.
Qed.

(************************************)
(** * Some lemmas                   *)
(************************************)
Lemma equiv_elt_list_is_empty_Nil : 
forall e, elt_list_is_empty e = false <-> e <> Nil.
Proof.
unfold not ; split.
- intros. subst. discriminate H.
- intros. case_eq e; intro ; auto. contradiction.
Qed.

Lemma member_empty_is_false : forall d , Inv_t d -> 
is_empty d = true -> forall v, member v d = false.
Proof.
intros d Hd Hempty v.
rewrite equiv_empty_values in Hempty.
  * case_eq (member v d) ; intro Heq;  auto.
    apply member_spec in Heq. 
       + rewrite Hempty in Heq; contradiction.
       + assumption.
  * assumption.
Qed.
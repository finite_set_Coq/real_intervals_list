Require Import R.Prelude.structure R.Prelude.tactics R.Prelude.xStandardLib R.FValue.min R.FBool.member.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Function elt_list_included (l1_l2 : elt_list*elt_list) {measure decreasing_length} := match l1_l2 with
|(Nil,_) => true
|(_,Nil) => false
|(Cons min1 max1 q1 as l1, Cons min2 max2 q2 as l2) =>
   (min1 >=? min2) && (max1 <=? max2) && (elt_list_included (q1,l2))
   || elt_list_included (l1,q2)
end.
Proof.
* intros l1_l2 l1 l2 min1 max1 q1 Hyp1 min2 max2 q2 Hyp2 Hyp3;
    unfold decreasing_length; rewrite <- Hyp1; simpl; lia.
* intros l1_l2 l1 l2 min1 max1 q1 Hyp1 min2 max2 q2 Hyp2 Hyp3;
    unfold decreasing_length; rewrite <- Hyp2; simpl; lia.
Defined.

(* code FaCiLe 
Definition included (d1 d2 : t) :=
(Z.leb (size d1)  (size d2))
&& (Z.leb (structure.max d1)  (structure.max d2))
&& elt_list_included (domain d1,domain d2).
*)

Definition included s1 s2 := 
     elt_list_included (domain s1, domain s2).
 
(************************************)
(** * Construction of the invariant *)
(************************************)
Lemma rewrite_bool_prop : forall q1 q2 min1 max1 min2 max2,
(min1 >=? min2) && (max1 <=? max2) &&
elt_list_included (q1, Cons min2 max2 q2)
|| elt_list_included (Cons min1 max1 q1, q2) = true
<-> 
((min1 >= min2) /\ (max1 <= max2) /\
(elt_list_included (q1, Cons min2 max2 q2) = true)
\/ elt_list_included (Cons min1 max1 q1, q2) = true).
Proof.
split;intro Ht.
- apply orb_true_iff in Ht.
  rewrite andb_true_iff in Ht.  
  rewrite andb_true_iff in Ht.
  decompose [or] Ht.
     * decompose [and] H. Zbool_lia. tauto.
     * tauto.
- apply orb_true_iff.
  rewrite andb_true_iff. 
  rewrite andb_true_iff.
  decompose [or] Ht.
     * decompose [and] H. left. split.
          + split;Zbool_lia_goal;lia.
          + assumption.
     * right;assumption.
Qed.

Lemma elt_list_member_get_min : forall q min max a y,
Inv_elt_list a (Cons min max q) ->
elt_list_member y (Cons min max q) = true ->
get_min (Cons min max q) a <= y.
Proof.
intros. rewrite min_elt_list_values.
  * apply mini_def.
    rewrite <- elt_list_member_spec with (y:=a);assumption.
  * assumption.
Qed.

Lemma elt_list_included_get_min : forall double q1 q2 min1 max1 a y1 y2,
Inv_elt_list y1 (Cons min1 max1 q1) -> Inv_elt_list y2 q2 -> 
double = (Cons min1 max1 q1, q2) ->  
elt_list_included double = true ->
get_min q2 a <= min1.
Proof.
intros double.
functional induction (elt_list_included double).
- intros q1 q2 min1 max1 a y1 y2 Hyp1 Hyp2 Hyp3 Hyp4.
  inversion Hyp3.
- intros q1 q2 min1 max1 a y1 y2 Hyp1 Hyp2 Hyp3 Hyp4.
  discriminate Hyp4.
- intros q3 q4 min0 max0 a y1 y2 Hyp1 Hyp2 Hyp3 Hyp4.
  rewrite rewrite_bool_prop in Hyp4. inversion Hyp3. subst.
  decompose [or] Hyp4.
    + decompose [and] H. simpl. lia.
    + inversion Hyp2.
      case_eq(q2).
         * intro. simpl. rewrite H7 in H. discriminate H.
         * intros.
      assert(get_min q2 a <= min0) as Hyp6
        by (apply IHb0 
        with (q1:=q3) (max1:=max0) (y1:=y1) (y2:=max2+2);
                (assumption||reflexivity)).
      simpl.
      rewrite H7 in H6. inversion H6.
      rewrite H7 in Hyp6. simpl in Hyp6. lia.
Qed.

Theorem elt_list_included_spec_2 : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 -> double = (l1,l2) -> 
elt_list_included double = true ->
(forall x, elt_list_member x l1 = true ->
elt_list_member x l2 = true).
Proof.
intros double.
functional induction (elt_list_included double).
- intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5. 
  inversion Hyp3. subst. simpl in Hyp5. discriminate Hyp5.
- intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  discriminate Hyp4.
- intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  rewrite rewrite_bool_prop in Hyp4. inversion Hyp3. subst.
  decompose [or] Hyp4.
    + simpl in Hyp5. decompose [and] H.
      case_eq(x <=? max1);intro Hyp6;rewrite Hyp6 in Hyp5.
        * simpl.
          assert(x <=? max2 =true) as Hyp7 
            by (Zbool_lia;Zbool_lia_goal;lia).
          rewrite Hyp7. Zbool_lia;Zbool_lia_goal;lia. 
        * inversion Hyp1. 
          apply IHb with (l1:=q1) (y1:=max1 + 2) (y2:=y2);
                (assumption||reflexivity).
    + simpl. inversion Hyp2.
      case_eq(x <=? max2);intro Hyp6. 
         * case_eq(q2).
             intro Hq2. rewrite Hq2 in H. discriminate H. 

             intros.
             assert(get_min (Cons min1 max1 q1) y1 <= x) as Hyp7
                by (apply elt_list_member_get_min;assumption);
                    simpl in Hyp7.
             assert(get_min q2 y1 <= min1) as Hyp8
                by (apply elt_list_included_get_min 
                with (double:=(Cons min1 max1 q1, q2))
                     (q1:=q1) (max1:=max1) (y1:=y1) (y2:=max2+2);
                    (assumption||reflexivity)).
             rewrite H7 in Hyp8. simpl in Hyp8.
             rewrite H7 in Hyp2. inversion Hyp2. inversion H14.
             assert(min1 >=? min2 = true)
                by (Zbool_lia;lia).
             Zbool_lia_goal;Zbool_lia;lia.
         * apply IHb0 
            with (l1:=Cons min1 max1 q1) (y1:=y1) (y2:=max2+2);
              (assumption||reflexivity).
Qed.

Lemma rewrite_bool_prop2 : forall d1 d2,
(size d1 <=? size d2) 
&& (structure.max d1 <=? structure.max d2) 
&& elt_list_included (domain d1, domain d2) = true
<->
((size d1) <= (size d2))
/\ ((structure.max d1)  <= (structure.max d2))
/\ elt_list_included (domain d1,domain d2) = true.
Proof.
split;intro Ht.
- rewrite andb_true_iff in Ht. 
  rewrite andb_true_iff in Ht.
  decompose [and] Ht. Zbool_lia. tauto.
- rewrite andb_true_iff. 
  rewrite andb_true_iff.
  repeat split;Zbool_lia_goal;tauto.
Qed.

Theorem included_spec_2 : forall d1 d2,
Inv_t d1 -> Inv_t d2 -> 
included d1 d2 = true -> 
(forall x, member x d1 = true -> member x d2 = true).
Proof.
intros d1 d2 Hyp1 Hyp2 Hyp3 x Hyp4.
unfold member. unfold member in Hyp4.
unfold Inv_t in Hyp1; decompose [and] Hyp1.
unfold Inv_t in Hyp2; decompose [and] Hyp2.
unfold included in Hyp3. (*rewrite rewrite_bool_prop2 in Hyp3. 
decompose [and] Hyp3.*)
apply elt_list_included_spec_2 
   with (l1:=domain d1) (y1:=min d1) (y2:=min d2) (double:=(domain d1,domain d2));
   (assumption||reflexivity).
Qed.



Lemma elt_list_member_increase : forall min max y q x , 
Inv_elt_list y (Cons min max q)->
elt_list_member x q = true -> 
elt_list_member x (Cons min max q) = true.
Proof.
intros. inversion_clear H. simpl.
destruct (x <=? max) ; try assumption.
Zbool_lia_goal. 
pose proof (elt_list_member_le _ _ _ H3 H0). lia.
Qed.

Theorem elt_list_included_spec_1 : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 -> 
double = (l1,l2) -> 
(forall x, elt_list_member x l1 = true -> 
           elt_list_member x l2 = true) ->
elt_list_included double = true.
Proof.
intros double.
functional induction (elt_list_included double).
- auto. 
- intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. 
  destruct _x. contradiction. 
  inversion Hyp3. subst. inversion_clear Hyp1. 
  assert (elt_list_member z (Cons z z0 _x) = true) as Hyp5.
   simpl. 
   assert (z <=? z0 = true) by (Zbool_lia_goal ; lia).
   rewrite H2 ; Zbool_lia_goal; lia.
  apply Hyp4 in Hyp5. discriminate.
- intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4.
  inversion Hyp3. subst. (*inversion_clear Hyp1. inversion_clear Hyp2.*)
  apply rewrite_bool_prop. 
  elim (Z_le_gt_dec min2 min1) ; intro Hmin1.
  + elim (Z_le_gt_dec max1 max2) ; intro Hmax1.
    * left ; repeat split;  try lia.
      apply IHb with (l1:=q1) (l2 := Cons min2 max2 q2) (y1 := max1 +2) (y2:=y2) ; auto.
      inversion_clear Hyp1; assumption.
      intros x Hxq1. 
      apply elt_list_member_increase with (min:=min1) (max:=max1) (y := y1) in Hxq1; auto.  
    * right. inversion_clear Hyp2.
      elim (Z_lt_ge_dec max2 min1) ; intro Hmax2.
        (**) apply IHb0 with (l1 := Cons min1 max1 q1) (l2 := q2) (y1 := y1) (y2 := max2 + 2); auto.
             intros x Hxq1.
             pose proof (Hyp4 x Hxq1). 
             apply elt_list_member_cons in Hxq1. 
             destruct Hxq1 as [HH1 | HH2] ; apply elt_list_member_cons in H2. 
             destruct H2 as [HH3 | HH4] ; [lia | assumption]. 
             destruct H2 as [HH3 | HH4] . inversion_clear Hyp1. 
             apply elt_list_member_le  with (x:=x) in H1 . lia. 
             apply elt_list_member_le  with (x:=x) in H4; auto . lia.
             assumption.
        (**) assert (elt_list_member (max2 + 1) (Cons min1 max1 q1) =
         true) as Hmem. simpl. case_eq (max2 + 1 <=? max1) ; intro Hle. Zbool_lia_goal ; lia.
         Zbool_lia ; lia.
         specialize (Hyp4 (max2 + 1) Hmem) as HSmax2. 
         apply elt_list_member_cons in HSmax2. destruct HSmax2 as [HH1 | HH2] ; try lia. 
         apply elt_list_member_le  with (x:=max2+1) in H1 ; [lia | assumption] . 
  + inversion_clear Hyp1. inversion_clear Hyp2.
    assert (elt_list_member min1 (Cons min1 max1 q1) =
         true) as Hmem. simpl. case_eq (min1  <=? max1) ; intro Hle. Zbool_lia_goal ; lia.
         Zbool_lia ; lia.
    specialize (Hyp4 min1 Hmem) as HHmin1. 
    apply elt_list_member_cons in HHmin1. destruct HHmin1 as [HH1 | HH2] ; try lia. 
    apply elt_list_member_le  with (x:=min1) in H4 ;  [lia | assumption].
Qed.

(*********************************)
(** * Optimisation of included   *)
(*********************************)
Function elt_list_included_bis (l1_l2 : elt_list*elt_list) {measure decreasing_length} := match l1_l2 with
|(Nil,_) => true
|(_,Nil) => false
|(Cons min1 max1 q1 as l1, Cons min2 max2 q2 as l2) =>
   if (min1 >=? min2) 
   then
     if (max1 <=? max2) then elt_list_included_bis (q1,l2)
     else if (max2 <? min1)  
          then  elt_list_included_bis (l1,q2)
          else false
   else false
end.
Proof.
* intros l1_l2 l1 l2 min1 max1 q1 Hyp1 min2 max2 q2 Hyp2 Hyp3 Hyp4 Hyp5;
    unfold decreasing_length. simpl. lia.
* intros l1_l2 l1 l2 min1 max1 q1 Hyp1 min2 max2 q2 Hyp2 Hyp3 Hyp4 Hyp5 Hyp6;
    unfold decreasing_length ; simpl; lia.
Defined.

Lemma elt_list_included_false_non_subset : 
forall double q1 q2 y1 y2 , 
Inv_elt_list y1 q1 ->
Inv_elt_list y2 q2 ->
double = (q1, q2) -> 
elt_list_included double  = false ->
exists x, elt_list_member x q1 = true /\ elt_list_member x q2 = false.
Proof.
intros double.
functional induction (elt_list_included double) ; intros.
+ discriminate. 
+ inversion H1. 
destruct _x. contradiction. subst . inversion_clear H. 
exists z. simpl. split ; auto.
apply  Zle_is_le_bool in H4. rewrite H4.
Zbool_lia_goal ; lia.
+ inversion H1. rewrite orb_false_iff in H2. destruct H2. subst. 
rewrite andb_false_iff in H2; destruct H2.
- rewrite andb_false_iff in H2; destruct H2.
  * exists min1 ; split ; simpl.
    inversion_clear H. apply  Zle_is_le_bool in H5. rewrite H5. Zbool_lia_goal ; lia.
    case_eq (min1 <=? max2). auto. intro. inversion_clear H0.
    case_eq (elt_list_member min1 q2); intro Heq ; auto.
    apply elt_list_member_le with (y:=max2+2)  in Heq ; auto. Zbool_lia. lia.
  * elim (Z_lt_ge_dec max2 min1) ; intro Hmax2.
    (**) inversion_clear H0. 
         apply IHb0 with (q3:=Cons min1 max1 q1) (q4:=q2) (y1 := y1) (y2 := max2 + 2) in H3 ; auto.
         destruct H3 as [x [Hx1 Hx2]].
         exists x ; split ; auto. inversion_clear H.
         case_eq (elt_list_member x (Cons min2 max2 q2)) ; intro HypFalse ; auto.
         apply elt_list_member_cons in HypFalse.
         destruct HypFalse.
         apply elt_list_member_cons in Hx1. destruct Hx1.
         lia. 
         apply elt_list_member_le with (y := max1 + 2) in H8; auto.
         lia.
         rewrite H in Hx2 ; discriminate.
    (**) inversion_clear H0. 
         exists (max2 + 1) ; split ; simpl.
         case_eq (max2 + 1 <=? max1) ; intro Hle. Zbool_lia_goal ; lia.
         Zbool_lia ; lia.
         replace (max2 + 1 <=? max2) with false.
         case_eq (elt_list_member (max2 + 1) q2) ; intro Hyp ; auto.
         apply elt_list_member_le with (y:=max2 + 2) in Hyp ; auto. lia.
         symmetry ; Zbool_lia_goal ; lia.
- inversion H.
  apply IHb with (q3:= q1) (q4:=Cons min2 max2 q2) (y1 := max1 + 2) (y2 := y2) in H2 ; auto.
  destruct H2 as [x [Hx1 Hx2]]. 
  exists x ; split ; auto. 
  apply elt_list_member_increase with (y:=y1) ; auto. 
Qed.
 
Lemma non_subset_elt_list_included_false : 
forall double q1 q2 y1 y2 , 
Inv_elt_list y1 q1 ->
Inv_elt_list y2 q2 ->
double = (q1, q2) -> 
(exists x, elt_list_member x q1 = true /\ elt_list_member x q2 = false) ->
elt_list_included double  = false.
Proof.
intros.
destruct H2 as [x [Hx1 Hx2]].
case_eq (elt_list_included double) ; intro Heq ; auto.
apply elt_list_included_spec_2 with (l1:=q1) (l2:=q2) (y1 := y1) (y2 := y2) (x:=x) in Heq ; auto.
rewrite Heq in Hx2 ; discriminate.
Qed.

Lemma elt_list_included_false : forall double q1 q2 y1 y2 min1 max1 min2 max2, 
min1 >= min2 -> max1 <= max2 -> min1 <= max1 ->
Inv_elt_list y1 (Cons min1 max1 q1) ->
Inv_elt_list y2 (Cons min2 max2 q2) ->
double = (q1, Cons min2 max2 q2) -> 
elt_list_included double  = false ->
elt_list_included (Cons min1 max1 q1, q2) = false.
Proof.
intros. subst.
inversion H2. 
apply elt_list_included_false_non_subset with (q1:=q1) (q2 := Cons min2 max2 q2) (y1 := max1+2) (y2:=y2) in H5 ; auto.
destruct H5 as [x [Hx1 Hx2]].
assert ((x < min2 \/ x > max2) /\ elt_list_member x q2 = false) as Hyp.
 simpl in Hx2.
 case_eq (x <=? max2 ) ; intro HH ; rewrite HH in Hx2. split. 
 Zbool_lia ;  tauto. 
 case_eq (elt_list_member x q2 ); intro Hyp0 ; auto.
 apply elt_list_member_le with (y:=max2 + 2) in Hyp0 ; auto. Zbool_lia ;  lia.
 inversion H3 ; auto.
 split; auto. Zbool_lia ; lia.
inversion_clear H3.
apply non_subset_elt_list_included_false with (q1 := Cons min1 max1 q1) (q2:=q2) (y1:=y1)(y2 := max2 + 2) ; auto.
destruct Hyp as [[Hyp1 | Hyp2] Hyp3]. 
+ exists x ; split ; auto.
  apply elt_list_member_increase with (y:=y1) ; auto.
+ exists x ; split ; auto.
  apply elt_list_member_increase with (y:=y1) ; auto.
Qed.

Lemma equiv_elt_list_included : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 -> 
double = (l1, l2) ->  
elt_list_included_bis double = elt_list_included double.
Proof.
intros double.
functional induction (elt_list_included_bis double).
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. rewrite elt_list_included_equation. reflexivity.
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. 
  destruct _x ; [ contradiction | rewrite elt_list_included_equation ; reflexivity].
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. 
  rewrite elt_list_included_equation. rewrite e0 ;  rewrite e1 ; simpl.
  inversion Hyp. rewrite <- H0 in inv1. rewrite <- H1 in inv2. inversion_clear inv1 . 
  rewrite IHb  with (l1 := q1) (l2 := Cons min2 max2 q2) (y1 := max1 + 2) (y2 := y2)  ; auto. 
  case_eq(elt_list_included (q1, Cons min2 max2 q2)) ; intro Heq.  auto.
  rewrite orb_false_l ; auto.
  symmetry. Zbool_lia.
  apply elt_list_included_false
     with (y1 := y1) (y2 := y2) (min1 := min1) (min2 := min2) 
          (max2 := max2) (double := (q1, Cons min2 max2 q2)) ; auto.
  constructor ; auto.
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. 
  rewrite elt_list_included_equation. rewrite e0 ;  rewrite e1 ; simpl.
  inversion Hyp. rewrite <- H0 in inv1. rewrite <- H1 in inv2. inversion_clear inv1 . 
  rewrite IHb  with (l1 := Cons min1 max1 q1) (l2 := q2) (y1 := y1) (y2 := y2)  ; auto. 
  constructor ; auto.
  inversion_clear inv2. 
  apply inv_elt_list_monoton with (y:= max2 + 2) . auto. lia.
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. 
  rewrite elt_list_included_equation. rewrite e0 ;  rewrite e1 ; simpl.
  inversion Hyp. rewrite <- H0 in inv1. rewrite <- H1 in inv2. 
  case_eq (elt_list_included (Cons min1 max1 q1, q2)) 
  ; intro Heq ; try congruence. inversion_clear inv2.
  assert (elt_list_member (max2 + 1) (Cons min1 max1 q1) = true) as Hmem. 
    simpl. case_eq (max2 + 1 <=? max1) ; intro Hle. Zbool_lia. Zbool_lia_goal ; lia.
         Zbool_lia ; lia.
  pose proof (elt_list_included_spec_2 _ (Cons min1 max1 q1) q2 _ _ inv1 H3 
  (eq_refl (Cons min1 max1 q1, q2)) Heq (max2 + 1) Hmem). 
  apply elt_list_member_le  with (y:=max2+2) in H4 ; [lia | assumption] . 
+ intros l1 l2 y1 y2 inv1 inv2 Hyp. 
  rewrite elt_list_included_equation. rewrite e0 ;  simpl.
  inversion Hyp. rewrite <- H0 in inv1. rewrite <- H1 in inv2.
  case_eq (elt_list_included (Cons min1 max1 q1, q2)) 
  ; intro Heq ; try congruence. inversion_clear inv2.
  assert (elt_list_member min1 (Cons min1 max1 q1) = true) as Hmem. 
  simpl. case_eq (min1  <=? max1) ; intro Hle.  Zbool_lia_goal ; lia.
  Zbool_lia;  inversion_clear inv1 ; lia.
  pose proof (elt_list_included_spec_2 _ (Cons min1 max1 q1) q2 _ _ inv1 H3 
  (eq_refl (Cons min1 max1 q1, q2)) Heq min1 Hmem). 
  apply elt_list_member_le  with (y:=max2+2) in H4 ; 
      [Zbool_lia ; lia | assumption] . 
Qed.

Definition included_optim s1 s2 := 
     elt_list_included_bis (domain s1, domain s2).
     
Theorem included_optim_spec_2 : forall d1 d2 : t,
Inv_t d1 ->
Inv_t d2 ->
included_optim d1 d2 = true ->
forall x : Z,
member x d1 = true -> member x d2 = true.
Proof.
unfold included_optim, Inv_t ; intros d1 d2 Hinv1 Hinv2 Hdef. 
apply included_spec_2 ; auto.
unfold included. set (double := (domain d1, domain d2)).
rewrite <- equiv_elt_list_included with (l1 := domain d1) (l2:= domain d2) (y1 := min d1) (y2 := min d2) ; auto.  tauto. tauto.
Qed.

Theorem included_optim_spec_1 : forall d1 d2 : t,
Inv_t d1 ->
Inv_t d2 ->
(forall x : Z,
member x d1 = true -> member x d2 = true) -> 
included_optim d1 d2 = true.
Proof.
unfold included_optim, Inv_t ; intros d1 d2 Hinv1 Hinv2 Hdef. 
set (double := (domain d1, domain d2)).
rewrite equiv_elt_list_included with (l1 := domain d1) (l2:= domain d2) (y1 := min d1) (y2 := min d2) ; try tauto.  
apply elt_list_included_spec_1 with (l1:=domain d1) 
(l2 := domain d2) (y1 := min d1) (y2 := min d2) ; try tauto. auto. auto.
Qed.
Require Import R.Prelude.structure R.FBool.member R.Prelude.tactics.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Function elt_list_equal l1_l2 {measure decreasing_length} := match l1_l2 with
| (Nil, Nil) => true
| (Nil, _) | (_, Nil) => false
| (Cons a1 b1 q1, Cons a2 b2 q2) => 
      if andb (Zeq_bool a1 a2) (Zeq_bool b1 b2)
      then elt_list_equal (q1, q2)
      else false
end.
Proof.
intros; unfold decreasing_length; simpl; lia.
Defined.

(* TESTS
Eval compute in elt_list_equal (Cons 4 5 (Cons 9 10 Nil), Cons 4 5 Nil).
Eval compute in elt_list_equal (Cons 4 5 (Cons 9 10 Nil), Cons 4 5 (Cons 9 10 Nil)).
*)

Definition equal s1 s2 := elt_list_equal (domain s1, domain s2).

(************************************)
(** * Specification                 *)
(************************************)
Lemma decreasing_equiv_elt_list_member : 
forall a1 b1 q1 y1 a2 b2 q2 y2,
Inv_elt_list y1 (Cons a1 b1 q1) ->
Inv_elt_list y2 (Cons a2 b2 q2) ->
a1 = a2 ->
b1 = b2 ->
(forall x : Z,
    elt_list_member x (Cons a1 b1 q1) = true <->
    elt_list_member x (Cons a2 b2 q2) = true) ->
(forall x : Z,
    elt_list_member x q1 = true <->
    elt_list_member x q2 = true).
Proof.
intros a1 b1 q1 y1 a2 b2 q2 y2 Hyp1 Hyp2 HypEq1 HypEq2 HypEquiv x.
subst. assert(elt_list_member x (Cons a2 b2 q1) = true <->
           elt_list_member x (Cons a2 b2 q2) = true) as Hyp3 
           by apply HypEquiv.
rewrite <- decompose_elt_list_member with (y:=y1) in Hyp3; 
  try auto.
rewrite <- decompose_elt_list_member with (y:=y2) in Hyp3; 
  try auto. destruct Hyp3 as [H12 H21].
split;intro Hyp3.
    + assert(a2 <= x <= b2 \/ elt_list_member x q1 = true) 
        as HypDisjonc by tauto. apply H12 in HypDisjonc.
      decompose [or] HypDisjonc.
        * case_eq(q1);intros;subst.
              - inversion Hyp3.
              - inversion Hyp1. inversion H6.
                apply prop_minimum with (y:=b2+2) in Hyp3; 
                  try auto. simpl in Hyp3. lia.
        * assumption.
    + assert(a2 <= x <= b2 \/ elt_list_member x q2 = true) 
        as HypDisjonc by tauto. apply H21 in HypDisjonc.
      decompose [or] HypDisjonc.
        * case_eq(q2);intros;subst.
              - inversion Hyp3.
              - inversion Hyp2. inversion H6.
                apply prop_minimum with (y:=b2+2) in Hyp3; 
                  try auto. simpl in Hyp3. lia.
        * assumption.
Qed.

Lemma decreasing_equiv_elt_list_member_false_borne_inf : 
forall a1 b1 q1 y1 a2 b2 q2 y2,
Inv_elt_list y1 (Cons a1 b1 q1) ->
Inv_elt_list y2 (Cons a2 b2 q2) ->
a1 < a2 ->
(forall x : Z,
    elt_list_member x (Cons a1 b1 q1) = true <->
    elt_list_member x (Cons a2 b2 q2) = true) -> False.
Proof.
intros a1 b1 q1 y1 a2 b2 q2 y2 Hyp1 Hyp2 Hyp3 HypEquiv. 
assert(elt_list_member a1 (Cons a1 b1 q1) = true <->
       elt_list_member a1 (Cons a2 b2 q2) = true)
  as Hyp4 by apply HypEquiv.
destruct Hyp4 as [H12 H21].
rewrite <- decompose_elt_list_member with (y:=y1) in H12; try auto.
rewrite <- decompose_elt_list_member with (y:=y2) in H12; try auto.
assert(a1 <= a1 <= b1 \/ elt_list_member a1 q1 = true)
  as HypDisjonc by (left;inversion Hyp1;lia). 
apply H12 in HypDisjonc. decompose [or] HypDisjonc.
   - lia.
   - apply elt_list_member_inegality
       with (a:=a2) (b:=b2) (y:=y2) in H;try assumption.
     inversion Hyp2;lia.
Qed.

Lemma decreasing_equiv_elt_list_member_false_borne_sup : 
forall a1 b1 q1 y1 a2 b2 q2 y2,
Inv_elt_list y1 (Cons a1 b1 q1) ->
Inv_elt_list y2 (Cons a2 b2 q2) ->
b2 < b1 ->
(forall x : Z,
    elt_list_member x (Cons a1 b1 q1) = true <->
    elt_list_member x (Cons a2 b2 q2) = true) -> False.
Proof.
intros a1 b1 q1 y1 a2 b2 q2 y2 Hyp1 Hyp2 Hyp3 HypEquiv.
assert(elt_list_member (b2+1) (Cons a1 b1 q1) = true <->
       elt_list_member (b2+1) (Cons a2 b2 q2) = true)
  as Hyp4 by apply HypEquiv.
destruct Hyp4 as [H12 H21].
rewrite <- decompose_elt_list_member with (y:=y1) in H12;try auto.
rewrite <- decompose_elt_list_member with (y:=y2) in H12;try auto.
assert(a1 <= (b2+1) <= b1 \/ elt_list_member (b2+1) q1 = true)
   as HypDisjonc. 
   + case_eq(a1 <=? b2);intro;Zbool_lia.
       * inversion Hyp1;left;split;lia.
       * assert(elt_list_member b2 (Cons a1 b1 q1) = true <->
                elt_list_member b2 (Cons a2 b2 q2) = true)
           as Hyp4 by apply HypEquiv.
         destruct Hyp4 as [H12_bis H21_bis]. 
         rewrite <- decompose_elt_list_member 
           with (y:=y2) in H21_bis; try auto.
         rewrite <- decompose_elt_list_member 
           with (y:=y1) in H21_bis; try auto.
          
         assert(a2 <= b2 <= b2 \/ elt_list_member b2 q2 = true)
           as Hyp4 by (left;inversion Hyp2;lia). 
         apply H21_bis in Hyp4. 
         decompose [or] Hyp4; try (destruct H0;lia).
          case_eq(q1);intros;subst.
            - inversion H0.
            - inversion Hyp1.
              apply prop_minimum with (y:=b1+2) in H0;try auto.
              simpl in H0;inversion H7;lia.
   + apply H12 in HypDisjonc. 
     decompose [or] HypDisjonc;try (destruct H;lia).
     case_eq(q2);intros;subst.
       * inversion H.
       * inversion Hyp2.
         apply prop_minimum with (y:=b2+2) in H; try auto.
         simpl in H;inversion H6;lia.
Qed.

Lemma decreasing_equiv_elt_list_member_false : 
forall a1 b1 q1 y1 a2 b2 q2 y2,
Inv_elt_list y1 (Cons a1 b1 q1) ->
Inv_elt_list y2 (Cons a2 b2 q2) ->
Zeq_bool a1 a2 = false \/ Zeq_bool b1 b2 = false ->
(forall x : Z,
    elt_list_member x (Cons a1 b1 q1) = true <->
    elt_list_member x (Cons a2 b2 q2) = true) -> False.
Proof.
intros a1 b1 q1 y1 a2 b2 q2 y2 Hyp1 Hyp2 HypEq HypEquiv.
decompose [or] HypEq;Zbool_lia. 
  * case_eq(a1 <=? a2);intro Hyp3;Zbool_lia.
     + apply decreasing_equiv_elt_list_member_false_borne_inf
         with (a1:=a1) (b1:=b1) (q1:=q1) (y1:=y1) (a2:=a2)
              (b2:=b2) (q2:=q2) (y2:=y2);(assumption||lia).
     + apply decreasing_equiv_elt_list_member_false_borne_inf
         with (a1:=a2) (b1:=b2) (q1:=q2) (y1:=y2) (a2:=a1)    
              (b2:=b1) (q2:=q1) (y2:=y1); try (assumption||lia).
       intro x. rewrite HypEquiv. tauto.
  * case_eq(b2 <=? b1);intro Hyp3;Zbool_lia.
     + apply decreasing_equiv_elt_list_member_false_borne_sup
         with (a1:=a1) (b1:=b1) (q1:=q1) (y1:=y1) (a2:=a2)
              (b2:=b2) (q2:=q2) (y2:=y2);(assumption||lia).
     + apply decreasing_equiv_elt_list_member_false_borne_sup
         with (a1:=a2) (b1:=b2) (q1:=q2) (y1:=y2) (a2:=a1)    
              (b2:=b1) (q2:=q1) (y2:=y1); try (assumption||lia).
       intro x. rewrite HypEquiv. tauto.
Qed.


Lemma elt_list_equal_compat_eq : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 ->
double = (l1, l2) ->
elt_list_equal double = true -> l1 = l2.
Proof.
intro double.
functional induction (elt_list_equal double).
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. 
  inversion Hyp3. auto.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. subst. 
  rewrite andb_true_iff in e0. decompose [and] e0. Zbool_lia.
  inversion Hyp1. inversion Hyp2. subst.
  rewrite IHb with (y1:=b2+2) (y2:=b2+2) (l1:=q1) (l2 :=q2); auto.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 . inversion Hyp4.
Qed.

Theorem equal_compat_eq_domain: forall s s',
Inv_t s -> Inv_t s' ->
equal s s' = true -> domain s = domain s'.
Proof.
unfold Inv_t. unfold member. unfold equal. 
intros s s' Hyp1 Hyp2 Hyp3 .
destruct Hyp1. destruct Hyp2.
apply elt_list_equal_compat_eq 
  with (double:=(domain s, domain s')) (y1:=min s) (y2:=min s');
auto.
Qed.

Theorem elt_list_equal_spec_1 : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 ->
double = (l1, l2) ->
(forall x, elt_list_member x l1 = true <-> 
elt_list_member x l2 = true) -> elt_list_equal double = true.
Proof.
intro double.
functional induction (elt_list_equal double); try auto.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. subst.
  case_eq(l2);intros.
    + subst. contradiction.
    + subst. rewrite (Hyp4 z).
      simpl. inversion Hyp2.
      assert(z <=? z0 = true) as Hyp5 
        by (Zbool_lia_goal;lia);
      rewrite Hyp5;Zbool_lia_goal;lia. 
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. subst.
  case_eq(l1);intros.
    + subst. contradiction.
    + subst. rewrite <- (Hyp4 z).
      simpl. inversion Hyp1.
      assert(z <=? z0 = true) as Hyp5 
        by (Zbool_lia_goal;lia);
      rewrite Hyp5;Zbool_lia_goal;lia.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. subst.
  inversion Hyp1. inversion Hyp2.
  rewrite andb_true_iff in e0. decompose [and] e0. Zbool_lia.
  apply IHb 
    with (l1:=q1) (l2:=q2) (y1:=b1+2) (y2:=b2+2); try auto.
  apply decreasing_equiv_elt_list_member
    with (a1:=a1) (b1:=b1) (y1:=y1) (a2:=a2) (b2:=b2) (y2:=y2);
      try auto.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. subst.
  inversion Hyp1. inversion Hyp2.
  rewrite andb_false_iff in e0. decompose [and] e0. Zbool_lia.
  assert(False).
  apply decreasing_equiv_elt_list_member_false
    with (a1:=a1) (b1:=b1) (y1:=y1) (a2:=a2) (b2:=b2) (y2:=y2)
         (q1:=q1) (q2:=q2);
      try auto.
  contradiction.
Qed.

(*useless - shorter proof below*)
Theorem elt_list_equal_spec_2 : forall double l1 l2 y1 y2,
Inv_elt_list y1 l1 -> Inv_elt_list y2 l2 ->
double = (l1, l2) ->
elt_list_equal double = true ->
(forall x, elt_list_member x l1 = true <-> 
elt_list_member x l2 = true).
Proof.
intro double.
functional induction (elt_list_equal double).
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x. 
  inversion Hyp3. subst. simpl. tauto.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x. inversion Hyp4.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x. inversion Hyp4.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x. inversion Hyp3. subst.
  rewrite andb_true_iff in e0. decompose [and] e0. Zbool_lia.
  inversion Hyp1. inversion Hyp2. subst.
  assert(elt_list_member x q1 = true 
              <-> elt_list_member x q2 = true).
  apply IHb with (y1:=b2+2) (y2:=b2+2); try auto.
  rewrite <- decompose_elt_list_member with (y:=y1); try auto.
  rewrite <- decompose_elt_list_member with (y:=y2); try auto.
  split;intro HypOr;decompose [or] HypOr; try auto.
     + right. apply H;assumption.
     + right. apply H;assumption.
* intros l1 l2 y1 y2 Hyp1 Hyp2 Hyp3 Hyp4 x. inversion Hyp4.
Qed.

Theorem equal_spec_1 : forall s s',
Inv_t s -> Inv_t s' ->
(forall x, member x s = true <-> member x s' = true) -> 
equal s s' = true.
Proof.
unfold Inv_t. unfold member. unfold equal. 
intros s s' Hyp1 Hyp2 Hyp3.
destruct Hyp1. destruct Hyp2.
apply elt_list_equal_spec_1 
  with (l1:=domain s) (l2:=domain s') (y1:=min s) (y2:=min s');
  try auto.
Qed.

Theorem equal_spec_2 : forall s s',
Inv_t s -> Inv_t s' ->
equal s s' = true ->
(forall x, member x s = true <-> member x s' = true).
Proof.
unfold Inv_t. unfold member.
intros s s' Hyp1 Hyp2 Hyp3.
apply equal_compat_eq_domain in Hyp3 ; auto.
rewrite Hyp3. intuition.
Qed.

Lemma egality_head : forall y1 a b q1 y2 c d q2, 
    Inv_elt_list y1 (Cons a b q1) -> 
    Inv_elt_list y2 (Cons c d q2) -> 
    elt_list_equal (Cons a b q1, Cons c d q2) = true ->
    (a = c /\ b = d) /\ elt_list_equal (q1 , q2) = true.
Proof.
    intros y1 a b q1 y2 c d q2 Hyp1 Hyp2 Hyp3.
    split.
      + rewrite elt_list_equal_equation in Hyp3.
        case_eq(Zeq_bool a c && Zeq_bool b d);intro Hyp4.
          - apply andb_true_iff in Hyp4.
            decompose [and] Hyp4.
            Zbool_lia. tauto.
          - rewrite Hyp4 in Hyp3. discriminate Hyp3.
      + rewrite elt_list_equal_equation in Hyp3.
        case_eq(Zeq_bool a c && Zeq_bool b d);intro Hyp4.
          - rewrite Hyp4 in Hyp3. assumption.
          - rewrite Hyp4 in Hyp3. discriminate Hyp3.
Qed.
Require Import R.Prelude.tactics R.Prelude.xStandardLib R.Prelude.structure R.CD.values.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.


(************************************)
(** * Definition                    *)
(************************************)
Fixpoint elt_list_member (x : Z) (l : elt_list) : bool :=
 match l with
  | Nil => false
  | Cons min max q => if x <=? max then x >=? min 
                      else (elt_list_member x q)
end.

Definition member (x : Z) (d : t) := elt_list_member x (domain d).

(************************************)
(** * Specification                 *)
(************************************)
Lemma elt_list_member_cons : forall a b q z,
elt_list_member z (Cons a b q) = true ->
a <= z <= b \/  elt_list_member z q = true.
Proof.
intros a b q z. simpl .
case_eq (z <=? b);  intros Hyp1 Hyp2.
+ left ; Zbool_lia; lia.
+ tauto.
Qed.

Lemma prop_minimum : forall x l y, Inv_elt_list y l -> 
elt_list_member x l = true -> get_min l y <= x.
Proof.
induction l;intros y Hyp1 Hyp2. 
- simpl in Hyp2. discriminate Hyp2.
- simpl. inversion Hyp1. simpl in Hyp2.
  case_eq(x <=? z0);intro Hyp3;rewrite Hyp3 in Hyp2;Zbool_lia;lia.
Qed.

Lemma prop_maximum : forall l x y, Inv_elt_list y l -> 
elt_list_member x l = true -> x <= process_max l.
Proof.
induction l; intros x y Hyp1 Hyp2.
- simpl in Hyp2. discriminate Hyp2.
- inversion Hyp1. simpl in Hyp2.
    case_eq(x <=? z0);intro Hyp3;rewrite Hyp3 in Hyp2.
      * Zbool_lia. case_eq(l);intros.
         + unfold process_max. unfold pm. lia.
         + rewrite H6 in H5. inversion H5.
           assert (z1 <= process_max l). 
           apply IHl with (y:=z0+2). rewrite H6. assumption.
             rewrite H6. simpl. case_eq(z1 <=? z2);intro Hyp4.
             Zbool_lia_goal;lia.
             Zbool_lia;lia.
             rewrite H6 in H14.
             unfold process_max. simpl.
             unfold process_max in H14. simpl in H14. lia.
      * Zbool_lia.
        assert(x <= process_max l) 
          by (apply IHl with (y:=z0+2);assumption).     
        case_eq(l);intros. 
          + rewrite H7 in Hyp2. simpl in Hyp2. 
            discriminate Hyp2.
          + rewrite H7 in H6. unfold process_max. simpl.
            unfold process_max in H6. simpl in H6. lia.
Qed.

Theorem elt_list_member_spec : 
forall (x : Z) (l : elt_list) y, Inv_elt_list y l -> 
elt_list_member x l = true <-> In x (elt_list_values l).
Proof.
induction l.
- split;intro Hyp;simpl in Hyp;[discriminate|contradiction].
- split;intro Hyp;simpl.
    * simpl in Hyp. 
      case_eq(x <=? z0); intro Hyp2;rewrite Hyp2 in Hyp. 
        + apply intro_enum_and_conc 
            with (z:=z) (z0:=z0) (l:=elt_list_values l);
            try reflexivity.
          left. split;Zbool_lia;lia.
        + apply intro_enum_and_conc 
            with (z:=z) (z0:=z0) (l:=elt_list_values l);
            try reflexivity.
          right. inversion H. apply IHl in H6. apply H6.
          assumption.
    * case_eq(x <=? z0); intro Hyp2.
        + simpl in Hyp. 
          apply elim_enum_and_conc 
            with (z:=z) (z0:=z0) (l:=elt_list_values l) in Hyp;
            try reflexivity. 
          decompose [or] Hyp;Zbool_lia_goal;Zbool_lia.
              decompose [and] H0;lia.

              inversion H. inversion H7. 
                rewrite <- H9 in H0. simpl in H0. contradiction.  

                rewrite <- H12 in H0. rewrite <- H12 in H. 
                assert(get_min l (z0+2) <= x) as Hyp3.
                   apply prop_minimum 
                     with (l:=l) (x:=x) (y:=z0+2); try assumption.
                   rewrite H12 in H0. 
                   apply IHl with (y:=z0+2);assumption.
                rewrite <- H12 in Hyp3. simpl in Hyp3.
                assert ((x <=? z0) = false) 
                  by (apply Z.leb_gt;lia). 
                Zbool_lia;lia.
         + simpl in Hyp. 
           apply elim_enum_and_conc 
             with (z:=z) (z0:=z0) (l:=elt_list_values l) in Hyp;
             try reflexivity. 
           decompose [or] Hyp.
             Zbool_lia;lia.
             inversion H. apply IHl in H7. apply H7. assumption.
Qed.





Lemma decompose_elt_list_member : forall min1 max1 q1 x y,
Inv_elt_list y (Cons min1 max1 q1) ->
min1 <= x <= max1 \/ elt_list_member x q1 = true
<-> elt_list_member x (Cons min1 max1 q1) = true.
Proof.
split;intro Hyp1.
* decompose [or] Hyp1.
    - apply elt_list_member_spec with (y:=y).
      assumption. unfold elt_list_values.
      apply intro_enum_and_conc with (z:=min1) (z0:=max1) 
         (l:=(fix elt_list_values (l0 : elt_list) : 
         list Z :=
            match l0 with
            | Nil => []
            | Cons min max q =>
              enum_and_conc (min, max, elt_list_values q)
           end) q1). 
      reflexivity. tauto.
    - apply elt_list_member_spec with (y:=y).
      assumption. unfold elt_list_values.
      apply intro_enum_and_conc with (z:=min1) (z0:=max1) 
          (l:=(fix elt_list_values (l0 : elt_list) : 
          list Z :=
             match l0 with
             | Nil => []
             | Cons min max q =>
               enum_and_conc (min, max, elt_list_values q)
            end) q1). 
       reflexivity. right.
       rewrite elt_list_member_spec with (y:=y) in H0. assumption.
       inversion H. Inv_monotony (max1+2).
* apply elt_list_member_spec with (y:=y) in Hyp1.
  unfold elt_list_values in Hyp1.
  apply elim_enum_and_conc with (z:=min1) (z0:=max1) 
          (l:=(fix elt_list_values (l0 : elt_list) : 
          list Z :=
             match l0 with
             | Nil => []
             | Cons min max q =>
               enum_and_conc (min, max, elt_list_values q)
            end) q1) in Hyp1. 
   decompose [or] Hyp1.
      tauto.
      right. rewrite elt_list_member_spec with (y:=y). assumption.
      inversion H. Inv_monotony (max1+2).
   reflexivity. assumption.
Qed.

Lemma elt_list_member_spec_not :
forall (x : Z) (l : elt_list) (y : Z),
Inv_elt_list y l ->
elt_list_member x l = false <->
~(In x (elt_list_values l)).
Proof.
unfold not; split; intros.
+ rewrite <- elt_list_member_spec with (y:=y) in H1 ; auto.
  congruence.
+ case_eq (elt_list_member x l) ; intro Heq.
  - rewrite elt_list_member_spec with (x:= x) (y := y) in Heq ; auto.
    apply H0 in Heq. contradiction.
  - auto.
Qed.

Lemma elt_list_member_decreasing : forall x min2 max2 q2 y,
Inv_elt_list y (Cons min2 max2 q2) ->
elt_list_member x (Cons min2 max2 q2) = false ->
elt_list_member x q2 = false.
Proof.
intros x min2 max2 q2 y Hyp1 Hyp2.
apply elt_list_member_spec_not with (y:=y) in Hyp2.
  + unfold not in Hyp2. 
    apply elt_list_member_spec_not with (y:=y).
       * inversion Hyp1. Inv_monotony (max2+2).
       * unfold not. intro Hyp3. unfold elt_list_values in Hyp3.
         apply Hyp2. unfold elt_list_values.
         apply intro_enum_and_conc with (z:=min2) (z0:=max2) (l:=(fix elt_list_values (l : elt_list) : 
      list Z :=
        match l with
        | Nil => []
        | Cons min max q =>
            enum_and_conc (min, max, elt_list_values q)
        end) q2).
        reflexivity. right. assumption. 
  + assumption.
Qed.

Lemma elt_list_member_inegality : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
elt_list_member x q = true -> b+2 <= x.
Proof.
intros a b x y q Hyp1 Hyp2. inversion Hyp1. 
rewrite elt_list_member_spec with (y:=b+2) in Hyp2; try assumption.
apply in_elt_list_values_le_2 with (l:=q);assumption.
Qed.

Lemma elt_list_member_inegality_2 : forall l x y,
Inv_elt_list y l ->
elt_list_member x l = true -> y <= x.
Proof.
intros l x y Hyp1 Hyp2. 
induction l;intros.
  + inversion Hyp2.
  + inversion Hyp1. 
    rewrite <- decompose_elt_list_member with (y:=y) in Hyp2;
       try assumption.
    decompose [or] Hyp2.
       - lia.
       - apply IHl. Inv_monotony (z0+2). assumption.
Qed.

(* OU
Proof.
intros l x y Hyp1 Hyp2.
rewrite elt_list_member_spec with (y:=y) in Hyp2; try assumption.
assert(y - 2 + 2 <= x).
apply in_elt_list_values_le_2 with (z0:=y-2) (l:=l); 
  try assumption.
  assert(y-2+2=y) as Hyp3 by (lia). rewrite Hyp3. assumption.
  lia.
Qed.
*)

Lemma elt_list_member_le : forall q y x, 
Inv_elt_list y q -> 
elt_list_member x q = true -> y <= x.
Proof.
induction 1; intros.
+ discriminate.
+ simpl in H2. 
  destruct (x <=? b). 
  - Zbool_lia; lia.
  - apply IHInv_elt_list in H2 ; lia.
Qed.

Lemma decompose_elt_list_member_not : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
elt_list_member x (Cons a b q) = false <-> 
~(a <= x <= b) /\ elt_list_member x q = false.
Proof.
split;intro Hyp1.
  * unfold elt_list_member in Hyp1.
    case_eq(x <=? b);intro Hyp2;rewrite Hyp2 in Hyp1.
      + Zbool_lia. split. 
          - lia.
          - case_eq (elt_list_member x q) ; intro Htrue; auto. 
            apply elt_list_member_le with (y:=b+2) in Htrue.
            lia. inversion H;assumption.
      + Zbool_lia. split.
          - lia.
          - auto.
  * simpl. case_eq(x <=? b); intro Hyp2.
      + Zbool_lia_goal;Zbool_lia;lia.
      + tauto.
Qed.

Lemma elt_list_member_inegality_3 : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
x < a ->
elt_list_member x (Cons a b q) = false.
Proof.
intros a b x y q Hyp1 Hyp2.
unfold elt_list_member. case_eq(x <=? b);intros.
  + Zbool_lia_goal;Zbool_lia;lia.
  + inversion Hyp1. Zbool_lia. lia.
Qed.

Lemma elt_list_values_member_spec : forall d x, Inv_t d -> 
member x d = true <-> In x (elt_list_values (domain d)).
Proof.
split;intro Hyp. 
- apply elt_list_member_spec with (y:=(min d)).
   + unfold Inv_t in H. tauto.
   + unfold member in Hyp. assumption.
- rewrite <- elt_list_member_spec in Hyp.
   + unfold member. assumption.
   + unfold Inv_t in H. decompose [and] H. exact H0.
Qed.

Theorem member_spec : forall v d, Inv_t d ->
member v d = true <-> In v (values d).
Proof.
split;intro Hyp. 
- unfold values.
  apply elt_list_values_member_spec;assumption.
- unfold values in Hyp.
  apply elt_list_values_member_spec;assumption.
Qed.
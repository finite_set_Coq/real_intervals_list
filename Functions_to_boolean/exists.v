Require Import R.Prelude.structure R.Prelude.tactics R.CD.values.
Require Import List ZArith Recdef Lia.
Import ListNotations.
Open Scope Z_scope.

Section exists_elem.

  (************************************)
  (** * Definition                    *)
  (************************************)
  Variable p : Z -> bool.

  Function exists_inter (min_max :  Z*Z) {measure minus_pair}:=
  match min_max with
    (min, max) =>
    if Zgt_bool min max then false
    else if Zeq_bool min max then p min
         else if p min then true
              else exists_inter (min+1,max)
  end.
  Proof.
  intros; simpl.
  apply Zabs_nat_lt.
  Zbool_lia; lia.
  Defined.

  Fixpoint exists_t l  := match l with
   |  Nil => false
   | Cons a b q => if exists_inter (a, b) then true else exists_t q
  end.

  Definition exist d := exists_t (domain d).

  (************************************)
  (** * Specification                 *)
  (************************************)
  Lemma exists_inter_true : forall c z z0,
  c = (z,z0) ->
  exists_inter c = true <-> exists a, z <= a <= z0 /\ p a = true.
  Proof.
  intro c.
  functional induction (exists_inter c); Zbool_lia ; intros z z0 H ; inversion H.
  + split; intros. inversion H0.
  destruct H0; lia.
  + split; intro Hyp.
   exists z; split; auto. lia.
   destruct Hyp.
   assert (x = z) by lia.
   subst ; tauto.
  + split; intro Hyp ; auto.
   exists min. split ; [lia | assumption].
  + pose proof (IHb (min+1) max (eq_refl ((min+1, max)))).
  subst.
  split ; intro Hyp.
  apply H0 in Hyp. destruct Hyp. exists x ; split ; [lia | tauto].
  destruct Hyp.
  assert (z <> x).
  elim (Z.eq_dec z x); intro.
  rewrite a in e2; rewrite e2 in H1; destruct H1. discriminate H2.
  auto.
  assert (exists a : Z, z + 1 <= a <= z0 /\ p a = true).
  exists x. split ; [lia | tauto].
  apply H0 ; auto.
  Qed.

  Lemma exists_inter_false : forall c z z0,  c = (z, z0) ->
  exists_inter c = false <-> forall a, z <= a <= z0 -> p a = false.
  Proof.
  intro c.
  functional induction (exists_inter c); Zbool_lia ; 
      intros z z0 H ; inversion H.
  + split; intros. lia.
   auto.
  + split; intros.
    assert (z = a) by lia.
    congruence.
    apply H0 ; lia.
  + split; intro Hyp. discriminate Hyp.
    assert (p min = false) as Hyp1 by (apply Hyp ; lia).
    rewrite e2 in Hyp1 ; discriminate Hyp1.
  +  pose proof (IHb (min+1) max (eq_refl ((min+1, max)))).
  subst.
  split ; intros Hyp .
  pose proof (proj1 H0 Hyp). intros.
  elim (Z.eq_dec z a); intro Hcase. subst; auto.
  apply H1 ; lia.
  apply H0.
  intros ; apply Hyp; lia.
  Qed.

  Theorem exists_t_spec : forall l ,
  exists_t l = true <-> exists a, In a (elt_list_values l) 
                                     /\ p a = true.
  Proof.
  induction l; simpl.
  - split; intros. inversion H. destruct H; intros;tauto.
  - case_eq (exists_inter (z, z0)); intro Hcase.
    + split; intros; auto.
    pose proof ((proj1 (exists_inter_true (z, z0) z z0 (eq_refl (z, z0)))) Hcase) as Hcase1.
    destruct Hcase1. exists x.
    split ; try tauto.
    apply intro_enum_and_conc 
       with (triple := (z, z0, elt_list_values l)) (z:= z) 
                       (z0 := z0) (l := elt_list_values l); 
       auto.
    intuition.
    + split ; intro Hyp.
    apply IHl in Hyp ; destruct Hyp.
    exists x ; split.
    apply intro_enum_and_conc 
       with (triple := (z, z0, elt_list_values l)) (z:= z) 
                       (z0 := z0) (l := elt_list_values l); 
       auto.
    intuition.
    tauto.
    apply IHl.
    destruct Hyp. destruct H.
    exists x ; split; auto.
    apply elim_enum_and_conc 
       with (z:= z) (z0 := z0) (l := elt_list_values l) in H ;
       auto.
    destruct H ; auto.
    pose proof (proj1 (exists_inter_false (z,z0) z z0 (eq_refl (z, z0))) Hcase x H).
    rewrite H0 in H1 ; discriminate H1.
  Qed.

  Theorem exist_spec : forall d,
  exist d = true <-> exists a, In a (values d) /\ p a = true.
  Proof.
  intro d. 
  unfold values. unfold exist. apply exists_t_spec.
  Qed.

End exists_elem.
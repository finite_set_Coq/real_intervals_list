Require Import R.Prelude.structure R.FBool.member R.FBool.exists R.Prelude.tactics R.CD.values.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

Section for_all_elem.

  (************************************)
  (** * Definition                    *)
  (************************************)
  Variable p : Z -> bool.

  (** For one interval    *)
  Function inter_for_all (a_b : Z * Z) {measure minus_pair} := 
  match a_b with
  (a,b) => if Zgt_bool a b then false
           else if Zeq_bool a b then p a
           else andb (p a) (inter_for_all ((a+1),b))
  end.
  Proof.
  intros ; simpl.
  apply Zabs_nat_lt.
  Zbool_lia; lia.
  Defined.

  (** For elt_list       *)
  Fixpoint elt_list_for_all l := match l with
   | Nil => true
   | Cons a b q => andb (inter_for_all (a,b)) (elt_list_for_all q)
  end.

  (** For domain         *)
  Definition for_all d := elt_list_for_all (domain d).

  (************************************)
  (** * Specification                 *)
  (************************************)

  (** For one interval    *)
  Lemma inter_for_all_spec : forall c a b (z:Z),
  c = (a,b) ->
  a <= b ->
  inter_for_all c = true <-> forall z, a <= z <= b -> p z = true.
  Proof.
  intro c.
  functional induction (inter_for_all c); Zbool_lia ; intros z z0 H ; inversion H.
  + split; intros. 
      * inversion H2. 
      * inversion H0. subst. lia.
  + split; intros. 
      * inversion H3. 
      * inversion H1. subst. lia.
  + split; intros.
      * inversion H3. 
      * inversion H1. subst. lia.
  + split; intros. 
      * inversion H0. subst. assert(z0=z1) by lia. rewrite <- H4. assumption.
      * inversion H0. subst. apply H2. lia.
  + split; intros. 
      * inversion H1. subst. assert(z0=z1) by lia. rewrite <- H5. assumption.
      * inversion H1. subst. apply H3. lia.
  + split; intros. 
      * inversion H1. subst. assert(z0=z1) by lia. rewrite <- H5. assumption. 
      * inversion H1. subst. apply H3. lia.
  + split; intros.
      * inversion H0. subst. apply andb_true_iff in H2.
        case_eq(Zeq_bool z1 z);intro Heq.
          - Zbool_lia. rewrite Heq. tauto.
          - apply IHb with (a:=z+1) (b:=z0).
            assumption. reflexivity. 
            Zbool_lia;lia. tauto. Zbool_lia;lia.
      * inversion H0. subst. apply andb_true_iff. split.
          - apply H2. lia.
          - rewrite IHb with (a:=z+1) (b:=z0).
               intros z1 Hyp1. apply H2. lia.
               assumption.
               reflexivity.
               lia.
  + split; intros.
      * inversion H1. subst. apply andb_true_iff in H3.
        case_eq(Zeq_bool z1 z);intro Heq.
          - Zbool_lia. rewrite Heq. tauto.
          - apply IHb with (a:=z+1) (b:=z0).
            assumption. reflexivity. 
            Zbool_lia;lia. tauto. Zbool_lia;lia.
      * inversion H1. subst. apply andb_true_iff. split.
          - apply H3. lia.
          - rewrite IHb with (a:=z+1) (b:=z0).
               intros z1 Hyp1. apply H3. lia.
               assumption.
               reflexivity.
               lia.
  + split; intros.
      * inversion H1. subst. apply andb_true_iff in H3.
        case_eq(Zeq_bool z1 z);intro Heq.
          - Zbool_lia. rewrite Heq. tauto.
          - apply IHb with (a:=z+1) (b:=z0).
            assumption. reflexivity. 
            Zbool_lia;lia. tauto. Zbool_lia;lia.
      * inversion H1. subst. apply andb_true_iff. split.
          - apply H3. lia.
          - rewrite IHb with (a:=z+1) (b:=z0).
               intros z1 Hyp1. apply H3. lia.
               assumption.
               reflexivity.
               lia.
  Qed.

  (**  For elt_list        *)
  Lemma elt_list_for_all_spec : forall l y,
  Inv_elt_list y l -> 
  elt_list_for_all l = true <-> 
  forall x, elt_list_member x l = true -> p x = true.
  Proof.
  induction l.
  * intros y Hyp1.
    split;intros.
      - simpl in H0. inversion H0.
      - auto.
  * intros y Hyp1.
    split;intros.
      - rewrite elt_list_member_spec with (y:=y) in H0.
    unfold elt_list_values in H0. apply elim_enum_and_conc with (z:=z) (z0:=z0) (l:=(fix elt_list_values (l : elt_list) : list Z :=
               match l with
               | Nil => []
               | Cons min max q => enum_and_conc (min, max, elt_list_values q)
               end) l) in H0.
        decompose [or] H0.
             simpl in H. apply andb_true_iff in H.
             decompose [and] H. rewrite inter_for_all_spec with (a:=z) (b:=z0) in H2. apply H2 in H1. assumption. assumption. reflexivity. lia.  
              apply IHl with (y:=y).
         + inversion Hyp1. Inv_monotony (z0+2).
         + simpl in H. apply andb_true_iff in H. tauto.
         + inversion Hyp1. 
           rewrite elt_list_member_spec with (y:=z0+2); auto.
         + reflexivity.
         + assumption.
      - simpl. apply andb_true_iff. split.
         + rewrite inter_for_all_spec with (a:=z) (b:=z0).
              intros z1 HypEnc. apply H with (x:=z1).
              rewrite elt_list_member_spec with (y:=y).
              unfold elt_list_values.
              apply intro_enum_and_conc with (z:=z) (z0:=z0) (l:=(fix elt_list_values (l0 : elt_list) : list Z :=
          match l0 with
          | Nil => []
          | Cons min max q => enum_and_conc (min, max, elt_list_values q)
          end) l). reflexivity. tauto.
              assumption. assumption. reflexivity. inversion Hyp1. assumption.
         + apply IHl with (y:=y).
           inversion Hyp1. Inv_monotony (z0+2).
           intro x0.
           assert(elt_list_member x0 (Cons z z0 l) = true 
                    -> p x0 = true) by apply H. 
           intro Hyp2. apply H0.
           rewrite elt_list_member_spec with (y:=y).
           unfold elt_list_values.
           apply intro_enum_and_conc with (z:=z) (z0:=z0) (l:=(fix elt_list_values (l : elt_list) : list Z :=
               match l with
               | Nil => []
               | Cons min max q => enum_and_conc (min, max, elt_list_values q)
               end) l). reflexivity. right. 
               apply elt_list_member_spec with (y:=z0+2) in Hyp2.
               auto. inversion Hyp1. assumption.
          assumption.
  Qed.

  Theorem for_all_spec : forall d,
  Inv_t d -> 
  for_all d = true <-> 
  forall x, member x d = true -> p x = true.
  Proof.
  intros d Hyp.
  unfold for_all. unfold member.
  rewrite elt_list_for_all_spec with (y:=min d).
    + reflexivity.
    + unfold Inv_t in Hyp. decompose [and] Hyp. assumption.
  Qed.

End for_all_elem.

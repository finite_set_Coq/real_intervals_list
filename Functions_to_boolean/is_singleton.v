Require Import R.Prelude.structure R.FBool.member R.Prelude.tactics R.CD.interval R.CD.singleton.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition is_singleton d := Inv_t d -> size d = 1.

(************************************)
(** * Specification                 *)
(************************************)
Theorem is_singleton_spec : forall d, Inv_t d -> 
is_singleton d <-> size d = 1.
Proof.
intro d.
split;intro Hyp.
- unfold is_singleton in Hyp.
  apply Hyp;assumption.
- unfold is_singleton;intro;assumption.
Qed.

Lemma singleton_not_in : forall a b, 
a <> b -> member b (singleton a) = false.
Proof.
intros a b Hyp.
unfold singleton. unfold interval. case_eq(a >? a);intro.
- Zbool_lia;lia.
- simpl. unfold interval_unsafe. unfold member. simpl. 
  case_eq(b <=? a);intro.
     + Zbool_lia;Zbool_lia_goal;lia.
     + auto.
Qed.

Lemma singleton_in : forall a, 
member a (singleton a) = true.
Proof.
intro a.
unfold singleton. unfold interval. case_eq(a >? a);intro.
- Zbool_lia;lia.
- simpl. unfold interval_unsafe. unfold member. simpl. 
  case_eq(a <=? a);intro.
     + Zbool_lia;Zbool_lia_goal;lia.
     + Zbool_lia. lia.
Qed.
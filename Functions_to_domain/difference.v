Require Import R.Prelude.structure R.FBool.member R.FBool.is_empty R.Prelude.tactics R.CD.empty R.CD.values.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Function elt_list_difference (l1_l2 : elt_list*elt_list) {measure decreasing_length} := match l1_l2 with
| (l, Nil) => l
| (Nil, l) => Nil
| (Cons min1 max1 q1 as l1, Cons min2 max2 q2 as l2) => 
      if Zlt_bool max1 min2 then Cons min1 max1 (elt_list_difference (q1,l2))
      else 
         if Zlt_bool max2 min1 then elt_list_difference (l1,q2)
         else (* min2 <= max1 and min1 <= max2 *)
              if Zle_bool max1 max2 then
                  if Zle_bool min2 min1 then elt_list_difference (q1, Cons max1 max2 q2)
                  else (* min1 < min2 *)
                      (*if Zeq_bool min2 max1 
                      then Cons min1 (min2-1) (elt_list_difference (q1, Cons (max1+1) max2 q2))
                      else*) Cons min1 (min2-1) (elt_list_difference (q1, Cons max1 max2 q2))
              else (* max2 < max1 *)
                  if Zle_bool min2 min1
                  then (elt_list_difference (Cons (max2+1) max1 q1,q2))
                  else (* min1 < min2 *)
                       Cons min1 (min2-1) (elt_list_difference (Cons (max2+1) max1 q1,q2))
                              
end.
Proof.
intros; simpl. 
   * intros; unfold decreasing_length; simpl;lia.
   * intros; unfold decreasing_length; simpl;lia.
   * intros; unfold decreasing_length; simpl;lia.
   * intros; unfold decreasing_length; simpl;lia.
   * intros; unfold decreasing_length; simpl;lia.
   * intros; unfold decreasing_length; simpl;lia.
Defined.

(* TESTS
Eval compute in elt_list_difference (Cons 4 5 Nil,Cons 7 8 Nil).
Eval compute in elt_list_difference (Cons (-4) (-2) Nil,Cons 5 18 Nil).
Eval compute in elt_list_difference (Cons (-4) (-2) Nil,Cons (-17) (-1) (Cons 5 18 Nil)).
Eval compute in elt_list_difference (Cons (-17) (-1) (Cons 5 18 Nil),Cons (-4) (-2) Nil).

Eval compute in elt_list_difference (Cons 7 8 Nil,Cons 4 5 Nil).
Eval compute in elt_list_difference (Cons 0 15 Nil, Cons 4 5 Nil).
Eval compute in elt_list_difference (Cons 4 5 Nil, Cons 0 15 Nil).
Eval compute in elt_list_difference (Cons 0 4 Nil, Cons 4 5 Nil).
Eval compute in elt_list_difference (Cons 4 5 Nil, Cons 0 4 Nil).
Eval compute in elt_list_difference (Cons 4 5 Nil, Cons 8 9 Nil).
Eval compute in elt_list_difference (Cons 8 9 Nil, Cons 4 5 Nil).

Eval compute in elt_list_difference (Cons 4 5 Nil, Cons 0 6 Nil).
Eval compute in elt_list_difference (Cons 0 6 Nil, Cons 4 5 Nil).

Eval compute in elt_list_difference (Cons 0 6 (Cons 8 10 Nil), Cons 7 9 Nil).

Eval compute in elt_list_difference (Cons 0 6 (Cons 10 12 Nil), Cons 7 9 Nil).
Eval compute in elt_list_difference (Cons 0 0 (Cons 6 6 (Cons 8 8 Nil)), Cons 0 20 Nil).
*)

Definition difference (d1 d2 : t) :=
if Zeq_bool (size d2) 0 then d1
else match elt_list_difference ((domain d1), (domain d2)) with
|Nil => empty
|l => mk_t l (process_size l) (process_max l) (get_min l structure.min_int)
end.

(************************************)
(** * Preservation of the invariant *)
(************************************)
Theorem elt_list_difference_inv : forall double s s' l y1 y2 y3, 
Inv_elt_list y1 s ->
Inv_elt_list y2 s' ->
double = (s,s') ->
l = elt_list_difference double ->
y3 <= y1 ->
Inv_elt_list y3 l.
Proof.
intros double.
functional induction (elt_list_difference double).
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. Inv_monotony (y1).
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. constructor.
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. 
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  constructor. lia. assumption.
  apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (y1:=max1+2) (y2:=min2);
     (assumption||reflexivity||Zbool_constructor).
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (y1:=y1) (y2:=max2+2);
    (Zbool_constructor||Inv_monotony (max2+2)||reflexivity||assumption).
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y1:=max1+2) (y2:=max1);
    try (Inv_monotony (max1+2)||Inv_monotony (max2+2)||reflexivity).
  constructor. lia. Zbool_lia;lia. assumption. lia.
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor. lia. Zbool_lia;lia.
  apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y1:=max1+2) (y2:=max1);
    try (Zbool_constructor||Inv_monotony (max1+2)||reflexivity).
  Zbool_lia;lia.
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y1:=max2+1) (y2:=max2+2);
    try (Inv_monotony (max2+2)||reflexivity).
  constructor. lia. Zbool_lia;lia. assumption.
  Zbool_lia;lia.
* intros s s' l0 y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor. lia. Zbool_lia;lia.
  apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y1:=max2+1) (y2:=max2+2);
    try (Inv_monotony (max1+2)||reflexivity||assumption).
  constructor. lia. Zbool_lia;lia. assumption.
  Zbool_lia;lia.
Qed.

Theorem difference_inv : forall s s', 
Inv_t s ->
Inv_t s' ->
Inv_t (difference s s').
Proof.
intros s s' Hyp1 Hyp2.
unfold difference.
case_eq(Zeq_bool (size s') 0);intro Hyp3.
   * assumption.
   * case_eq(elt_list_difference (domain s, domain s'));intros.
        - apply empty_inv.
        - unfold Inv_t. simpl. split;try tauto. rewrite <- H.
          unfold Inv_t in Hyp1. decompose [and] Hyp1.
          unfold Inv_t in Hyp2. decompose [and] Hyp2.
          assert(Inv_elt_list (min s) (elt_list_difference (domain s, domain s'))).
          apply elt_list_difference_inv 
            with (s:=domain s) (s':=domain s') 
                (double:=(domain s, domain s')) (y1:=min s) (y2:=min s');
            try (auto||lia).
            
          rewrite H in H7. inversion H7.
          rewrite H. Zbool_constructor.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
(*
  Parameter diff_1 : In x (diff s s') -> In x s.
  Parameter diff_2 : In x (diff s s') -> ~ In x s'.
  Parameter diff_3 : In x s -> ~ In x s' -> In x (diff s s').
  
  D'où :  
     Parameter diff_spec : In x (diff s s') <-> In x s /\ ~In x s'.
*)

Lemma translation_In_elt_list_member : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
elt_list_member x (Cons a b q) = true <-> a <= x <= b \/ In x (elt_list_values q).
Proof.
split;intro Hyp2.
  * rewrite elt_list_member_spec with (y:=y) in Hyp2;
       try assumption.
    unfold elt_list_values in Hyp2. 
    apply elim_enum_and_conc with (z:=a) (z0:=b) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) q) in Hyp2;
               [assumption|reflexivity].
  * rewrite elt_list_member_spec with (y:=y);
       try assumption.
    unfold elt_list_values.
    apply intro_enum_and_conc with (z:=a) (z0:=b) (l:=(fix elt_list_values (l : elt_list) : list Z :=
        match l with
        | Nil => []
        | Cons min max q =>
            enum_and_conc (min, max, elt_list_values q)
        end) q);
           [reflexivity|assumption].
Qed.

Theorem elt_list_difference_spec_1 : forall double s s' x y,
Inv_elt_list y s ->
Inv_elt_list y s' ->
double = (s,s') ->
elt_list_member x (elt_list_difference double) = true ->
elt_list_member x s = true.
Proof.
intros double.
functional induction (elt_list_difference double).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  subst. assumption.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. simpl in Hyp4. 
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  rewrite translation_In_elt_list_member with (y:=y0).
  case_eq(x <=? max1);intro Heq.
     + rewrite Heq in Hyp4. Zbool_lia. left. lia.
     + rewrite Heq in Hyp4. 
       apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (y:=y0) in Hyp4;
         try (Inv_monotony (max1+2)||reflexivity||assumption).
       right. rewrite elt_list_member_spec with (y:=max1+2) in Hyp4.
       unfold elt_list_values in Hyp4. assumption. assumption.
     + assumption.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (y:=y) in Hyp4;
    try (Inv_monotony (max2+2)||reflexivity||assumption).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  rewrite translation_In_elt_list_member with (y:=y). right.
  rewrite <- elt_list_member_spec with(y:=y).
  apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y);
    try (Inv_monotony (max1+2)||reflexivity||assumption).
  Zbool_constructor. Inv_monotony (max1+2). assumption.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  simpl in Hyp4.
  rewrite translation_In_elt_list_member with (y:=y).
  case_eq(x <=? min2 - 1);intro Heq.
     + rewrite Heq in Hyp4. Zbool_lia. left. lia.
     + rewrite Heq in Hyp4. 
       apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y) in Hyp4;
          try (Inv_monotony (max1+2)||reflexivity||assumption).
       right. rewrite elt_list_member_spec with (y:=max1+2) in Hyp4.
       unfold elt_list_values in Hyp4. assumption. assumption.
       Zbool_constructor.
     + assumption.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  
  assert(elt_list_member x (Cons (max2 + 1) max1 q1) = true).
  apply IHe with (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y:=y);
     try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
  
  rewrite translation_In_elt_list_member with (y:=y);
    try Zbool_constructor.
  rewrite translation_In_elt_list_member with (y:=y) in H15;
    try Zbool_constructor.
  decompose [or] H15.
      + left. Zbool_lia;lia.
      + tauto.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  simpl in Hyp4.
  rewrite translation_In_elt_list_member with (y:=y);
    try Zbool_constructor.
  case_eq(x <=? min2 - 1);intro Heq.
     + rewrite Heq in Hyp4. Zbool_lia. left. lia.
     + rewrite Heq in Hyp4.
     
  assert(elt_list_member x (Cons (max2 + 1) max1 q1) = true).
  apply IHe with (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y:=y);
     try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).

  rewrite translation_In_elt_list_member with (y:=y) in H15;
    try Zbool_constructor.
  decompose [or] H15.
      - left. Zbool_lia;lia.
      - tauto.
Qed.

Theorem difference_spec_1 : forall s s' x,
Inv_t s ->
Inv_t s' ->
member x (difference s s') = true ->
member x s = true.
Proof.
intros s s' x Hyp1 Hyp2 Hyp3.
unfold member. unfold member in Hyp3.
unfold difference in Hyp3.
case_eq(Zeq_bool (size s') 0);intro Hyp4;rewrite Hyp4 in Hyp3.
  + assumption.
  + case_eq(elt_list_difference (domain s, domain s'));intros.
      - rewrite H in Hyp3. simpl in Hyp3. inversion Hyp3.
      - rewrite H in Hyp3. unfold domain in Hyp3. 
        rewrite <- H in Hyp3.
        unfold Inv_t in Hyp1. unfold Inv_t in Hyp2.
        decompose [and] Hyp1. decompose [and] Hyp2.
        case_eq(min s <=? min s');intro Hyp5;Zbool_lia.
          * apply elt_list_difference_spec_1 
              with (double:=(domain s, domain s')) 
                   (s':=domain s') (y:=min s);
              try (assumption||Inv_monotony (min s')||reflexivity).
          * apply elt_list_difference_spec_1 
              with (double:=(domain s, domain s')) 
                   (s':=domain s') (y:=min s');
              try (assumption||Inv_monotony (min s)||reflexivity).
Qed.



Theorem elt_list_difference_spec_2 : forall double s s' x y,
Inv_elt_list y s ->
Inv_elt_list y s' ->
double = (s,s') ->
elt_list_member x (elt_list_difference double) = true -> elt_list_member x s' = false.
Proof.
intros double.
functional induction (elt_list_difference double).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3. auto.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. simpl in Hyp4. 
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  case_eq(x <=? max1);intro Hyp5.
     + apply elt_list_member_inegality_3 with (y:=y0);
          try assumption. Zbool_lia;lia.
     + rewrite Hyp5 in Hyp4.
       apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (y:=y0) in Hyp4;
          try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4.
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  rewrite decompose_elt_list_member_not with (y:=y);
     try assumption. split.
        + apply elt_list_difference_spec_1 
    with (s:=Cons min1 max1 q1) (s':=q2) (y:=y) in Hyp4;
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
      rewrite <- decompose_elt_list_member with (y:=y) in Hyp4;
              try assumption.
          decompose [or] Hyp4.
             - Zbool_lia;lia.
             - apply elt_list_member_inegality 
                 with (a:=min1) (b:=max1) (y:=y) in H15.
               Zbool_lia;lia. assumption.
        + apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (y:=y);
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4.
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  rewrite decompose_elt_list_member_not with (y:=y);
     try assumption. 
     assert(~ min2 <= x <= max1-1 /\ ~ max1 <= x <= max2 /\ elt_list_member x q2 = false).
     - split.
        + apply elt_list_difference_spec_1 
    with (s:=q1) (s':=Cons max1 max2 q2) (y:=y) in Hyp4;
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
      apply elt_list_member_inegality 
                 with (a:=min1) (b:=max1) (y:=y) in Hyp4;
         try assumption. lia. Inv_monotony (max1+2).
        + rewrite <- decompose_elt_list_member_not with (y:=y);
             try Zbool_constructor.
          apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y);
      try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor).
      
    - decompose [and] H15. split;[lia|assumption].
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. simpl in Hyp4. 
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  case_eq(x <=? min2-1);intro Hyp5.
       apply elt_list_member_inegality_3 with (y:=y);
          try assumption. Zbool_lia;lia.
       rewrite Hyp5 in Hyp4.
       rewrite decompose_elt_list_member_not with (y:=y);
          try assumption. 
     assert(~ min2 <= x <= max1-1 /\ ~ max1 <= x <= max2 /\ elt_list_member x q2 = false).
     - split.
        + apply elt_list_difference_spec_1 
    with (s:=q1) (s':=Cons max1 max2 q2) (y:=y) in Hyp4;
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
      apply elt_list_member_inegality 
                 with (a:=min1) (b:=max1) (y:=y) in Hyp4;
         try assumption. lia. Inv_monotony (max1+2).
        + rewrite <- decompose_elt_list_member_not with (y:=y);
             try Zbool_constructor.
          apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y);
      try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor).
      
    - decompose [and] H15. split;[lia|assumption].
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. simpl in Hyp4. 
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  
  rewrite decompose_elt_list_member_not with (y:=y);
          try assumption. 
  split.
        + apply elt_list_difference_spec_1 
    with (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y:=y) in Hyp4;
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
      apply elt_list_member_inegality_2 
                 with (y:=max2+1) in Hyp4;
         try assumption. lia. Zbool_constructor.
        + apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y:=y);
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. simpl in Hyp4.
  inversion Hyp3. rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  case_eq(x <=? min2-1);intro Hyp5.
       apply elt_list_member_inegality_3 with (y:=y);
          try assumption. Zbool_lia;lia.
       rewrite Hyp5 in Hyp4.
       rewrite decompose_elt_list_member_not with (y:=y);
          try assumption. 
       split.
        + apply elt_list_difference_spec_1 
    with (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y:=y) in Hyp4;
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
      apply elt_list_member_inegality_2 
                 with (y:=max2+1) in Hyp4;
         try assumption. lia. Zbool_constructor.
        + apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y:=y);
      try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
Qed.

Lemma equiv_is_empty_elt_list_is_empty_2 : forall d,
Inv_t d ->
is_empty d = true <-> d = empty.
Proof.
split;intros.
 * apply equiv_is_empty_elt_list_is_empty in H0; try assumption.
   apply elt_list_is_empty_spec in H0 ; try assumption.
   apply equiv_empty_Nil;assumption.
 * apply equiv_is_empty_elt_list_is_empty; try assumption.
   apply elt_list_is_empty_spec ; try assumption.
   apply equiv_empty_Nil;assumption.
Qed.

Theorem difference_spec_2 : forall s s' x,
Inv_t s ->
Inv_t s' ->
member x (difference s s') = true ->
member x s' = false.
Proof.
intros s s' x Hyp1 Hyp2 Hyp3.
unfold member. unfold member in Hyp3.
unfold difference in Hyp3.
case_eq(Zeq_bool (size s') 0);intro Hyp4;rewrite Hyp4 in Hyp3.
  + assert(is_empty s' = true). unfold is_empty. assumption.
    apply equiv_is_empty_elt_list_is_empty_2 in H; try assumption. rewrite H. auto.
  + case_eq(elt_list_difference (domain s, domain s'));intros.
      - rewrite H in Hyp3. simpl in Hyp3. inversion Hyp3.
      - rewrite H in Hyp3. unfold domain in Hyp3. 
        rewrite <- H in Hyp3.
        unfold Inv_t in Hyp1. unfold Inv_t in Hyp2.
        decompose [and] Hyp1. decompose [and] Hyp2.
        
        case_eq(min s <=? min s');intro Hyp5;Zbool_lia.
          * apply elt_list_difference_spec_2 
              with (double:=(domain s, domain s')) 
                   (s:=domain s) (y:=min s);
              try (assumption||Inv_monotony (min s')||reflexivity).
          * apply elt_list_difference_spec_2 
              with (double:=(domain s, domain s')) 
                   (s:=domain s) (y:=min s');
              try (assumption||Inv_monotony (min s)||reflexivity).
Qed.

Theorem elt_list_difference_spec_3 : forall double s s' x y,
Inv_elt_list y s ->
Inv_elt_list y s' ->
double = (s,s') ->
elt_list_member x s = true ->
elt_list_member x s' = false ->
elt_list_member x (elt_list_difference double) = true.
Proof.
intros double.
functional induction (elt_list_difference double).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  inversion Hyp3. assumption.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  subst. inversion Hyp4.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  rewrite translation_In_elt_list_member with (y:=y0).
    + rewrite translation_In_elt_list_member with (y:=y0) in Hyp4;
         try Zbool_constructor.
      decompose [or] Hyp4;try tauto.
         right. rewrite <- elt_list_member_spec with (y:=y0).
           - apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (y:=y0);
               try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor). 
             rewrite elt_list_member_spec with (y:=max1+2);
             assumption.
           - apply elt_list_difference_inv with
               (double:=(q1, Cons min2 max2 q2)) (s:=q1) 
               (s':=Cons min2 max2 q2) (y1:=max1+2) (y2:=y0);
               try (Inv_monotony (max1+2)||reflexivity||assumption||
                    Zminmax_lia_goal;lia) .
     + constructor; try lia.
       apply elt_list_difference_inv with
               (double:=(q1, Cons min2 max2 q2)) (s:=q1) 
               (s':=Cons min2 max2 q2) (y1:=max1+2) (y2:=y0);
               try (Inv_monotony (max1+2)||reflexivity||assumption||
                    Zminmax_lia_goal;lia).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  simpl. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (y:=y);
       try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).
  apply elt_list_member_decreasing with (min2:=min2) (max2:=max2) (y:=y);assumption.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  simpl. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  rewrite translation_In_elt_list_member with (y:=y) in Hyp4;
         try Zbool_constructor.
  decompose [or] Hyp4.
     + rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
         try assumption.
       decompose [and] Hyp5. Zbool_lia;lia.
     + apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y);
       try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor). 
       rewrite <- elt_list_member_spec with (y:=y) in H15. 
       assumption. Inv_monotony (max1+2).
       rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
         try assumption. decompose [and] Hyp5.
       rewrite decompose_elt_list_member_not with (y:=y).
          split;[Zbool_lia;lia|assumption]. Zbool_constructor.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  simpl. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  rewrite translation_In_elt_list_member with (y:=y) in Hyp4;
         try Zbool_constructor.
  decompose [or] Hyp4.
     + case_eq(x <=? min2 - 1);intros.
          - Zbool_lia_goal;Zbool_lia;lia.
          - Zbool_lia. 
            rewrite decompose_elt_list_member_not 
               with (y:=y) in Hyp5. lia. assumption.
     + case_eq(x <=? min2 - 1);intros.
          - Zbool_lia_goal;Zbool_lia. 
            rewrite <- elt_list_member_spec 
              with (y:=max1+2) in H15; try assumption.
            apply elt_list_member_inegality 
              with (a:=min1) (b:=max1) (y:=y) in H15. 
            lia. assumption.
          - apply IHe with (s:=q1) (s':=Cons max1 max2 q2) (y:=y);
       try (Inv_monotony (max1+2)||reflexivity||assumption||Zbool_constructor).
       rewrite <- elt_list_member_spec with (y:=y) in H15. 
       assumption. Inv_monotony (max1+2). 
       rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
         try assumption. decompose [and] Hyp5.
       rewrite decompose_elt_list_member_not with (y:=y).
          split;[Zbool_lia;lia|assumption]. Zbool_constructor.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
  simpl. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  
  apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y:=y);
       try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).

  rewrite translation_In_elt_list_member with (y:=y);
    try Zbool_constructor.
  rewrite translation_In_elt_list_member with (y:=y) in Hyp4;
    try Zbool_constructor.
  decompose [or] Hyp4.
      + left. Zbool_lia. 
        rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
          try assumption. lia.
      + tauto.
      + apply elt_list_member_decreasing 
           with (min2:=min2) (max2:=max2) (y:=y);assumption.
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp1. rewrite <- H0 in Hyp4. inversion Hyp1.
  rewrite <- H1 in Hyp2. rewrite <- H1 in Hyp5. inversion Hyp2.
  assert(min1 <= x <= min2-1 \/ min2 <= x <= max1 \/ In x (elt_list_values q1)).
    - rewrite translation_In_elt_list_member with (y:=y) in Hyp4;
        try Zbool_constructor.
      decompose [or] Hyp4.
         + assert(min1 <= x <= min2 - 1 \/ min2 <= x <= max1).
           lia. tauto.
         + tauto.
    - rewrite translation_In_elt_list_member with (y:=y). 
      decompose [or] H15.
         + tauto.
         + rewrite <- elt_list_member_spec with (y:=y).
           right. 
           rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
              try assumption.
  
  apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y:=y);
       try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).

  rewrite translation_In_elt_list_member with (y:=y);
    try (Zbool_constructor). tauto.
  apply elt_list_difference_inv with
     (double:=(Cons (max2 + 1) max1 q1, q2)) 
     (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y1:=max2+1) (y2:=y);
         try (Inv_monotony (max2+2)||reflexivity||assumption||
              Zminmax_lia_goal;lia||Zbool_constructor).
              
       + rewrite <- elt_list_member_spec with (y:=y).
           right. 
           rewrite decompose_elt_list_member_not with (y:=y) in Hyp5;
              try assumption.
  
  apply IHe with (s:=Cons (max2+1) max1 q1) (s':=q2) (y:=y);
       try (Inv_monotony (max2+2)||reflexivity||assumption||Zbool_constructor).

  rewrite translation_In_elt_list_member with (y:=y);
    try (Zbool_constructor). tauto.
  apply elt_list_difference_inv with
     (double:=(Cons (max2 + 1) max1 q1, q2)) 
     (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y1:=max2+1) (y2:=y);
         try (Inv_monotony (max2+2)||reflexivity||assumption||
              Zminmax_lia_goal;lia||Zbool_constructor).
    
    + constructor. lia. Zbool_lia;lia.
     apply elt_list_difference_inv with
     (double:=(Cons (max2 + 1) max1 q1, q2)) 
     (s:=Cons (max2 + 1) max1 q1) (s':=q2) (y1:=max2+1) (y2:=y);
         try (Inv_monotony (max2+2)||reflexivity||assumption||
              Zminmax_lia_goal;lia||Zbool_constructor).
Qed.

Lemma inclu : forall double s s' y,
Inv_elt_list y s ->
Inv_elt_list y s' ->
double = (s, s') ->
elt_list_difference double = Nil ->
(forall z, elt_list_member z s  = true -> 
           elt_list_member z s' = true ).
Proof.
intros double.
functional induction (elt_list_difference double).
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  inversion Hyp3. subst. inversion Hyp5.
* intros s s' y0 Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  inversion Hyp3. subst. inversion Hyp5.
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5. inversion Hyp4.
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  inversion Hyp3. subst. 
  rewrite <- decompose_elt_list_member with (y:=y);
     try assumption. right. inversion Hyp2. Zbool_lia.
  apply IHe with (s:=Cons min1 max1 q1) (y:=y);
     try (assumption||reflexivity||Inv_monotony (max2+2)).
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  inversion Hyp3. subst. inversion Hyp1. inversion Hyp2.
  rewrite <- decompose_elt_list_member with (y:=y) in Hyp5;
     try assumption.
  rewrite <- decompose_elt_list_member with (y:=y);
     try assumption.
  decompose [or] Hyp5.
     + left. Zbool_lia;lia.
     + assert(max1 <= x <= max2 \/ elt_list_member x q2 = true).
       rewrite decompose_elt_list_member with (y:=y);
          try Zbool_constructor. Zbool_lia.
       apply IHe with (s:=q1) (y:=y);
          try (assumption||reflexivity||Inv_monotony (max1+2)||Zbool_constructor). 
        decompose [or] H14.
           - left. Zbool_lia;lia.
           - tauto.
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5. inversion Hyp4.
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5.
  inversion Hyp3. subst. inversion Hyp1. inversion Hyp2.
  rewrite <- decompose_elt_list_member with (y:=y) in Hyp5;
     try assumption.
  rewrite <- decompose_elt_list_member with (y:=y);
     try assumption.
     
  assert(min1 <= x <= max2 \/ (max2+1 <= x <= max1 \/       
         elt_list_member x q1 = true)).
    - decompose [or] Hyp5.
        + assert(min1 <= x <= max2 \/ max2 + 1 <= x <= max1)
           as Hyp6 by (lia). tauto.
        + tauto. 
    - decompose [or] H13.
        + left. Zbool_lia;lia.
        + right.
          apply IHe with (s:=Cons (max2+1) max1 q1) (y:=y);
            try (assumption||reflexivity||Inv_monotony (max2+2)||Zbool_constructor).
          apply decompose_elt_list_member with (y:=y);
            try (Zbool_lia;Zbool_constructor).
        + right.
          apply IHe with (s:=Cons (max2+1) max1 q1) (y:=y);
            try (assumption||reflexivity||Inv_monotony (max2+2)||Zbool_constructor).
          apply decompose_elt_list_member with (y:=y);
            try (Zbool_lia;Zbool_constructor).
* intros s s' y Hyp1 Hyp2 Hyp3 Hyp4 x Hyp5. inversion Hyp4.
Qed.

Theorem difference_spec_3 : forall s s' x,
Inv_t s ->
Inv_t s' ->
member x s = true ->
member x s' = false ->
member x (difference s s') = true.
Proof.
intros s s' x Hyp1 Hyp2 Hyp3 Hyp4.
unfold member. unfold member in Hyp3. unfold member in Hyp4.
unfold difference.
case_eq(Zeq_bool (size s') 0);intro Hyp5.
  + assumption.
  + case_eq(elt_list_difference (domain s, domain s'));intros. 
      - assert(elt_list_member x (domain s') = true) as Hyp6.
        unfold Inv_t in Hyp1. unfold Inv_t in Hyp2.
        decompose [and] Hyp1. decompose [and] Hyp2.
        case_eq(min s <=? min s');intro Hyp6.
          * Zbool_lia. 
            apply inclu with (double:=(domain s, domain s')) 
               (s:=domain s) (y:=min s); 
               (reflexivity||Inv_monotony (min s')||assumption).
          * Zbool_lia. 
            apply inclu with (double:=(domain s, domain s')) 
               (s:=domain s) (y:=min s');
               (reflexivity||Inv_monotony (min s)||assumption).
          * rewrite Hyp6 in Hyp4. inversion Hyp4.
      - unfold domain. rewrite <- H.
        unfold Inv_t in Hyp1. unfold Inv_t in Hyp2.
        decompose [and] Hyp1. decompose [and] Hyp2.
        
        case_eq(min s <=? min s');intro Hyp6;Zbool_lia.
          * apply elt_list_difference_spec_3 
              with (double:=(domain s, domain s')) 
                   (s:=domain s) (s':=domain s') (y:=min s);
              try (assumption||Inv_monotony (min s')||reflexivity).
          * apply elt_list_difference_spec_3
              with (double:=(domain s, domain s')) 
                   (s:=domain s) (s':=domain s') (y:=min s');
              try (assumption||Inv_monotony (min s)||reflexivity).
Qed.

Require Import R.Prelude.structure R.FBool.member R.Prelude.tactics R.FDomain.filter.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition partition p d :=
  (R.FDomain.filter.filter p d, 
   R.FDomain.filter.filter (fun x => negb (p x)) d).

(************************************)
(** * Preservation of the invariant *)
(************************************)

(************************************)
(** * Specification                 *)
(************************************)
Theorem partition_spec_1 : forall d p x, Inv_t d ->
member x (fst (partition p d)) = true <-> member x d = true /\ p x = true.
Proof.
unfold partition. unfold fst. apply filter_spec.
Qed.

Theorem partition_spec_2 : forall d p x, Inv_t d ->
member x (snd (partition p d)) = true <-> member x d = true /\ p x = false.
Proof.
unfold partition. unfold snd.
split; intro Hyp.
  + apply filter_spec in Hyp; try assumption.
    decompose [and] Hyp.
    split.
      - assumption.
      - unfold negb in H1. 
        case_eq(p x);intro Hyp2;rewrite Hyp2 in H1.
          * inversion H1.
          * auto.
  + apply filter_spec; try assumption.
    decompose [and] Hyp.
    split.
      - assumption.
      - unfold negb. 
        case_eq(p x);intro Hyp2;rewrite Hyp2 in H1.
          * inversion H1.
          * auto.
Qed.
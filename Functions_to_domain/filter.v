Require Import R.Prelude.structure R.FBool.member R.Prelude.tactics.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Function filter_elt_int (p_min_x_max_acc : ((Z->bool)*(Z*Z))*(Z*elt_list))
{measure minus_pair_other}
:= match p_min_x_max_acc with
|((p, (min, x)), (max, acc)) =>
       if (x <? max) && (p x) then
             filter_elt_int ((p,(min,(x+1))),(max,acc))
       else
             if Zgt_bool x max then Cons min max acc (* x = max+1 *)
             else if Zeq_bool x max then
                     if p x then Cons min max acc
                     else
                         if Zeq_bool x min then acc
                         else Cons min (max-1) acc
                  else if Zeq_bool x min 
                       then filter_elt_int ((p,((min+1),(x+1))),(max,acc))
                       else Cons min (x-1) (filter_elt_int ((p,((x+1),(x+1))),(max,acc)))
end.
Proof.
- intros;simpl;apply Zabs_nat_lt.
  rewrite andb_true_iff in teq3. decompose [and] teq3.
  Zbool_lia. lia.
- intros;simpl;apply Zabs_nat_lt;Zbool_lia;lia.
- intros;simpl;apply Zabs_nat_lt;Zbool_lia;lia.
Defined.

Fixpoint filter_elt_list p l := match l with
|Nil => Nil
|Cons a b t => filter_elt_int (p,(a,a),(b, (filter_elt_list p t)))
end.

Definition filter p d := let res := filter_elt_list p (domain d) in
mk_t res
(process_size res)
(process_max res)
(get_min res min_int).

(*******************************************************)
(*                       Tests                         *)
(*******************************************************)
(*
Definition p_test x := x mod 2 =? 0.
Eval compute in 0 =? 0.
Eval compute in p_test 2.

Eval compute in filter_elt_int (p_test,(1,1),(2,Nil)).

Eval compute in filter_elt_list p_test (Cons 1 2 (Cons 5 6 (Cons 78 80 Nil))).
Eval compute in filter_elt_list p_test (Cons 1 2 (Cons 5 6 Nil)).
Eval compute in filter_elt_list p_test (Cons 1 2 (Cons 5 7 Nil)).
Eval compute in filter_elt_list p_test (Cons 1 2 (Cons 5 7 (Cons 78 80 Nil))).
Eval compute in filter_elt_list p_test (Cons 2 3 (Cons 5 7 (Cons 78 80 Nil))).
Eval compute in filter_elt_list p_test (Cons 2 2 (Cons 5 7 (Cons 78 79 Nil))).

Eval compute in filter_elt_list p_test (Cons 3 3 (Cons 5 7 (Cons 78 79 Nil))).


Eval compute in filter p_test (create (elt_list_values (Cons 1 2 (Cons 5 6 (Cons 78 80 Nil))))).
Eval compute in filter p_test (create (elt_list_values (Cons 1 2 (Cons 5 6 Nil)))).
Eval compute in filter p_test (create (elt_list_values (Cons 1 2 (Cons 5 7 Nil)))).
Eval compute in filter p_test (create (elt_list_values (Cons 1 2 (Cons 5 7 (Cons 78 80 Nil))))).
Eval compute in filter p_test (create (elt_list_values (Cons 2 3 (Cons 5 7 (Cons 78 80 Nil))))).
Eval compute in filter p_test (create (elt_list_values (Cons 2 2 (Cons 5 7 (Cons 78 79 Nil))))).
*)

(************************************)
(** * Preservation of the invariant *)
(************************************)
Theorem filter_elt_int_inv : forall quintet p a b q x y,
Inv_elt_list y (Cons a b q) ->
quintet = ((p, (a,x)),(b,q)) ->
a <= x <= b ->
Inv_elt_list y (filter_elt_int quintet).
Proof.
intros quintet.
functional induction (filter_elt_int quintet).
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst.
  apply andb_true_iff in e0 ; decompose [and] e0.
  apply IHe with (p:=p0) (a0:=a) (x:=x0+1) (b0:=b) (q0:=q) (y:=z).
  assumption.
  reflexivity. Zbool_lia;lia.
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst. assumption.
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst. assumption.
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst. 
  inversion Hinv. Inv_monotony (b+2).
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst.
  Zbool_lia. inversion Hinv.
  apply inv_elt_list_restrict with (a:=a) (b:=b);(lia||assumption).
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst.
  apply IHe with (p:=p0) (a0:=a+1) (x:=x0+1) (b0:=b) (q0:=q) (y:=z).
  Zbool_lia. inversion Hinv.
  apply inv_elt_list_restrict with (a:=a) (b:=b);(lia||assumption).
  reflexivity. Zbool_lia;lia.
* intros p0 a b q x0 z Hinv Hyp1 Hyp2. inversion Hyp1. subst.
  constructor.
    + inversion Hinv. assumption.
    + Zbool_lia;lia.
    + apply IHe with (p:=p0) (a:=x0+1) (x:=x0+1) (b0:=b) (q0:=q) (y:=x0-1+2).
         Zbool_lia. inversion Hinv. Zbool_constructor.   
         reflexivity. Zbool_lia;lia.
Qed.

Theorem filter_elt_list_inv :
forall p l y, Inv_elt_list y l ->
Inv_elt_list y (filter_elt_list p l).
Proof.
induction l.
* intros y Hinv1. simpl. constructor.
* intros y Hinv1. simpl. inversion Hinv1.
  apply filter_elt_int_inv with (p:=p) (a:=z) (b:=z0)
    (q:=filter_elt_list p l) (x:=z).
       + constructor;try lia.
         apply IHl with (y:=z0+2). assumption.
       + reflexivity.
       + lia.
Qed.

Lemma Inv_elt_list_get_min : forall l y z,
Inv_elt_list y l -> z <= (get_min l z) ->
Inv_elt_list z l.
Proof.
induction 1 ; intros.
+ constructor.
+  simpl in H2.
constructor ; auto.
Qed.

Theorem filter_inv :
forall p d, Inv_t d -> Inv_t (filter p d).
Proof.
intros p d Hyp.
unfold filter. unfold Inv_t.
unfold Inv_t in Hyp. decompose [and] Hyp. simpl. repeat split.
apply filter_elt_list_inv with (p:=p) in H.
apply Inv_elt_list_get_min with (y:=min d) ; auto.
unfold get_min.
case (filter_elt_list p (domain d)).
lia. intros ; lia.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Theorem filter_elt_int_spec_1 : forall quintet p a b q x y z,
Inv_elt_list y (Cons a b q) ->
a <= x <= b+1 ->
(forall z, a <= z < x -> p z = true) ->
quintet = ((p, (a,x)),(b,q)) ->
elt_list_member z (filter_elt_int quintet) = true ->
 (a <= z <= b /\ p z = true) \/
elt_list_member z q = true.
Proof.
intros quintet.
functional induction (filter_elt_int quintet) ;
intros p0 a b q x0 y z Hinv Hx Hz Hyp1 Hyp2 ; inversion Hyp1 ; subst ; clear Hyp1 ; Zbool_lia.
* apply andb_true_iff in e0 ; decompose [and] e0.
  apply IHe with (x:=x0+1) (y:=y); auto. Zbool_lia; lia.
  intros. assert (a <= z0 <= x0 ) by lia.
  elim (Z_le_lt_eq_dec z0 x0) .
  intro Hyp ; apply Hz ; lia.
  intro Hyp ; congruence.
  lia.
* assert (x0 = b+1) by lia.
  apply elt_list_member_cons in Hyp2.
  destruct Hyp2 as [Hyp3 | Hyp4].
  left; split ; auto. apply Hz ; lia.
  tauto.
* apply elt_list_member_cons in Hyp2.
  destruct Hyp2 as [Hyp3 | Hyp4].
  left; split ; auto. rewrite <- e2 in Hyp3.
  elim (Z_le_lt_eq_dec z x0) .
  intro Hyp ; apply Hz ; lia.
  intro Hyp ; congruence.
  lia.
  tauto.
* tauto.
* apply elt_list_member_cons in Hyp2.
  destruct Hyp2 as [Hyp3 | Hyp4].
  left; split ; auto. lia.
  apply Hz ; lia.
  tauto.
* apply IHe with (a0:=a+1) (x:=x0+1) (p:=p0) (b0:=b) (q0:=q) (y:=y) in Hyp2 ; auto.
  destruct Hyp2 as [Hyp3 | Hyp4]. destruct Hyp3.
  left ; split ; auto. lia. tauto.
  inversion Hinv.
  constructor ; try lia. auto. lia.
  intros ;  lia.
* apply elt_list_member_cons in Hyp2. destruct Hyp2 as [Hyp3 | Hyp4].
  left ; split. lia. apply Hz ; lia.
  eapply IHe with (y:=y) in Hyp4 ; auto.
  destruct Hyp4 as [Hyp5 | Hyp6]. destruct Hyp5.
  left; split; auto . lia. tauto.
  inversion Hinv.
  constructor ; try lia. auto. lia.
  intros ; lia.
Qed.

Lemma  elt_list_member_le_bis :
forall x l y, Inv_elt_list y l ->
elt_list_member x l = true -> y <= x.
Proof.
induction l;intros y Hyp1 Hyp2.
- simpl in Hyp2. discriminate Hyp2.
- simpl. inversion Hyp1. simpl in Hyp2.
  case_eq(x <=? z0);intro Hyp3;rewrite Hyp3 in Hyp2;Zbool_lia;lia.
Qed.

Lemma filter_elt_list_spec_right : forall l p y x, Inv_elt_list y l ->
elt_list_member x (filter_elt_list p l) = true ->
elt_list_member x l = true /\ p x = true.
Proof.
induction l.
- intros p y x Hyp_inv. simpl. intro pb ; inversion pb.
- intros p y x Hyp_inv HypS.
  simpl in HypS. inversion_clear Hyp_inv.
  eapply filter_elt_int_spec_1 with (y:=y) in HypS ; auto.
  + destruct HypS as [Hyp1 | Hyp2].
    * destruct Hyp1.
    split ; auto.
    simpl.
    assert (x <=? z0 = true) as Hge by (Zbool_lia_goal; lia).
    rewrite Hge ; Zbool_lia_goal; lia.
    * apply IHl with (y:=z0+2) in Hyp2 ; auto. destruct Hyp2.
    split ; auto.
    simpl.
    assert ( x <=? z0 = false) as Hyp.
    Zbool_lia_goal.
    apply elt_list_member_le with (x := x) in H1 ; [lia | assumption].
    rewrite Hyp ; assumption.
  + constructor ; auto. apply filter_elt_list_inv ; auto.
  + lia.
  + intros ; lia.
Qed.
 
Theorem filter_elt_int_spec_2 : forall quintet p a b q x y z,
Inv_elt_list y (Cons a b q) ->
a <= x <= b+1 ->
(forall z, a <= z < x -> p z = true) ->
quintet = ((p, (a,x)),(b,q)) ->
(a <= z <= b /\ p z = true) \/  elt_list_member z q = true ->
  elt_list_member z (filter_elt_int quintet) = true .
Proof.
intros quintet.
functional induction (filter_elt_int quintet) ;
intros p0 a b q x0 y z Hinv Hx Hz Hyp1 Hyp2 ; inversion Hyp1 ; subst ; clear Hyp1 ; Zbool_lia.
* apply andb_true_iff in e0 ; decompose [and] e0.
  apply IHe with (x:=x0+1) (y:=y) (p:=p0) (a0:=a) (b0 :=b) (q0:=q); auto. Zbool_lia; lia.
  intros. assert (a <= z0 <= x0 ) by lia.
  elim (Z_le_lt_eq_dec z0 x0) .
  intro Hyp ; apply Hz ; lia.
  intro Hyp ; congruence.
  lia.
* assert (x0 = b+1) by lia.
  destruct Hyp2 as [Hyp3 | Hyp4].
  - simpl ; destruct Hyp3. 
    assert (z <=? b = true) as Hle by (Zbool_lia_goal ; lia) .
    rewrite Hle ; Zbool_lia_goal ; lia.
  - inversion_clear Hinv.
    apply elt_list_member_le with (x := z) in H2 ; auto.
    simpl.
    assert (z <=? b = false) as Hyp by (Zbool_lia_goal ; lia).
    rewrite Hyp ; auto.
* destruct Hyp2 as [Hyp3 | Hyp4].
  - simpl ; destruct Hyp3. 
    assert (z <=? b = true) as Hle by (Zbool_lia_goal ; lia) .
    rewrite Hle ; Zbool_lia_goal ; lia.
  - inversion_clear Hinv.
    apply elt_list_member_le with (x := z) in H1 ; auto.
    simpl.
    assert (z <=? b = false) as Hyp by (Zbool_lia_goal ; lia).
    rewrite Hyp ; auto.
* subst.
  inversion_clear Hinv. destruct Hyp2 as [Hyp3 | Hyp4].
  - destruct Hyp3 as [Hyp4 Hyp5].
    assert (z=a) by lia. subst. rewrite Hyp5 in e3 ; inversion e3.
  - assumption.
* subst.
  inversion_clear Hinv. destruct Hyp2 as [Hyp3 | Hyp4].
  - destruct Hyp3 as [Hyp4 Hyp5].
    simpl.
    assert (z <=? (b-1) = true) as Hyp.
    Zbool_lia_goal .
    assert (z <> b). intro Hpb. subst. rewrite e3 in Hyp5 ; inversion Hyp5.
    lia.
    rewrite Hyp. Zbool_lia_goal ; lia.
  - apply elt_list_member_le with (x := z) in H1 ; auto.
    simpl.
    assert (z <=? b -1 = false) as Hyp by (Zbool_lia_goal ; lia).
    rewrite Hyp ; auto.
*  assert (x0 <? b = true) as Hyp by (Zbool_lia_goal ; lia).
   rewrite Hyp in e0. simpl in e0.
   inversion_clear Hinv.
   apply IHe with (x:=x0+1) (y:=y) (p:=p0) (a0:=a+1) (b0 :=b) (q0:=q); auto.
   constructor ; [lia | lia | assumption].
   lia. intros; apply Hz ; lia.
   destruct Hyp2 as [Hyp3 | Hyp4].
   - destruct Hyp3.
   left ; split ; auto.
   assert (z <> a) by (intro Hpb; subst; rewrite H3 in e0 ; inversion e0).
   lia.
   - tauto.
*  assert (x0 <? b = true) as Hyp by (Zbool_lia_goal ; lia).
   rewrite Hyp in e0. simpl in e0.
   inversion_clear Hinv.
   destruct Hyp2 as [Hyp3 | Hyp4].
   - simpl . destruct Hyp3 as [Hyp5 Hyp6].
   case_eq (z <=? x0 - 1) ; intro.
   Zbool_lia_goal ; lia.
   assert (x0 <= z) by (Zbool_lia; lia).
   apply IHe with (a:= x0 +1) (x := x0 + 1) (b0 := b) (q0 := q) (p := p0) (y:=x0) ; auto.
   constructor ; [lia | lia |  assumption].
   lia.
   intros. apply Hz ; lia.
   left ; split ; auto. Zbool_lia . split ; try lia.
   assert (x0 <> z). intro pb. subst. rewrite Hyp6 in e0 ; inversion e0.
   lia.
   - pose proof (elt_list_member_le _ _ _ H1 Hyp4 ) as HH.
   simpl .
   case_eq (z <=? x0 - 1) ; intro.
   Zbool_lia_goal ; lia.
   apply IHe with (a:= x0 +1) (x := x0 + 1) (b0 := b) (q0 := q) (p := p0) (y:=x0); auto.
   constructor ; [lia | lia | assumption].
   lia.
   intros; apply Hz ; lia.
Qed.

Lemma filter_elt_list_spec_left : forall l p y x, Inv_elt_list y l ->
elt_list_member x l = true -> p x = true ->
elt_list_member x (filter_elt_list p l) = true.
Proof.
induction l.
 - intros p y x Hyp_inv. simpl. intro pb ; inversion pb.
 - intros p y x Hyp_inv. inversion_clear Hyp_inv.
   simpl. intros Hyp Hpx.
   apply filter_elt_int_spec_2
     with (p:=p) (a:=z) (x:=z) (b:=z0) (q:=filter_elt_list p l) (y:=y) ; auto ; try (intros ; lia).
      + constructor ; try lia. apply filter_elt_list_inv ; auto.
      + intros;
        case_eq (x <=? z0) ; intro Heq ; rewrite Heq in Hyp; Zbool_lia.
           * left ; split  ; [lia | auto].
           * right; apply IHl with (y := z0 + 2) ; auto.
Qed.

Theorem filter_elt_list_spec : forall l p y x, Inv_elt_list y l ->
elt_list_member x (filter_elt_list p l) = true <-> 
elt_list_member x l = true /\ p x = true.
Proof.
split.
 + apply filter_elt_list_spec_right with (y:=y);assumption.
 + intro Hyp. decompose [and] Hyp.
   apply filter_elt_list_spec_left with (y:=y);assumption.
Qed.

Theorem filter_spec : forall d p x, Inv_t d ->
member x (filter p d) = true <-> member x d = true /\ p x = true.
Proof.
intros d p x Hyp. split;intro HypS.    
   * unfold filter in HypS. unfold member in HypS. simpl in HypS.
     rewrite filter_elt_list_spec with (y:=min d) in HypS. assumption.
     unfold Inv_t in Hyp. decompose [and] Hyp. assumption.
   * unfold member in HypS. unfold filter. unfold member. simpl.
     rewrite filter_elt_list_spec with (y:=min d). assumption.
     unfold Inv_t in Hyp. decompose [and] Hyp. assumption.
Qed.
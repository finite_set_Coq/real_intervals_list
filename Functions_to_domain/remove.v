Require Import R.Prelude.structure R.Prelude.tactics R.CD.empty R.FValue.max R.FValue.size R.FValue.min R.FBool.is_empty R.FBool.member.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)

(* elt_list_remove
*    The first component of the couple indicates whether or 
     not the list was modified.
*)
Fixpoint elt_list_remove (l : elt_list) (x : Z) : bool * elt_list := match l with
|Nil => (false,Nil)
|Cons min max q => if Zle_bool x max then
                      if Zeq_bool x max then
                              if Zeq_bool min max then (true, q)
                              else (true, (Cons min (max-1) q))
                      else if Zge_bool x min then
                              if Zeq_bool x min then (true, (Cons (min+1) max q))
                              else (true,Cons min (x-1) (Cons (x+1) max q))
                      else (false,Cons min max q)
                   else let res := elt_list_remove q x in (fst res, Cons min max (snd res))
end.

Functional Scheme elt_list_remove_ind := Induction for elt_list_remove Sort Prop.
(*
Definition remove (x : Z) (d : t) : t :=
          if ((Zlt_bool x (min d))||(Zgt_bool x (structure.max d)))%bool then d (*x n'appartient pas à d*)
          else let newl := (elt_list_remove (domain d) x) in
                  if negb (fst newl) then d  (*si nous n'avons pas touché à la liste*)
                  else if elt_list_is_empty (snd newl) then empty  (*sinon, nous vérifions que la liste est vide car nous avons déjà créé empty*)
                  else let newm := if Zeq_bool x (structure.max d) then process_max (snd newl) else (structure.max d) in
                       let res := mk_t (snd newl) ((size d)-1) newm (get_min (snd newl) (min_int)) in res.
                       
Functional Scheme remove_ind := Induction for remove Sort Prop.
*)
(*
Definition remove (x : Z) (d : t) : t :=
          if ((Zlt_bool x (min d))||(Zgt_bool x (structure.max d)))%bool then d (*x n'appartient pas à d*)
          else match (elt_list_remove (domain d) x) with
               (newl1, newl2) =>
                  if negb newl1 then d  (*si nous n'avons pas touché à la liste*)
                  else if elt_list_is_empty newl2 then empty  (*sinon, nous vérifions que la liste est vide car nous avons déjà créé empty*)
                  else if Zeq_bool x (structure.max d) 
                       then mk_t newl2 ((size d)-1) (process_max newl2) (get_min newl2 (min_int)) 
                       else mk_t newl2 ((size d)-1) (structure.max d) (get_min newl2 (min_int))
                end.
Functional Scheme remove_ind := Induction for remove Sort Prop.
*)

Definition remove (x : Z) (d : t) : t :=
          if ((Zlt_bool x (min d))||(Zgt_bool x (structure.max d)))%bool then d (* x does not belong to d *)
          else let newl := (elt_list_remove (domain d) x) in
                  if negb (fst newl) then d (* if we haven't modified the list  *)
                  else if elt_list_is_empty (snd newl) then empty  
                  (* otherwise, we check that the list is empty 
                                because we have already created empty  *)
                  else if Zeq_bool x (structure.max d) 
                       then mk_t (snd newl) ((size d)-1) (process_max (snd newl)) (get_min (snd newl) (min_int)) 
                       else mk_t (snd newl) ((size d)-1) (structure.max d) (get_min (snd newl) (min_int)).

                       
Functional Scheme remove_ind := Induction for remove Sort Prop.

(************************************)
(** * Preservation of the invariant *)
(************************************)

(** OBJECTIVE : Theorem remove_inv : forall (d : t),                    **)
(**             Inv_t d -> forall (e :Z), Inv_t (remove e d).           **)
            (** Proof.                                                  **)
            (** intros d Hyp e. unfold Inv_t. repeat split.             **)
            (** - apply remove_inv_domain ;assumption.  (1)             **)
            (** - apply remove_inv_min    ;assumption.  (2)             **)
            (** - apply remove_inv_max    ;assumption.  (3)             **)
            (** - apply remove_inv_size   ;assumption.  (4)             **)
            (** Qed. **)

(** First of all, we need this theorem :                     **)
(** Lemma remove_inv_domain_min_d : forall d e,              **)
(** Inv_t d ->                                               **)
(** Inv_elt_list (min d) (domain (remove e d)).              **)

Lemma elt_list_remove_inv : forall (l1 : elt_list) (e : Z) l2 b y,
elt_list_remove l1 e = (b, l2) ->
Inv_elt_list y l1 ->
Inv_elt_list y l2.
Proof.
intros l e.
apply elt_list_remove_ind.
- intros. inversion H. constructor.
- intros. inversion H. Zbool_lia. inversion H0. 
  rewrite H3 in H9. Inv_monotony (max+2).
- intros. inversion H. Zbool_lia. inversion H0.
  apply inv_elt_list_restrict 
    with (a:=min) (b:=max);(lia||assumption).
- intros. inversion H. Zbool_lia. inversion H0.
  apply inv_elt_list_restrict
    with (a:=min) (b:=max);(lia||assumption).
- intros. inversion H. Zbool_lia. inversion H0.
  constructor;try lia. constructor;(lia||assumption).
- intros. inversion H. assumption.
- intros. Zbool_lia. 
  case_eq res. intros. rewrite H2 in H0.
  simpl in H0. inversion H0. inversion H1.
  constructor;try lia.
  apply H with (l2:=e2) (b:=b) (y:=max+2).
     * destruct (elt_list_remove q x). rewrite <- H4. auto.
     * assumption.
Qed.

Theorem remove_inv_domain_min_d : 
forall d e, Inv_t d -> 
Inv_elt_list (min d) (domain (remove e d)).
Proof.
intros d e.
apply remove_ind.
- intros Hyp1 Hd. unfold Inv_t in Hd;decompose [and] Hd.   
  assumption.
- intros Hyp1 newl Hyp2 Hd. 
  unfold Inv_t in Hd;decompose [and] Hd. assumption.
- intros Hyp1 newl Hyp2 Hyp3 Hd. unfold empty. simpl. constructor.
- intros Hyp1 newl Hyp2 Hyp3 Hyp4 Hd.
  unfold Inv_t in Hd;decompose [and] Hd. simpl. 
  apply elt_list_remove_inv
    with (l1:=(domain d)) (e:=e) 
         (b:=fst (elt_list_remove (domain d) e)).
    * destruct (elt_list_remove (domain d) e). 
      inversion Hyp2. auto.
    * assumption.
- intros Hyp1 newl Hyp2 Hyp3 Hyp4 Hd.
  unfold Inv_t in Hd;decompose [and] Hd. simpl. 
  apply elt_list_remove_inv
    with (l1:=(domain d)) (e:=e) 
         (b:=fst (elt_list_remove (domain d) e)).
    * destruct (elt_list_remove (domain d) e). 
      inversion Hyp2. auto.
    * assumption.
Qed.

(*******************************************)
(** ** (1) : remove_inv_domain             *)
(*******************************************)

Lemma min_remove_ineg : forall x a l y, Inv_elt_list y l -> 
elt_list_is_empty (snd (elt_list_remove l x)) = false -> 
get_min l a <= get_min (snd (elt_list_remove l x)) a.
Proof.
induction l;intros y inv Hyp;simpl.
- lia.
- case_eq(x <=? z0);intro Hyp2.
   * case_eq(Zeq_bool x z0);intro Hyp3.
      + case_eq(Zeq_bool z z0);intro Hyp4.
           case_eq(l);simpl. 
              simpl in Hyp. 
              rewrite Hyp2 in Hyp;rewrite Hyp3 in Hyp;
              rewrite Hyp4 in Hyp;simpl in Hyp. 
              intro Hyp5;rewrite Hyp5 in Hyp. discriminate Hyp.
              
              intros z1 z2 e Hyp5. inversion inv. 
              rewrite Hyp5 in H5. inversion H5.  
              Zbool_lia;lia.
           simpl;lia.
       + case_eq(x >=? z);intro Hyp4.
           case_eq(Zeq_bool x z);intro Hyp5;simpl;lia.
           simpl;lia.
   * simpl;lia.
Qed.

Lemma min_remove_simpl : forall e0 a e z, a < e -> 
get_min (snd (elt_list_remove (Cons a z e0) e)) min_int = a.
Proof.
induction e0;intros a e c Hyp1;simpl.
- case_eq(e <=? c);intro Hyp2;auto.
    case_eq(Zeq_bool e c);intro Hyp3;auto.
      + case_eq(Zeq_bool a c);intro Hyp4;
            (Zbool_lia;lia||auto).
      + case_eq(e >=? a);intro Hyp4;auto.
          case_eq(Zeq_bool e a);intro Hyp5;
            (Zbool_lia;lia||auto).
- case_eq(e <=? c);intro Hyp2;auto.
    case_eq(Zeq_bool e c);intro Hyp3;auto.
      + case_eq(Zeq_bool a c);intro Hyp4;
            (Zbool_lia;lia||auto).
      + case_eq(e >=? a);intro Hyp4;auto.
          case_eq(Zeq_bool e a);intro Hyp5;
            (Zbool_lia;lia||auto).
Qed.

Lemma Morgan's_law : forall e d, 
((e <? min d) || (e >? structure.max d))%bool = false <-> 
(e >= min d) /\ (e <= structure.max d).
Proof.
split;intros.
- apply  orb_false_iff in H. decompose [and] H. 
  split;Zbool_lia;lia. 
- apply  orb_false_iff. decompose [and] H. 
  split;Zbool_lia_goal;lia.
Qed.

Lemma info_min : forall a z e0 d, Inv_t d -> 
domain d = Cons a z e0 -> min d = a.
Proof.
intros a z e0 d Hyp1 Hyp2. 
rewrite egality_min_get_min.
   + rewrite Hyp2;auto. 
   + assumption.
Qed.

Lemma borne_sup : 
forall l a b z z0 y, Inv_elt_list y (Cons a b (Cons z z0 l)) -> 
b < process_max (Cons a b (Cons z z0 l)).
Proof.
induction l;intros a b c d y Hyp1. 
- rewrite decreasingProcess_max. 
    + unfold process_max. unfold pm.
      inversion Hyp1. inversion H5. lia.
    + discriminate.
- rewrite decreasingProcess_max.
    + inversion Hyp1. 
      assert(d < process_max (Cons c d (Cons z z0 l))).
      apply IHl with (y:=b+2) (z:=z) (z0:=z0);assumption.      
      inversion H5. lia. 
    + discriminate.
Qed.

Lemma contradiction_max : forall d a b e0 x, Inv_t d -> 
domain d = Cons a b e0 -> e0 <> Nil -> 
x = structure.max d -> x = b -> False.
Proof.
intros d a b e0 x Hd Hyp1 Hyp2 Hyp3 Hyp4.
unfold Inv_t in Hd;decompose [and] Hd.
rewrite egality_max_process_max in Hyp3; try auto.
rewrite Hyp1 in Hyp3. 
case_eq(e0);intro.
  + contradiction.
  + intros. rewrite H2 in Hyp3. rewrite Hyp1 in H. 
    rewrite H2 in H.
    assert(b < process_max (Cons a b (Cons z z0 e))) 
     by (apply borne_sup 
           with (y:=min d) (a:=a) (z:=z);assumption).
    lia.
Qed.

Lemma min_diff_max : forall d a b e0 x, Inv_t d ->
domain d = Cons a b e0 -> 
((x <? min d) || (x >? structure.max d))%bool = false -> 
elt_list_is_empty (snd (elt_list_remove (domain d) x)) = false -> 
x = structure.max d -> x <> min d.
Proof.
intros d a b e0 x Hd Hyp2 Hyp3.
unfold Inv_t in Hd;decompose [and] Hd.
rewrite Morgan's_law in Hyp3. decompose [and] Hyp3.
rewrite equiv_elt_list_is_empty_Nil. rewrite Hyp2. simpl.
rewrite egality_min_get_min. rewrite Hyp2.
case_eq(x <=? b);intro Hyp5.
  * case_eq(Zeq_bool x b);intro Hyp6.
      + case_eq(Zeq_bool a b);intro Hyp7.
         - intros Hyp8 Hyp9. Zbool_lia. simpl. rewrite Hyp6.
           case_eq(e0);intro.
             simpl in Hyp8. contradiction.
             intros. assert(False)
                by (apply contradiction_max 
                     with (d:=d) (a:=a) (b:=b) 
                          (e0:=e0) (x:=x);assumption).
             contradiction.
         - intros Hyp8 Hyp9. Zbool_lia. simpl. 
           rewrite Hyp6. lia.
      + case_eq(x >=? a);intro Hyp7.
         - case_eq(Zeq_bool x a);intro Hyp8.
            intros Hyp9 Hyp10. Zbool_lia. simpl.
            assert(a < structure.max d). 
                
            case_eq(e0);intro.
              rewrite egality_max_process_max.
              rewrite H5 in Hyp2. rewrite Hyp2.
              unfold process_max. unfold pm. lia. assumption.
                       
              intros. rewrite egality_max_process_max.
              rewrite H5 in Hyp2. rewrite Hyp2. rewrite Hyp2 in H.
                 assert(b < process_max (Cons a b (Cons z z0 e))).
                   apply borne_sup with (y:=min d) (a:=a) (z:=z).
                       assumption.
                       lia. assumption. 
                 lia.
                
              intros Hyp9 Hyp10. Zbool_lia. simpl. assumption.
         - intros Hyp9 Hyp10. Zbool_lia. simpl. lia.
  * intros Hyp9 Hyp10. simpl. Zbool_lia. 
    rewrite Hyp2 in H. inversion H. lia.
  * assumption.
Qed.

Lemma remove_inv_case1 : forall d e, Inv_t d ->
(e <? min d) || (e >? structure.max d) = false ->
negb (fst (elt_list_remove (domain d) e)) = false ->
elt_list_is_empty (snd (elt_list_remove (domain d) e)) = false ->
Zeq_bool e (structure.max d) = true ->
Inv_elt_list
  (get_min (snd (elt_list_remove (domain d) e)) min_int)
  (snd (elt_list_remove (domain d) e)).
Proof.
intros d e Hd Hyp1 Hyp2 Hyp3 Hyp4. unfold Inv_t in Hd;decompose [and] Hd.
apply elt_list_remove_inv
with (l1:= (domain d)) (e:=e) (b:=fst (elt_list_remove (domain d) e)).
* destruct (elt_list_remove (domain d) e). simpl;reflexivity.
* case_eq(domain d);intro a.
    + simpl. apply invNil.
    + intros z e0 Hyp5.
      assert(e <> (min d)) 
         by (Zbool_lia; 
             apply min_diff_max with (a:=a) (b:=z) (e0:=e0);
             (assumption||lia)).
      assert (min d < e) 
         by (Zbool_lia; 
             apply Morgan's_law in Hyp1; 
             decompose [and] Hyp1; lia).
      rewrite Hyp5 in Hd. rewrite <- Hyp5.
      assert(min d = a) as Hyp6
         by (apply info_min in Hyp5;auto;unfold Inv_t;auto). 
      rewrite Hyp5 in H1.
      apply inv_elt_list_monoton with (y:=min d).
        - assumption.
        - assert(get_min (snd (elt_list_remove (Cons a z e0) e)) min_int = a) 
          as Hyp7 by (apply min_remove_simpl; lia).
          rewrite <- Hyp5 in Hyp7. rewrite Hyp7. lia.
Qed.

Lemma remove_inv_case2 : forall d e, Inv_t d ->
(e <? min d) || (e >? structure.max d) = false ->
negb (fst (elt_list_remove (domain d) e)) = false ->
elt_list_is_empty (snd (elt_list_remove (domain d) e)) = false ->
Zeq_bool e (structure.max d) = false ->
Inv_elt_list
  (get_min (snd (elt_list_remove (domain d) e)) min_int)
  (snd (elt_list_remove (domain d) e)).
Proof.
intros d e Hd Hyp1 Hyp2 Hyp3 Hyp4. unfold Inv_t in Hd;decompose [and] Hd.
case_eq(Z.eq_dec e (min d));intros b Hyp5.
- case_eq(domain d);simpl.
    intro. constructor.
    intros z z0 e0 Hyp7. 
    case_eq(e <=? z0);intro Hyp8.
      case_eq(Zeq_bool e z0);intro Hyp9.
       + case_eq(Zeq_bool z z0);intro Hyp10;simpl.
           case_eq(e0);intros.
             * apply equiv_elt_list_is_empty_Nil in Hyp3.
               rewrite Hyp7 in Hyp3. 
               unfold elt_list_remove in Hyp3.
               
               rewrite Hyp8 in Hyp3;rewrite Hyp9 in Hyp3;
               rewrite Hyp10 in Hyp3.
               
               simpl in Hyp3. contradiction.
                                      
             * rewrite Hyp7 in H. inversion H.
               rewrite H2 in H10. inversion H10. 
               constructor;(simpl;lia||assumption).
                                
             * rewrite Hyp7 in H. inversion H. 
               constructor;Zbool_lia;lia.

       + case_eq(e >=? z);intro Hyp10.
           case_eq(Zeq_bool e z);intro Hyp11;simpl.
              rewrite Hyp7 in H. Zbool_lia. inversion H. 
              constructor;(lia||assumption).
                                
              rewrite Hyp7 in H. Zbool_lia. inversion H. 
              constructor;lia.
      
           rewrite Hyp7 in H. inversion H.
           constructor;(simpl;lia||assumption).
       + simpl. rewrite Hyp7 in H. Zbool_lia. inversion H. 
         constructor;lia.
- apply elt_list_remove_inv with (l1:= domain d) (e:=e) (b:=fst (elt_list_remove (domain d) e)). 
    + destruct (elt_list_remove (domain d) e). auto.
    + case_eq(domain d);intro a. 
        simpl. apply invNil.
        intros z e0 Hyp6.
        assert (min d < e) 
          by (Zbool_lia; 
              apply Morgan's_law in Hyp1; 
              decompose [and] Hyp1; lia).
        rewrite Hyp6 in Hd. rewrite <- Hyp6.
        assert(min d = a) as Hyp7 
          by (apply info_min in Hyp6;auto;unfold Inv_t;auto;rewrite Hyp6; 
              assumption). 
        rewrite Hyp6 in H1.
        apply inv_elt_list_monoton with (y:=min d).
        assumption.
        assert(get_min (snd (elt_list_remove (Cons a z e0) e)) min_int = a) as Hyp8 
          by (apply min_remove_simpl; lia).
        rewrite <- Hyp6 in Hyp8. rewrite Hyp8. lia.
Qed.

Lemma remove_inv_domain : 
forall d e, Inv_t d -> 
Inv_elt_list (min (remove e d)) (domain (remove e d)).
Proof.
intros d e Hd. unfold Inv_t in Hd;decompose [and] Hd. 
unfold remove. 
case_eq(((e <? min d) || (e >? structure.max d))%bool);intro Hyp1.
 - assumption.
 - case_eq(negb (fst (elt_list_remove (domain d) e)));intro Hyp2.
    * assumption.
    * case_eq(elt_list_is_empty (snd (elt_list_remove (domain d) e)));intro Hyp3.
       + apply empty_inv.
       + case_eq(Zeq_bool e (structure.max d));intro Hyp4;simpl.
                   apply remove_inv_case1;assumption.
                   apply remove_inv_case2;assumption.
Qed.

(****************************************)
(** ** (2) : remove_inv_min             *)
(****************************************)

Lemma remove_inv_min : forall e d, Inv_t d -> 
min (remove e d) = get_min (domain (remove e d)) min_int.
Proof.
intros e d Hyp. unfold remove. 
case_eq(((e <? min d) || (e >? structure.max d))%bool);intro Hyp1.
 - apply egality_min_get_min. assumption.
 - case_eq(negb (fst (elt_list_remove (domain d) e)));intro Hyp2.
    * apply egality_min_get_min. assumption.
    * case_eq(elt_list_is_empty (snd (elt_list_remove (domain d) e)));intro Hyp3.
       + unfold empty;auto.
       + case_eq(Zeq_bool e (structure.max d));intro Hyp4;auto.
Qed.

(****************************************)
(** ** (3) : remove_inv_max             *)
(****************************************)

Lemma lastContradiction : forall e0 z z0 x min max b,
elt_list_remove (Cons z z0 e0) x = (b,Nil) ->
x <> process_max (Cons min max (Cons z z0 e0)) ->
(x <=? max) = false -> False.
Proof.
intros e0 z z0 x min max b Hyp1 Hyp2 Hyp3.
inversion Hyp1.
case_eq(x <=? z0);intro Hyp4;rewrite Hyp4 in H0.
  * case_eq(Zeq_bool x z0);intro Hyp5;rewrite Hyp5 in H0.
      + case_eq(Zeq_bool z z0);intro Hyp6;rewrite Hyp6 in H0.
          inversion H0. subst. unfold process_max in Hyp2.
          unfold pm in Hyp2. Zbool_lia;lia.
          
          inversion H0. 
       + case_eq(x >=? z);intro Hyp6;rewrite Hyp6 in H0.
            case_eq(Zeq_bool x z);intro Hyp7;rewrite Hyp7 in H0;
                 inversion H0.
            inversion H0.
  * inversion H0.
Qed.

Lemma sameMax : forall l e y l' b, Inv_elt_list y l ->
e <> process_max l ->
elt_list_remove l e = (b,l') ->
process_max l = process_max l'.
Proof.
intros l e.
apply elt_list_remove_ind.
- intros l0 x Hyp1 y l' b Hyp2 Hyp3 Hyp4. inversion Hyp4. auto.
- intros l0 x min max q Hyp1 Hyp2 Hyp3 Hyp4 y l' b Hyp5 Hyp6 Hyp7.
  inversion Hyp7. simpl. rewrite <- H1.
  case_eq(q);intros.
     * rewrite H in Hyp6. unfold process_max in Hyp6.
       unfold pm in Hyp6. Zbool_lia;lia.
     * rewrite decreasingProcess_max;[reflexivity|discriminate].
- intros l0 x min max q Hyp1 Hyp2 Hyp3 Hyp4 y l' b Hyp5 Hyp6 Hyp7.
  inversion Hyp7.
  case_eq(q);intros.
     * rewrite H in Hyp6. unfold process_max in Hyp6.
       unfold pm in Hyp6. Zbool_lia;lia.
     * rewrite decreasingProcess_max;[reflexivity|discriminate].
- intros l0 x min max q Hyp1 Hyp2 Hyp3 Hyp4 Hyp5 y l' b 
         Hyp6 Hyp7 Hyp8. inversion Hyp8. simpl. reflexivity.
- intros l0 x min max q Hyp1 Hyp2 Hyp3 Hyp4 Hyp5 y l' b 
         Hyp6 Hyp7 Hyp8. inversion Hyp8. simpl. reflexivity.
- intros l0 x min max q Hyp1 Hyp2 Hyp3 Hyp4 Hyp5 y l' b Hyp6 Hyp7.
  inversion Hyp7. simpl. reflexivity.
- intros l0 x min max q.
  case_eq(q);intros.
     * inversion H3. auto.
     * inversion H3. 
       case_eq res. intros b0 e3 HYP. rewrite HYP in H3.
       simpl in H3. inversion H3.
       case_eq(e3);intros.
          assert(False).
             apply lastContradiction with (e0:=e0) (z:=z) (z0:=z0)
              (x:=x) (min:=min) (max:=max) (b:=b0).
             rewrite <- H4. auto. assumption. assumption.
          contradiction.
       
          rewrite decreasingProcess_max. symmetry. 
          rewrite decreasingProcess_max.
          symmetry. simpl.
          apply H0 with (y:=max+2) (b:=b0). 
             + inversion H1. assumption.
             + rewrite decreasingProcess_max in H2. assumption.
               discriminate.
             + destruct (elt_list_remove (Cons z z0 e0) x). simpl.
               rewrite <- H4. auto.
             + simpl. discriminate.
             + discriminate.
Qed.

Lemma remove_inv_max : forall d e, 
Inv_t d -> 
structure.max (remove e d) = process_max (domain (remove e d)).
Proof.
intros d e Hyp. unfold remove. 
case_eq(((e <? min d) || (e >? structure.max d))%bool);intro Hyp1.
- apply egality_max_process_max;assumption.
- case_eq(negb (fst (elt_list_remove (domain d) e)));intro Hyp2.
    * apply egality_max_process_max;assumption.
    * case_eq(elt_list_is_empty (snd (elt_list_remove (domain d) e)));intro Hyp3.
        + unfold empty;auto.
        + case_eq(Zeq_bool e (structure.max d));intro Hyp4.
             simpl;reflexivity.
             
             simpl. rewrite egality_max_process_max.
             unfold Inv_t in Hyp. decompose [and] Hyp.
             apply sameMax with (y:=min d) (e:=e) 
             (b:=fst (elt_list_remove (domain d) e)). 
                 assumption.
                 Zbool_lia. apply Morgan's_law in Hyp1. lia.  
                 destruct (elt_list_remove (domain d) e). auto.
                 assumption.
Qed.

(*****************************************)
(** ** (4) : remove_inv_size             *)
(*****************************************)

Lemma decreasingModif : forall e0 e z z0, (e <=? z0) = false ->
negb (fst (elt_list_remove (Cons z z0 e0) e)) = false ->
                  negb (fst (elt_list_remove e0 e)) = false.
Proof.
induction e0;intros a b e Hyp1. 
- simpl. rewrite Hyp1. simpl. discriminate.
- simpl. rewrite Hyp1. 
  case_eq(a <=? z0);intro Hyp3.
     * case_eq(Zeq_bool a z0);intro Hyp4.
         + case_eq(Zeq_bool z z0);intro Hyp5;
              simpl;intro;reflexivity. 
         + case_eq(a >=? z);intro Hyp5.
              case_eq(Zeq_bool a z);intro Hyp6;
                simpl;intro;reflexivity. 
              simpl;intro;discriminate H.
     * simpl;intro;assumption. 
Qed.

Lemma decreasingSize : forall d e, Inv_t d -> 
negb (fst (elt_list_remove (domain d) e)) = false ->
size d - 1 = process_size (snd (elt_list_remove (domain d) e)).
Proof.
intros d e Hyp1 Hyp2.
rewrite egality_size_process_size.
induction domain.
- simpl in Hyp2. discriminate Hyp2.
- simpl. 
  case_eq(e <=? z0);intro Hyp3.
     * case_eq(Zeq_bool e z0);intro Hyp4. 
        + case_eq(Zeq_bool z z0);intro Hyp5.
            destruct Hyp1. unfold snd. Zbool_lia. rewrite Hyp5.
            rewrite split_process_size_simpl. lia.
          
            unfold snd. rewrite split_process_size_simpl. 
            symmetry. rewrite split_process_size_simpl. lia.
         
         + case_eq(e >=? z);intros Hyp5.
             case_eq(Zeq_bool e z);intro Hyp6.
                unfold snd. rewrite split_process_size_simpl.
                symmetry. rewrite split_process_size_simpl. lia.
                  
                unfold snd. rewrite split_process_size_simpl. 
                symmetry. rewrite split_process_size.
                rewrite split_process_size_simpl. 
                rewrite Z.add_comm. 
                rewrite split_process_size. 
                rewrite split_process_size_simpl.
                assert(process_size Nil = 0) 
                by (unfold process_size;unfold ps;lia). 
                rewrite H. lia.
                  
             simpl in Hyp2. rewrite Hyp3 in Hyp2. 
             rewrite Hyp4 in Hyp2.
             
             rewrite Hyp5 in Hyp2. simpl in Hyp2. 
             discriminate Hyp2. 
                  
     * simpl. symmetry. rewrite split_process_size_simpl. 
       rewrite <- IHe0.
         + symmetry. rewrite split_process_size_simpl. lia.
         + apply decreasingModif with (z:=z) (z0:=z0);assumption.
- assumption.
Qed.

Lemma remove_inv_size : forall e d, Inv_t d -> 
size (remove e d) = process_size (domain (remove e d)).
Proof.
intros e d Hd. unfold remove. 
case_eq(((e <? min d) || (e >? structure.max d))%bool);intro Hyp1.
- apply egality_size_process_size. assumption.
- case_eq(negb (fst (elt_list_remove (domain d) e)));intro Hyp2.
    * apply egality_size_process_size. assumption.
    * case_eq(elt_list_is_empty (snd (elt_list_remove (domain d) e)));intro Hyp3.
        + unfold empty;auto.
        + case_eq(Zeq_bool e (structure.max d));intro Hyp4;simpl;
                apply decreasingSize;assumption.
Qed.

(****************)
(** ** Result   *)
(****************)

Theorem remove_inv : forall (d : t), 
Inv_t d -> forall (e : Z), Inv_t (remove e d).
Proof.
intros d Hyp e. unfold Inv_t. repeat split.
- apply remove_inv_domain ;assumption.
- apply remove_inv_min    ;assumption.
- apply remove_inv_max    ;assumption.
- apply remove_inv_size   ;assumption.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Theorem elt_list_remove_spec_1 : forall l x y, Inv_elt_list y l ->
elt_list_member x (snd (elt_list_remove l x)) = false.
Proof.
intros l x.
functional induction (elt_list_remove l x).
- intros y Hyp1. auto.
- intros y Hyp1. simpl.
  case_eq(q);intros.
    * auto.
    * simpl. rewrite H in Hyp1.
      inversion Hyp1. inversion H6.
      assert(x <=? z0 = true) as Hyp2 by
        (Zbool_lia_goal;Zbool_lia;lia).
      rewrite Hyp2. Zbool_lia_goal;Zbool_lia;lia.
- intros y Hyp1. simpl.
  assert(x <=? max - 1 = false) as Hyp2 by
  (Zbool_lia_goal;Zbool_lia;lia).
  rewrite Hyp2. 
  case_eq(q);intros.
     * auto.
     * simpl. rewrite H in Hyp1.
       inversion Hyp1. inversion H6.
       assert(x <=? z0 = true) as Hyp3 by
         (Zbool_lia_goal;Zbool_lia;lia).
       rewrite Hyp3. Zbool_lia_goal;Zbool_lia;lia.
- intros y Hyp1. simpl. inversion Hyp1.
  assert(x <=? max = true) as Hyp2 by
  (Zbool_lia_goal;Zbool_lia;lia).
  rewrite Hyp2.
  Zbool_lia_goal;Zbool_lia;lia.
- intros y Hyp1. simpl.
  assert(x <=? x - 1 = false) as Hyp2 by
  (Zbool_lia_goal;lia).
  rewrite Hyp2. rewrite e0.
  Zbool_lia_goal;lia.
- intros y Hyp1. simpl. 
  rewrite e0. rewrite e2. reflexivity.
- intros y Hyp1. simpl. 
  rewrite e0. apply IHp with (y:=max+2).
  inversion Hyp1. assumption.
Qed.

Lemma elt_list_member_not_in_1 : forall l x y b l', Inv_elt_list y l ->
elt_list_remove l x = (b,l') ->
negb b = true ->
elt_list_member x l = false.
Proof.
intros l x.
functional induction (elt_list_remove l x).
- intros. auto.
- intros. case_eq(q);intros;
  inversion H0;subst;inversion H1. 
- intros. inversion H0;subst;inversion H1. 
- intros. inversion H0;subst;inversion H1.
- intros. inversion H0;subst;inversion H1.
- intros. simpl. rewrite e0. assumption.
- intros. simpl. rewrite e0.
  apply IHp with (y:=max+2) (b:=b) 
                 (l':=(snd (elt_list_remove q x))).
  inversion H. assumption.
  destruct (elt_list_remove q x). auto.
  inversion H0. auto. assumption.
Qed.

Lemma Morgan's_law_true : forall x z z0,
(x <? z) || (x >? z0) = true 
<->
(x < z) \/ (x > z0).
Proof.
split;intros.
- apply  orb_true_iff in H. decompose [or] H;Zbool_lia;tauto. 
- apply  orb_true_iff. decompose [or] H;
     [left|right];Zbool_lia_goal;assumption. 
Qed.

Lemma inegality_process_max_gen : forall l y z z0 x,
Inv_elt_list y (Cons z z0 l) ->
x > process_max (Cons z z0 l) ->
x > z0.
Proof.
induction l.
- intros y z z0 x Hyp1 Hyp2.
  unfold process_max in Hyp2. simpl in Hyp2. assumption.
- intros y z1 z2 x Hyp1 Hyp2.
  rewrite decreasingProcess_max in Hyp2.
  assert(x > z0). inversion Hyp1.
  apply IHl with (y:=z2+2) (z:=z) (z0:=z0).
    * assumption.
    * assumption.
    * inversion Hyp1. inversion H6. lia.
    * discriminate.
Qed.

Lemma elt_list_member_not_in_2 : forall l x y a, Inv_elt_list y l ->
(x <? get_min l a) || (x >? process_max l) = true ->
elt_list_member x l = false.
Proof.
induction l.
- auto.
- intros. unfold elt_list_member. 
  apply Morgan's_law_true in H0. decompose [or] H0.
     + inversion H. simpl in H1. assert(x <=? z0 = true) as Hyp
         by (Zbool_lia_goal;Zbool_lia;lia).
       rewrite Hyp. Zbool_lia_goal;Zbool_lia;lia.
     + case_eq(l);intros.
         case_eq(x <=? z0);intros.
             unfold process_max in H1. rewrite H2 in H1. simpl in H1.
             Zbool_lia;lia.
             reflexivity.
         apply inegality_process_max_gen with (y:=y) in H1.
         assert(x <=? z0 = false) as Hyp
           by (Zbool_lia_goal;Zbool_lia;lia).
         rewrite Hyp. rewrite H2 in IHl.
         apply IHl with (y:=z0+2) (a:=min_int).
            * inversion H. rewrite <- H2. assumption.
            * decompose [or] H0.
              rewrite Morgan's_law_true. left.
              simpl. simpl in H3. inversion H. rewrite H2 in H10.
              inversion H10. lia.
              
              rewrite Morgan's_law_true. right. rewrite H2 in H3.
              rewrite decreasingProcess_max in H3.
              assumption.
              discriminate.
          * assumption.
Qed.

Lemma member_not_in : forall d v,
(v <? min d) || (v >? structure.max d) = true ->
Inv_t d ->
member v d = false.
Proof.
intros.
unfold Inv_t in H0. decompose [and] H0.
unfold member. apply elt_list_member_not_in_2 with (y:=min d) (a:=min_int).
assumption.
rewrite <- egality_min_get_min.
rewrite <- egality_max_process_max;assumption.
assumption.
Qed.

Theorem remove_spec_1 : forall d v,  Inv_t d -> 
member v (remove v d) = false.
Proof.
intros d v.
apply remove_ind.
- intros. apply member_not_in;assumption.
- intros. unfold member.
  apply elt_list_member_not_in_1 
     with (y:=min d) (b:=fst newl) (l':=snd newl).
  unfold Inv_t in H. decompose [and] H. assumption.
  destruct (elt_list_remove (domain d) v). auto.
  assumption.
- intros. unfold member. unfold empty. auto.
- intros. unfold member. simpl.
  unfold Inv_t in H;decompose [and] H.
  apply elt_list_remove_spec_1 with (y:=min d).
  assumption.
- intros. unfold member. simpl.
  unfold Inv_t in H;decompose [and] H.
  apply elt_list_remove_spec_1 with (y:=min d).
  assumption.
Qed.

(* There is another version in "Work extension" *)

Lemma elt_list_remove_elt_list_is_empty_true : forall l v b l' y, 
Inv_elt_list y l ->
elt_list_remove l v = (b,l') ->
elt_list_is_empty l' = true ->
l = Cons v v Nil \/ l = Nil.
Proof.
intros l v.
functional induction (elt_list_remove l v).
- intros. tauto.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. tauto.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. inversion H4.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. inversion H4.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. inversion H4.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. inversion H4.
- intros. apply elt_list_is_empty_spec in H1.
  Zbool_lia. inversion H0. subst. inversion H4.
Qed.

Theorem elt_list_remove_spec_2_3 : forall l v w y,
Inv_elt_list y l -> w <> v ->
elt_list_member w (snd (elt_list_remove l v)) = elt_list_member w l.
Proof.
intros l x.
functional induction (elt_list_remove l x);auto.
- intros w y Hyp1 Hyp2; simpl.
  Zbool_lia. subst.
  case_eq ( elt_list_member w q) ; intro Hypwq.
  * case_eq(w <=? max); intro Hyp4; auto.
        inversion Hyp1.
        pose proof (proj1 (elt_list_member_spec  w q _ H5) Hypwq) 
          as Hin.
        pose proof (borne_inf _ _ _ _ Hyp1 w Hin).
        Zbool_lia ;lia.
  * case_eq(w <=? max); intro Hyp4; auto.
    Zbool_lia ; symmetry; Zbool_lia_goal; lia.
- intros w y Hyp1 Hyp2. simpl. inversion Hyp1.
  case_eq(w <=? max - 1);intro Hyp5.
      * Zbool_lia_goal;Zbool_lia.
        assert(w <=? max = true) as Hyp4
          by (Zbool_lia_goal;lia).
        rewrite Hyp4 ; auto.
      * assert(w <=? max = false) as Hyp4
          by (Zbool_lia_goal;Zbool_lia;lia).
        rewrite Hyp4 ; auto.
- intros w y Hyp1 Hyp2.
  Zbool_lia. subst. simpl.
  case_eq(w <=? max);intro Hyp3; auto.
      case_eq (w >=? min) ; intro Hypp;
      Zbool_lia_goal; Zbool_lia;lia.
- intros w y Hyp1 Hyp2.
  Zbool_lia. simpl.
  case_eq(w <=? max);intro Hyp3.
      * case_eq(w <=? x - 1);intro Hyp5;
           Zbool_lia_goal;Zbool_lia. auto.
        assert(w >=? x + 1 = true) as Hyp4
          by (Zbool_lia_goal;Zbool_lia;lia).
        rewrite Hyp4. symmetry ; Zbool_lia_goal ; Zbool_lia;lia.
      * case_eq(w <=? x - 1);intro Hyp5;
           Zbool_lia_goal;Zbool_lia; auto.
        lia.
- intros w y Hyp1 Hyp2. simpl.
  case_eq (w <=? max); intro Hypp ; auto.
  inversion_clear Hyp1. rewrite IHp with (y:=max+2) ; auto.
Qed. 

Theorem remove_spec_2_3 : forall d v y, Inv_t d ->
y <> v -> member y (remove v d) = member y d.
Proof.
intros d v.
apply remove_ind ; auto.
- intros. unfold member.
  assert((domain d) = Cons v v Nil \/ (domain d) = Nil).
  apply elt_list_remove_elt_list_is_empty_true
  with (b:=fst newl) (l':=snd newl) (y:=min d).
     * unfold Inv_t in H. decompose [and] H. assumption.
     * destruct (elt_list_remove (domain d) v). auto.
     * assumption.
     * decompose [or] H1.
          + rewrite H2 . simpl.
            case_eq(y <=? v);intro Hyp2 ; auto.
              Zbool_lia; symmetry ; Zbool_lia_goal ; lia ; auto.
          + rewrite H2 ; auto.
- intros.  unfold member . simpl.
  unfold Inv_t in H;decompose [and] H.
  apply elt_list_remove_spec_2_3 with (y:=min d);assumption.
- intros. unfold member. simpl.
  unfold Inv_t in H;decompose [and] H.
  apply elt_list_remove_spec_2_3 with (y:=min d);assumption.
Qed.
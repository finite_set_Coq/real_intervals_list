Require Import R.Prelude.structure R.FBool.equal R.FBool.member R.FDomain.union R.Prelude.tactics R.CD.empty R.CD.values.
Require Export R.FBool.equal.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)

(** For elt_list     *)
Function elt_list_intersection (l1_l2 : elt_list*elt_list) {measure decreasing_length} := match l1_l2 with
|(Nil, _) | (_, Nil) => Nil
|(Cons min1 max1 q1, Cons min2 max2 q2) =>
   let min := Z.max min1 min2 in
   let max := Z.min max1 max2 in
   if Zgt_bool max2 max1 then 
        if Zle_bool min max then Cons min max (elt_list_intersection (q1,(Cons min2 max2 q2))) 
        else (elt_list_intersection (q1,(Cons min2 max2 q2))) 
                         else 
        if Zle_bool min max then Cons min max (elt_list_intersection ((Cons min1 max1 q1),q2))
        else 
        (elt_list_intersection ((Cons min1 max1 q1),q2))
end.
Proof.
intros; unfold decreasing_length; simpl; lia.
intros; unfold decreasing_length; simpl; lia.
intros; unfold decreasing_length; simpl; lia.
intros; unfold decreasing_length; simpl; lia.
Defined.

(* TESTS
Eval compute in elt_list_intersection(Cons 1 5 Nil,Cons 1 5 Nil).
Eval compute in elt_list_intersection(Cons 0 5 Nil,Cons 1 5 Nil).
Eval compute in elt_list_intersection(Cons 1 5 Nil,Cons 0 5 Nil).
Eval compute in elt_list_intersection(Cons 2 5 Nil,Cons 1 5 Nil).
Eval compute in elt_list_intersection(Cons 1 5 Nil,Cons 2 5 Nil).
*)

(** For domain: simplified version *)
Definition intersection (d1 d2 : t) :=
             match elt_list_intersection (domain d1,domain d2) with
              |Nil => empty
              |l => let s := process_size l in
                     mk_t l s (process_max l) (get_min l min_int)
             end.

(************************************)
(** * Preservation of the invariant *)
(************************************)
Theorem elt_list_intersection_inv_1 : forall double s s' l y1 y2 y3, 
Inv_elt_list y1 s ->
Inv_elt_list y2 s' ->
double = (s,s') ->
l = elt_list_intersection double ->
y3 = (if (Zle_bool y1 y2) then y2 else y1) ->
Inv_elt_list y3 l.
Proof.
intros double.
functional induction (elt_list_intersection double).
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. constructor.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. constructor.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor.
    + case_eq(y1 <=? y2);intro Heq;
          rewrite Heq in Hyp5;
          case_eq(min1 <=? min2);intro Heq2;
             Zminmax_lia_goal; Zbool_lia;lia.
    + case_eq(min1 <=? min2);intro Heq;
            Zminmax_lia_goal;Zminmax_lia;lia.
    + Zminmax_lia_goal.
      apply IHe with (s:=q1) (s':=(Cons min2 max2 q2)) (y1:=max1+2) (y2:=y2);
      try (assumption||reflexivity).
      assert(min2 <= max1). Zminmax_lia;Zminmax_lia_goal.
      assert(max1 + 2 <=? y2 = false) as Hyp6 
         by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp6. reflexivity.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1.
  apply IHe with (y1:=y1) (y2:=y2) (s:=q1) (s':=(Cons min2 max2 q2));
      (Inv_monotony (max1+2)||assumption||reflexivity).
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor.
    + case_eq(y1 <=? y2);intro Heq.
          - rewrite Heq in Hyp5.
            case_eq(min1 <=? min2);intro Heq2.
                assert(Z.max min1 min2 = min2) as Hyp6 
                  by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6;
                Zbool_lia;lia. 
            
                assert(Z.max min1 min2 = min1) as Hyp6 
                  by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6;
                Zbool_lia;lia. 
          - rewrite Heq in Hyp5.
            case_eq(min1 <=? min2);intro Heq2.
                assert(Z.max min1 min2 = min2) as Hyp6 
                      by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6;
                Zbool_lia;lia. 
                
                assert(Z.max min1 min2 = min1) as Hyp6 
                      by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6;
                Zbool_lia;lia. 
            
    + assert(Z.min max1 max2 = max2) as Hyp6 
         by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6.
      case_eq(min1 <=? min2);intro Heq.
            assert(Z.max min1 min2 = min2) as Hyp7 
               by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp7.
            lia.
            assert(Z.max min1 min2 = min1) as Hyp7 
               by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp7.
            Zbool_lia;lia.
    + assert(Z.min max1 max2 = max2) as Hyp6 
         by (Zbool_lia;Zminmax_lia_goal;lia);rewrite Hyp6.
      apply IHe with (s:=Cons min1 max1 q1) (s':=(q2)) (y1:=y1) (y2:=max2+2);
      try (assumption||reflexivity).
      assert(min1 <= max2). Zminmax_lia;Zminmax_lia_goal.
      assert(y1 <=? max2 + 2 = true) as Hyp7 
         by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp7. reflexivity.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp2.
  apply IHe with (y1:=y1) (y2:=y2) (s:=Cons min1 max1 q1) (s':=q2);
     (Inv_monotony (max2+2)||assumption||reflexivity).
Qed.

Lemma Inv_elt_list_simpl : forall l y,
Inv_elt_list y l ->
Inv_elt_list (get_min l min_int) l.
Proof.
induction l.
* intros y Hyp. simpl. constructor.
* intros y Hyp. simpl. inversion Hyp. constructor;(lia||assumption).
Qed.

Theorem elt_list_intersection_inv : forall double s s' l y1 y2 y3, 
Inv_elt_list y1 s ->
Inv_elt_list y2 s' ->
double = (s,s') ->
l = elt_list_intersection double ->
y3 <= Z.max y1 y2 ->
Inv_elt_list y3 l.
Proof.
intros double.
functional induction (elt_list_intersection double).
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. constructor.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. constructor.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor.
    + case_eq(y2 <=? y1);intro Heq;Zminmax_lia_goal.
          case_eq(min1 <=? min2);intro Heq2;
             Zminmax_lia_goal; Zminmax_lia; Zbool_lia;lia.
          case_eq(min1 <=? min2);intro Heq2;
             Zminmax_lia_goal; Zminmax_lia; Zbool_lia;lia.
    + Zbool_lia;lia.
    + Zminmax_lia_goal.
      apply IHe with (s:=q1) (s':=(Cons min2 max2 q2)) (y1:=max1+2) (y2:=y2);
      try (assumption||reflexivity).
      assert(min2 <= max1). Zminmax_lia;Zminmax_lia_goal.
      Zminmax_lia_goal;lia.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1.
  apply IHe with (y1:=y1) (y2:=y2) (s:=q1) (s':=(Cons min2 max2 q2));
      (Inv_monotony (max1+2)||assumption||reflexivity).
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor.
    + case_eq(y1 <=? y2);intro Heq;Zminmax_lia_goal.
          - case_eq(min1 <=? min2);intro Heq2;
              Zminmax_lia_goal;Zminmax_lia;lia.
          - case_eq(min1 <=? min2);intro Heq2;
              Zminmax_lia_goal;Zminmax_lia;lia.
    + Zbool_lia;lia.
    + Zminmax_lia_goal.
      apply IHe with (s:=Cons min1 max1 q1) (s':=(q2)) (y1:=y1) (y2:=max2+2);
         try (assumption||reflexivity).
      assert(min1 <= max2) by (Zminmax_lia;Zminmax_lia_goal).
      Zminmax_lia_goal;lia.
* intros s s' l y1 y2 y3 Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp2.
  apply IHe with (y1:=y1) (y2:=y2) (s:=Cons min1 max1 q1) (s':=q2);
     (Inv_monotony (max2+2)||assumption||reflexivity).
Qed.

Theorem intersection_inv : forall d d', 
Inv_t d -> Inv_t d' ->
Inv_t (intersection d d').
Proof.
intros d d' Hypd Hypd'. unfold intersection.
(** simplification of intersection 
case_eq( Zeq_bool (extractionOption Z (compare d d') min_int) 0 );
  intro Hyp1; try assumption.
  **)
case_eq (elt_list_intersection (domain d, domain d')); intros.
   + apply empty_inv.
   + (** simplification of intersection 
   case_eq(Zeq_bool (process_size (Cons z z0 e)) (size d)); 
        intro Hyp2; try assumption.
     case_eq(Zeq_bool (process_size (Cons z z0 e)) (size d'));
        intro Hyp3; try assumption.**)
        
        unfold Inv_t. simpl. repeat split; try reflexivity.
        assert(Inv_elt_list z (elt_list_intersection (domain d, domain d'))).
        
     case_eq(min d <=? min d');intro Hyp4.
       - assert(Inv_elt_list (min d) (elt_list_intersection (domain d, domain d'))).
         apply elt_list_intersection_inv with (double:=(domain d, domain d')) (s:=domain d) (s':=domain d') (y1:=min d) (y2:=min d'); try reflexivity.
          * unfold Inv_t in Hypd. decompose [and] Hypd. assumption.
          * unfold Inv_t in Hypd'. decompose [and] Hypd'. assumption.
          * Zminmax_lia_goal;assumption.
          * rewrite H in H0. rewrite H. inversion H0.
            constructor;(lia||assumption).
       - assert(Inv_elt_list (min d') (elt_list_intersection (domain d, domain d'))).
         apply elt_list_intersection_inv with (double:=(domain d, domain d')) (s:=domain d) (s':=domain d') (y1:=min d) (y2:=min d'); try reflexivity.
          * unfold Inv_t in Hypd. decompose [and] Hypd. assumption.
          * unfold Inv_t in Hypd'. decompose [and] Hypd'. assumption.
          * Zminmax_lia_goal;Zminmax_lia;lia.
          * rewrite H in H0. rewrite H. inversion H0.
            constructor;(lia||assumption).
       - rewrite H in H0. assumption.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
(*
  Parameter inter_1 : In x (inter s s') -> In x s.
  Parameter inter_2 : In x (inter s s') -> In x s'.
  Parameter inter_3 : In x s -> In x s' -> In x (inter s s').
*)

Lemma translation_In_elt_list_member : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
elt_list_member x (Cons a b q) = true <-> 
a <= x <= b \/ In x (elt_list_values q).
Proof.
split;intro Hyp2.
  * rewrite elt_list_member_spec with (y:=y) in Hyp2;
       try assumption.
    unfold elt_list_values in Hyp2. 
    apply elim_enum_and_conc with (z:=a) (z0:=b) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) q) in Hyp2;
               [assumption|reflexivity].
  * rewrite elt_list_member_spec with (y:=y);
       try assumption.
    unfold elt_list_values.
    apply intro_enum_and_conc with (z:=a) (z0:=b) (l:=(fix elt_list_values (l : elt_list) : list Z :=
        match l with
        | Nil => []
        | Cons min max q =>
            enum_and_conc (min, max, elt_list_values q)
        end) q);
           [reflexivity|assumption].
Qed.
     
Theorem elt_list_intersection_spec_1 : forall double s s' x y,
Inv_elt_list y s ->
Inv_elt_list y s' ->
double = (s,s') ->
elt_list_member x (elt_list_intersection double) = true -> 
elt_list_member x s = true.
Proof.
intros double.
functional induction (elt_list_intersection double).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp4.
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2. Zminmax_lia.
  rewrite translation_In_elt_list_member with (y:=y0) in Hyp4.
  decompose [or] Hyp4. 
          + rewrite translation_In_elt_list_member with (y:=y0);
               try assumption.
            left. split. Zminmax_lia_goal. tauto.
          + rewrite translation_In_elt_list_member with (y:=y0);
               try assumption.
            right. rewrite <- elt_list_member_spec with (y:=max1+2);
             try assumption.             
             apply IHe with (s':=Cons min2 max2 q2) (y:=y0);
             try
               (Inv_monotony (max1+2)||assumption||reflexivity). 
             rewrite elt_list_member_spec with (y:=y0).
             unfold elt_list_values. assumption.
             apply elt_list_intersection_inv_1 with (double:=(q1,Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2) (y1:=y0) (y2:=y0);
               try (Inv_monotony (max1+2)||assumption||reflexivity).
               assert(y0 <=? y0 = true) as Hyp6 
                  by (Zbool_lia_goal;lia);rewrite Hyp6;reflexivity.
         + constructor.
             - case_eq(min1 <=? min2);intro Hyp6;
                  Zminmax_lia_goal;lia.
             - case_eq(min1 <=? min2);intro Hyp6;
                  Zminmax_lia_goal;lia.
             - apply elt_list_intersection_inv_1 with (double:=(q1,Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2) (y1:=max1+2) (y2:=y0);
                     (assumption||reflexivity||assert( max1 + 2 <=? y0 = false) as Hyp6 by (Zbool_lia_goal;lia);rewrite Hyp6;reflexivity).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  rewrite translation_In_elt_list_member with(y:=y);
     try assumption.
  right. rewrite <- elt_list_member_spec with (y:=max1+2);
             try assumption.
  apply IHe with (s':=Cons min2 max2 q2) (y:=y);
        (Inv_monotony (max1+2)||assumption||reflexivity).
* intros s s' x y0 Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2. Zminmax_lia.
  rewrite translation_In_elt_list_member with (y:=y0) in Hyp4.
  decompose [or] Hyp4. 
          + rewrite translation_In_elt_list_member with (y:=y0);
               try assumption.
            left. split. Zminmax_lia_goal. lia.
          + apply IHe with (s':=q2) (y:=y0);
    try (Inv_monotony (max2+2)||assumption||reflexivity).
             rewrite elt_list_member_spec with (y:=y0).
             unfold elt_list_values. assumption.
             apply elt_list_intersection_inv_1 with (double:=(Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2) (y1:=y0) (y2:=y0);
               try (Inv_monotony (max2+2)||assumption||reflexivity).
               assert(y0 <=? y0 = true) as Hyp6 
                  by (Zbool_lia_goal;lia);rewrite Hyp6;reflexivity.
         + constructor.
             - case_eq(min1 <=? min2);intro Hyp6;
                  Zminmax_lia_goal;lia.
             - case_eq(min1 <=? min2);intro Hyp6;
                  Zminmax_lia_goal;lia.
             - apply elt_list_intersection_inv_1 with (double:=(Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2) (y1:=y0) (y2:=max2+2);
                     (assumption||reflexivity||assert( y0 <=? max2+2 = true) as Hyp6 by (Zbool_lia_goal;lia);rewrite Hyp6;reflexivity).
* intros s s' x y Hyp1 Hyp2 Hyp3 Hyp4. inversion Hyp3.
  rewrite <- H0 in Hyp1. inversion Hyp1.
  rewrite <- H1 in Hyp2. inversion Hyp2.
  apply IHe with (s':=q2) (y:=y);
    (Inv_monotony (max2+2)||assumption||reflexivity).
Qed.

Theorem intersection_spec_1 : forall s s' x, 
Inv_t s ->
Inv_t s' ->
member x (intersection s s') = true -> member x s = true.
Proof.
unfold member, Inv_t, intersection. intros.
decompose [and] H. decompose [and] H0. clear H. clear H0.
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s)) as hyp1 by  
  (apply inv_elt_list_monoton with (y:=min s);auto; apply Z.le_min_l).
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s')) as hyp2 by  
  (apply inv_elt_list_monoton with (y:=min s');auto; apply Z.le_min_r).
apply elt_list_intersection_spec_1 with 
 (double:=(domain s, domain s')) (y := Z.min (min s) (min s')) (s' := domain s'); auto.
case_eq (elt_list_intersection (domain s, domain s')); intros ; 
rewrite H in H1; simpl in H1; auto. 
Qed.

Lemma elt_list_member_inegality : forall a b x y q,
Inv_elt_list y (Cons a b q) ->
elt_list_member x q = true -> b+2 <= x.
Proof.
intros a b x y q Hyp1 Hyp2. inversion Hyp1. 
rewrite elt_list_member_spec with (y:=b+2) in Hyp2; try assumption.
apply in_elt_list_values_le_2 with (l:=q);assumption.
Qed.

Theorem elt_list_intersection_spec_3 : forall double s s' x z,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
elt_list_member x s = true ->
elt_list_member x s' = true ->
elt_list_member x (elt_list_intersection double) = true.
Proof.
intros double.
functional induction (elt_list_intersection double).
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp4. inversion Hyp4.
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H1 in Hyp5. inversion Hyp5. 
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp4. rewrite <- H1 in Hyp5.
  rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2.

  Zminmax_lia_goal;Zminmax_lia.
  rewrite <- decompose_elt_list_member with (y:=z).
  rewrite <- decompose_elt_list_member with (y:=z) in Hyp4;
     try assumption.
  decompose [or] Hyp4.
    + case_eq(min1 <=? min2);intro.
         - Zminmax_lia_goal. left. 
           rewrite <- decompose_elt_list_member with (y:=z) in Hyp5;
              try assumption.
           decompose [or] Hyp5.
             { lia. }
             { apply elt_list_member_inegality
                with (a:=min2) (b:=max2) (y:=z) in H17;try assumption. lia. }
         - Zminmax_lia_goal. left. lia.
    + right.
      apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (z:=z);
               try (Inv_monotony (max1+2)||assumption||reflexivity).
    + constructor. apply Z.max_le_iff; tauto. 
      assumption. 
      apply elt_list_intersection_inv with (double:=(q1, Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2) (y1:=max1+2) (y2:=z);
             (assumption||reflexivity||Zminmax_lia_goal;lia).
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp4. rewrite <- H1 in Hyp5.
  rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2.
  
  apply IHe with (s:=q1) (s':=Cons min2 max2 q2) (z:=z);
               try (Inv_monotony (max1+2)||assumption||reflexivity).
               
  case_eq(min1 <=? min2);intro Hyp6;Zminmax_lia.
     + rewrite elt_list_member_spec with (y:=z) in Hyp4;
          try assumption.
       unfold elt_list_values in Hyp4. 
       apply elim_enum_and_conc with (z:=min1) (z0:=max1) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) (q1)) in Hyp4; try reflexivity.
       decompose [or] Hyp4.
           - rewrite elt_list_member_spec with (y:=z) in Hyp5;
                try assumption.
             unfold elt_list_values in Hyp5.
             apply elim_enum_and_conc with (z:=min2) (z0:=max2) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) (q2)) in Hyp5; try reflexivity.
             decompose [or] Hyp5.
                lia.
                assert(max2 <= x) by 
                 (apply in_elt_list_values_le with (r:=q2) (y:=z) (z:=min2);
                   try assumption);lia.
           - rewrite elt_list_member_spec with (y:=max1+2);assumption.
     + lia.
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp4. rewrite <- H1 in Hyp5.
  rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2.

  Zminmax_lia_goal;Zminmax_lia.
  rewrite <- decompose_elt_list_member with (y:=z).
  rewrite <- decompose_elt_list_member with (y:=z) in Hyp5;
     try assumption.
  decompose [or] Hyp5.
    + case_eq(min1 <=? min2);intro.
         - Zminmax_lia_goal. left. lia.
         - Zminmax_lia_goal. left.
           rewrite <- decompose_elt_list_member with (y:=z) in Hyp4;
              try assumption.
           decompose [or] Hyp4.
             { lia. }
             { apply elt_list_member_inegality
                with (a:=min1) (b:=max1) (y:=z) in H17. lia. Zbool_constructor. }
    + right.
      apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (z:=z);
               try (Inv_monotony (max2+2)||assumption||reflexivity).
    + constructor. 
      apply Z.max_le_iff; tauto.
      assumption. 
      apply elt_list_intersection_inv with (double:=(Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2) (y1:=z) (y2:=max2+2);
             (assumption||reflexivity||Zminmax_lia_goal;lia).
* intros s s' x z Hyp1 Hyp2 Hyp3 Hyp4 Hyp5. inversion Hyp3.
  rewrite <- H0 in Hyp4. rewrite <- H1 in Hyp5.
  rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2.
  
  apply IHe with (s:=Cons min1 max1 q1) (s':=q2) (z:=z);
               try (Inv_monotony (max2+2)||assumption||reflexivity).
               
  case_eq(min1 <=? min2);intro Hyp6;Zminmax_lia.
     + lia.
     + rewrite elt_list_member_spec with (y:=z) in Hyp5;
          try assumption.
       unfold elt_list_values in Hyp5. 
       apply elim_enum_and_conc with (z:=min2) (z0:=max2) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) (q2)) in Hyp5; try reflexivity.
       decompose [or] Hyp5.
           - rewrite elt_list_member_spec with (y:=z) in Hyp4;
                try assumption.
             unfold elt_list_values in Hyp4.
             apply elim_enum_and_conc with (z:=min1) (z0:=max1) (l:=(fix elt_list_values (l : elt_list) : 
             list Z :=
               match l with
               | Nil => []
               | Cons min max q =>
                   enum_and_conc (min, max, elt_list_values q)
               end) (q1)) in Hyp4; try reflexivity.
             decompose [or] Hyp4.
                lia.
                assert(max1 <= x) by 
                 (apply in_elt_list_values_le with (r:=q1) (y:=z) (z:=min1);
                   try assumption);lia.
           - rewrite elt_list_member_spec with (y:=max2+2);assumption.
Qed.

Theorem intersection_spec_3 : forall s s' x,
Inv_t s -> Inv_t s' ->
member x s = true ->
member x s' = true ->
member x (intersection s s') = true.
Proof.
unfold member, Inv_t, intersection. intros.
decompose [and] H. decompose [and] H0. clear H. clear H0.
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s)) as hyp1 by  
  (apply inv_elt_list_monoton with (y:=min s);auto; apply Z.le_min_l).
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s')) as hyp2 by  
  (apply inv_elt_list_monoton with (y:=min s');auto; apply Z.le_min_r).
pose proof (elt_list_intersection_spec_3 (domain s, domain s') 
    _ _ _ _  hyp1 hyp2 (eq_refl(domain s, domain s')) H1 H2) as HH.
case_eq (elt_list_intersection (domain s, domain s')); intros ; rewrite H in HH.
 discriminate.  simpl; auto. 
Qed.

Lemma elt_list_intersection_Cons : forall q2 q1 min2 max1, 
Inv_elt_list (max1 + 2) q1 -> 
min2 <= max1 ->
elt_list_intersection (Cons min2 max1 q2, q1) = 
elt_list_intersection (q2, q1).
Proof.
destruct q1 ; intros ; rewrite elt_list_intersection_equation.
+ case q2 ; intros ; rewrite elt_list_intersection_equation ; auto.
+ inversion_clear H.
assert(z0 >? max1 = true) as Hyp1
   by (Zbool_lia_goal;Zbool_lia; lia);rewrite Hyp1.
replace (Z.max min2 z) with z. 
replace (Z.min max1 z0) with max1.
assert (z <=? max1 = false) as Hyp2
   by (Zbool_lia_goal;Zbool_lia; lia);rewrite Hyp2.
auto.
rewrite  Z.min_l ; [auto |  lia].
rewrite  Z.max_r ; [auto |  lia].
Qed.

Lemma elt_list_intersection_Cons_rev : forall q2 q1 min1 max1, 
Inv_elt_list (max1 + 2) q1 -> 
Inv_elt_list (max1 + 2) q2 -> 
min1 <= max1 ->
elt_list_intersection (q2, Cons min1 max1 q1) = 
elt_list_intersection (q2, q1).
Proof.
destruct q2 ; intros ; rewrite elt_list_intersection_equation.
+ case q1 ; intros ; rewrite elt_list_intersection_equation; auto.
+ rewrite elt_list_intersection_equation.
inversion H0.
assert(max1 >? z0 = false) as Hyp1
   by (Zbool_lia_goal;Zbool_lia; lia);rewrite Hyp1.
replace (Z.max z min1) with z.
replace (Z.min z0 max1) with max1.
assert (z <=? max1 = false) as Hyp2
   by (Zbool_lia_goal;Zbool_lia; lia);rewrite Hyp2.
auto.
rewrite  Z.min_r ; [auto |  lia].
rewrite  Z.max_l ; [auto |  lia].
Qed.

Lemma elt_list_intersection_symmetry : forall double s s' z,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
elt_list_intersection (s',s) = elt_list_intersection double.
Proof.
intros l1_l2 .
apply elt_list_intersection_ind; simpl; intros.
* inversion H1. rewrite elt_list_intersection_equation.
  case_eq(s');intros;subst;reflexivity.
* inversion H1. rewrite elt_list_intersection_equation. auto.
* inversion H2. subst. rewrite elt_list_intersection_equation.
  assert(max1 >? max2 = false) as Hyp1
    by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp1.
  assert(Z.max min2 min1 = Z.max min1 min2) as Hyp2
    by (apply Z.max_comm);rewrite Hyp2.
  assert(Z.min max2 max1 = Z.min max1 max2) as Hyp3
    by (apply Z.min_comm);rewrite Hyp3. rewrite e1.
  Zminmax_lia_goal.
  rewrite H with (s':=Cons min2 max2 q2) (s:=q1) (z:=z);
     try (reflexivity||inversion H0;Inv_monotony (max1+2)||assumption).
* inversion H2. subst. rewrite elt_list_intersection_equation.
  assert(max1 >? max2 = false) as Hyp1
    by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp1.
  assert(Z.max min2 min1 = Z.max min1 min2) as Hyp2
    by (apply Z.max_comm);rewrite Hyp2.
  assert(Z.min max2 max1 = Z.min max1 max2) as Hyp3
    by (apply Z.min_comm);rewrite Hyp3. rewrite e1.
  rewrite H with (s:=q1) (s':=Cons min2 max2 q2) (z:=z);
     try (reflexivity||inversion H0;Inv_monotony (max1+2)||assumption).
* inversion H2. subst. clear H2. rewrite elt_list_intersection_equation.
  assert(Z.max min2 min1 = Z.max min1 min2) as Hyp3
   by (apply Z.max_comm);rewrite Hyp3.
  assert(Z.min max2 max1 = Z.min max1 max2) as Hyp4
   by (apply Z.min_comm);rewrite Hyp4.
  case_eq(max1 =? max2);intro Hyp1.
   + apply Z.eqb_eq in Hyp1.
     assert(max1 >? max2 = false) as Hyp2
      by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp2.
     rewrite e1.
     inversion H0. inversion H1. subst.
      rewrite elt_list_intersection_Cons ; auto.
     rewrite <- H with (s:=Cons min1 max2 q1) (z:=z)
                       (s':= q2); auto.
     rewrite elt_list_intersection_Cons_rev; auto.
     apply inv_elt_list_monoton with (y:= max2 + 2) ; auto; try lia.
   + apply Z.eqb_neq in Hyp1. rewrite e1.
     assert(max1 >? max2 = true) as Hyp2
      by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp2.
     rewrite <- H with (s':=q2) (s:=Cons min1 max1 q1) (z:=z);
      try (reflexivity||inversion H0;Inv_monotony (max2+2)||assumption).
      inversion H1. Inv_monotony (max2+2).
* inversion H2. subst. rewrite elt_list_intersection_equation.
  assert(Z.max min2 min1 = Z.max min1 min2) as Hyp3
   by (apply Z.max_comm);rewrite Hyp3.
  assert(Z.min max2 max1 = Z.min max1 max2) as Hyp4
   by (apply Z.min_comm);rewrite Hyp4.
  case_eq(max1 =? max2);intro Hyp1.
   + apply Z.eqb_eq in Hyp1.
     assert(max1 >? max2 = false) as Hyp2
      by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp2.
     rewrite e1. 
     Zbool_lia. 
     inversion H0. inversion H1. subst.
     rewrite elt_list_intersection_Cons ; auto.
     rewrite <- H with (s:=Cons min1 max2 q1) (z:=z)
                       (s':= q2); auto.
     rewrite elt_list_intersection_Cons_rev; auto.
     apply inv_elt_list_monoton with (y:= max2 + 2) ; auto; try lia.
   + apply Z.eqb_neq in Hyp1. rewrite e1.
     assert(max1 >? max2 = true) as Hyp2
      by (Zbool_lia_goal;Zbool_lia;lia);rewrite Hyp2.
     rewrite <- H with (s':=q2) (s:=Cons min1 max1 q1) (z:=z);
      try (reflexivity||inversion H0;Inv_monotony (max2+2)||assumption).
      inversion H1. Inv_monotony (max2+2).
Qed.

Theorem elt_list_intersection_spec_2 : forall double s s' x z,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
elt_list_member x (elt_list_intersection double) = true -> 
elt_list_member x s' = true.
Proof.
intros. subst.
rewrite <- elt_list_intersection_symmetry with 
   (double := (s,s')) (s:=s) (s':=s') (z:=z) in H2 ; auto.
apply elt_list_intersection_spec_1 with (s:=s') (s':=s) (y:=z) (double :=(s',s)) ; auto.
Qed.

Theorem intersection_spec_2 : forall  s s' x ,
Inv_t s ->
Inv_t s' ->
member x (intersection s s') = true -> member x s' = true.
Proof.
unfold member, Inv_t, intersection. intros.
decompose [and] H. decompose [and] H0. clear H. clear H0.
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s)) as hyp1 by  
  (apply inv_elt_list_monoton with (y:=min s);auto; apply Z.le_min_l).
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s')) as hyp2 by  
  (apply inv_elt_list_monoton with (y:=min s');auto; apply Z.le_min_r).
apply elt_list_intersection_spec_2 with 
 (double:=(domain s, domain s')) (z := Z.min (min s) (min s')) (s := domain s); auto.
case_eq (elt_list_intersection (domain s, domain s')); intros ; 
rewrite H in H1; simpl in H1; auto. 
Qed.
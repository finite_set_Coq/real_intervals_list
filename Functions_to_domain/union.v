Require Import R.Prelude.structure R.FBool.member R.Prelude.tactics R.CD.empty R.CD.values.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)

(** ** For elt_list    *)
Function elt_list_union (l1_l2 : elt_list*elt_list) {measure
decreasing_length} : elt_list := match l1_l2 with 
|(Nil, _) => snd(l1_l2) |(_,Nil) => fst(l1_l2)
|((Cons min1 max1 q1) as l1, (Cons min2 max2 q2) as l2) =>
 if Zlt_bool max1 (min2-1)
 then Cons min1 max1 (elt_list_union (q1,l2))
 else if Zlt_bool max2 (min1-1)
      then Cons min2 max2 (elt_list_union (l1,q2))
      else if Zgt_bool max1 max2
           then elt_list_union ((Cons (Z.min min1 min2) max1 q1),q2)
           else
             if Zle_bool min1 min2
             then elt_list_union ((Cons min1 max2 q2),q1) (**)
             else elt_list_union ((Cons min2 max2 q2),q1)
end.
Proof.
  intros; unfold decreasing_length; simpl; lia.
  intros; unfold decreasing_length; simpl; lia.
  intros; unfold decreasing_length; simpl; lia.
  intros; unfold decreasing_length; simpl; lia.
  intros; unfold decreasing_length; simpl; lia.
Defined.

(** ** For domain     *)
Definition union (d1 d2 : t) := match elt_list_union (domain d1,domain d2) with
|Nil => empty
|l => mk_t l (process_size l) (process_max l) (get_min l structure.min_int)
end.

Functional Scheme union_ind := Induction for union Sort Prop.

(************************************)
(** * Preservation of the invariant *)
(************************************)
Theorem elt_list_union_inv : forall double s s' l z, 
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
l = elt_list_union double ->
Inv_elt_list z l.
Proof.
intro double.
functional induction (elt_list_union double).
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. simpl. inversion Hyp3. assumption.
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4.
  rewrite Hyp4. simpl.  inversion Hyp3. assumption. 
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor. assumption. Zbool_lia;lia.
  apply IHe with (s:=q1) (s':=Cons min2 max2 q2);
    (reflexivity||assumption||Zbool_constructor). 
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  constructor. assumption. Zbool_lia;lia.
  apply IHe with (s:=Cons min1 max1 q1) (s':=q2);
    (reflexivity||assumption||Zbool_constructor).
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  simpl.
  apply IHe with (s:=Cons (Z.min min1 min2) max1 q1) (s':=q2).
    - case_eq(min1 <=? min2);intro Hyp5.              
        constructor;Zminmax_lia_goal;assumption. 
        Zminmax_lia_goal;Zbool_constructor.
    - Inv_monotony (max2+2).
    - reflexivity.
    - reflexivity.
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2. subst.
  simpl.
  apply IHe with (s:=Cons min1 max2 q2) (s':=q1);
    (reflexivity||Zbool_constructor||Inv_monotony (max1+2)).
* intros s s' l0 z Hyp1 Hyp2 Hyp3 Hyp4. 
  rewrite Hyp4. inversion Hyp3. rewrite <- H0 in Hyp1. 
  rewrite <- H1 in Hyp2. inversion Hyp1. inversion Hyp2.
  simpl.
  apply IHe with (s:=Cons min2 max2 q2) (s':=q1);
    (reflexivity||Zbool_constructor||Inv_monotony (max1+2)).
Qed.

Lemma union_inv_domain_court :
forall d1 d2, Inv_t d1 -> Inv_t d2 ->  
Inv_elt_list (Z.min (min d1) (min d2)) (domain (union d1 d2)).
Proof.
intros d1 d2 Hypd1 Hypd2.
unfold Inv_t in Hypd1;decompose [and] Hypd1. clear Hypd1.
unfold Inv_t in Hypd2;decompose [and] Hypd2. clear Hypd2.
unfold union. simpl.
case_eq (elt_list_union (domain d1, domain d2)); intros ; simpl.
  * constructor.
  * apply elt_list_union_inv 
       with (double:=(domain d1,domain d2)) (s:=domain d1) (s':=domain d2).
         + simpl;apply inv_elt_list_monoton with (y:=min d1); 
              [assumption | apply Z.le_min_l ].
         + simpl;apply inv_elt_list_monoton with (y:=min d2); 
              [assumption | apply Z.le_min_r ].
         + reflexivity.
         + auto.
Qed.

Lemma get_min_union : forall l1_l2 l' y, 
elt_list_union l1_l2 = l' -> 
fst l1_l2 <> Nil -> snd l1_l2 <> Nil -> 
Inv_elt_list y (fst l1_l2) -> 
Inv_elt_list y (snd l1_l2) -> 
get_min (elt_list_union l1_l2) min_int = 
Z.min (get_min (fst l1_l2) min_int) (get_min (snd l1_l2) min_int).
Proof.
intros l1_l2 .
apply elt_list_union_ind; simpl; intros.
- contradiction.
- contradiction.
- Zbool_lia. inversion H3. inversion H4. 
symmetry. apply Z.min_l. lia.
- Zbool_lia. inversion H3. inversion H4. symmetry. apply Z.min_r. lia.
- inversion H3 ; inversion H4 ; Zbool_lia.
  case_eq q2; intros.
 + rewrite elt_list_union_equation. auto.
 + rewrite <- H19. erewrite H  with (y:=y); auto. 
   rewrite H19 in * ; simpl ; inversion H18. 
   elim (Zmin_spec min1 min2) ; intro Hyp ; destruct Hyp ; rewrite H28; apply Z.min_l; lia. 
   intro pb ; inversion pb.
   intro pb ; inversion pb; rewrite pb in H19; inversion H19.
   constructor; auto.
   elim (Zmin_spec min1 min2) ; intro Hyp ; destruct Hyp ; rewrite H21;lia. 
   elim (Zmin_spec min1 min2) ; intro Hyp ; destruct Hyp; 
   rewrite H21 ; lia. Inv_monotony (max2+2).
- inversion H3 ; inversion H4 ; Zbool_lia.
  rewrite Z.min_l ; auto.
  case_eq q1; intros.
 + rewrite elt_list_union_equation; auto.
 + rewrite <- H19. erewrite H  with (y:=y); auto. 
   rewrite H19 in * ; simpl ; inversion H11.  apply Z.min_l ; lia.
   intro pb ; inversion pb.
   intro pb; rewrite pb in H19; inversion H19.
   constructor; auto. lia. Inv_monotony (max1+2).
- inversion H3 ; inversion H4 ; Zbool_lia.
  rewrite Z.min_r ; try lia.
  case_eq q1; intros.
 + rewrite elt_list_union_equation; auto.
 + rewrite <- H19. erewrite H  with (y:=y); auto. 
   rewrite H19 in * ; simpl ; inversion H11.  apply Z.min_l ; lia.
   intro pb ; rewrite pb in H19; inversion H19. Inv_monotony (max1+2).
Qed.

Theorem union_inv : 
forall d1 d2, Inv_t d1 -> Inv_t d2 ->  
Inv_t (union d1 d2).
Proof.
unfold union. intros.
case_eq (elt_list_union (domain d1, domain d2)); intros.
- apply empty_inv.
- unfold Inv_t in *; repeat split ; simpl. decompose [and] H . decompose [and] H0.
  case_eq (domain d1) ; intros.
  + rewrite H9 in *. 
    rewrite elt_list_union_equation in H1.
    inversion H1. rewrite H12 in *. simpl in H8 ; rewrite H8 in H5. assumption.
  + case_eq (domain d2) ; intros.
    * rewrite H11 in *. 
    rewrite elt_list_union_equation in H1.
    rewrite H9 in *. simpl in H1. inversion H1. simpl in H4 . rewrite H4 in H2. subst. assumption.
    * assert (z = Z.min (get_min (domain d1) min_int) (get_min (domain d2) min_int)) as hypz.
  replace z with (get_min (elt_list_union
       (domain d1, domain d2)) min_int) by (rewrite H1; auto).
  apply get_min_union with (y:= Z.min (min d1) (min d2)) (l':=Cons z z0 e) ; auto.
  simpl ; intro pb ; rewrite pb in H9 ; inversion H9.
  simpl ; intro pb ; rewrite pb in H11 ; inversion H11.
  apply inv_elt_list_monoton with (y:=min d1) ; auto.
  apply Z.le_min_l.
  apply inv_elt_list_monoton with (y:=min d2) ; auto.
  apply Z.le_min_r.
  apply elt_list_union_inv with (l:= (Cons z z0 e)) 
  (double := (domain d1, domain d2)) (s:=domain d1) (s':=domain d2); simpl; auto.
  rewrite hypz.
  apply inv_elt_list_monoton with (y:=min d1) ; auto. 
  rewrite <- H4. rewrite <- H8.
  apply Z.le_min_l.
  rewrite hypz.
  apply inv_elt_list_monoton with (y:=min d2) ; auto. 
  rewrite <- H4. rewrite <- H8.
  apply Z.le_min_r.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Lemma great_decompose : forall x z min1 max1 q1 min2 max2 q2,
max1 > max2 ->
Inv_elt_list z (Cons min1 max1 q1) ->
Inv_elt_list z (Cons min2 max2 q2) ->
min2 <= x <= max2 \/ max2 < x <= max1 \/ elt_list_member x q1 = true \/ elt_list_member x q2 = true
<->                 
elt_list_member x (Cons min2 max1 q1) = true \/
       elt_list_member x q2 = true.
Proof.
intros x z min1 max1 q1 min2 max2 q2 Hyp1 Hyp2 Hyp3.
inversion Hyp2. inversion Hyp3. subst.
split;intro.
* decompose [or] H.
    + left. apply decompose_elt_list_member with (y:=z). Zbool_constructor. 
      left. lia.
    + left. apply decompose_elt_list_member with (y:=z). Zbool_constructor. 
      left. lia. 
    + left. apply decompose_elt_list_member with (y:=z). Zbool_constructor. 
      tauto.
    + tauto.
* decompose [or] H.
    + apply decompose_elt_list_member with (y:=z) in H0.
      assert(min2 <= x <= max2 \/ max2 < x <= max1 <-> min2 <= x <= max1)
        as Hyp4 by (Zbool_lia;lia). 
        tauto. Zbool_constructor.
    + tauto.
Qed.

Theorem elt_list_union_spec_1 : forall double s s' x z l,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
l = elt_list_union double ->
elt_list_member x l = true ->
elt_list_member x s = true \/ elt_list_member x s' = true.
Proof.
intro double.
functional induction (elt_list_union double) ; 
intros s s' x z l Hyp1 Hyp2 Hyp3 Hyp4 Hyp5;  inversion Hyp3.
* simpl in Hyp4.  right. subst. assumption.
* simpl in Hyp4. left. subst. assumption.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  assert(Inv_elt_list z (elt_list_union (q1, Cons min2 max2 q2)))
    as Hyp6 by
      (apply elt_list_union_inv with (double:=(q1,Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2);[Inv_monotony (max1+2)|assumption|reflexivity|reflexivity]). 

 rewrite <- decompose_elt_list_member with (y:=z).
    - apply or_assoc. 
      rewrite <- decompose_elt_list_member with (y:=z) in Hyp5.
      + decompose [or] Hyp5.
         tauto.
         right. 
         apply IHe with (z:=z) (l:=elt_list_union (q1, Cons min2 max2 q2));
           (Inv_monotony (max1+2)||assumption||reflexivity).
      + constructor. lia. lia.
        apply elt_list_union_inv with (double:=(q1,Cons min2 max2 q2)) 
          (s:=q1) (s':=Cons min2 max2 q2);
             (reflexivity||assumption||Zbool_constructor).
   - assumption.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  assert(Inv_elt_list z (elt_list_union (Cons min1 max1 q1,q2)))
    as Hyp6 by
      (apply elt_list_union_inv with (double:=(Cons min1 max1 q1,q2)) (s':=q2) (s:=Cons min1 max1 q1);[assumption|Inv_monotony (max2+2)|reflexivity|reflexivity]).
  rewrite <- decompose_elt_list_member with (y:=z) (min1:=min2) (max1:=max2) (q1:=q2).
    - rewrite <- decompose_elt_list_member with (y:=z) in Hyp5.
      + decompose [or] Hyp5.
         tauto.
         apply or_comm. apply or_assoc. right. apply or_comm.
         apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1,q2));
           (Inv_monotony (max2+2)||assumption||reflexivity).
      + constructor. lia. lia.
        apply elt_list_union_inv with (double:=(Cons min1 max1 q1,q2)) 
          (s':=q2) (s:=Cons min1 max1 q1);
             (reflexivity||assumption||Zbool_constructor).
   - assumption.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  assert(Z.min min1 min2 = min1 \/ Z.min min1 min2 = min2) as Hyp6 
    by (apply Zmin_irreducible with (n:=min1) (m:=min2)).
  apply or_comm.
  rewrite <- decompose_elt_list_member with (y:=z).
  apply or_assoc.
  assert(Inv_elt_list z (elt_list_union (Cons min1 max1 q1, q2))).
  apply elt_list_union_inv with (double:=(Cons min1 max1 q1,q2)) 
          (s':=q2) (s:=Cons min1 max1 q1) ;
             (reflexivity||assumption||Inv_monotony (max2+2)). 
    assert(Inv_elt_list z (elt_list_union (Cons min2 max1 q1, q2))).   
    apply elt_list_union_inv 
    with (double:=(Cons min2 max1 q1, q2)) (s:=Cons min2 max1 q1) (s':=q2);
    [Zbool_constructor|Inv_monotony (max2+2)|reflexivity|reflexivity].
    decompose [or] Hyp6.
      - apply elt_list_member_spec with (y:=z) in Hyp5.
        rewrite H1 in Hyp5. rewrite H1 in IHe.
        unfold elt_list_values in Hyp5. right. apply or_comm.
        apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1, q2)).
           + assumption.
           + Inv_monotony (max2+2).
           + reflexivity.
           + reflexivity.
           + apply elt_list_member_spec with (y:=z);
             assumption.
           + rewrite H1. assumption.
      - rewrite H1 in Hyp5. rewrite H1 in IHe.
        inversion Hyp1. inversion Hyp2. subst.
        apply IHe with (s:=Cons min2 max1 q1) (s':=q2) (z:=z) in Hyp5.
           + rewrite <- great_decompose with (x:=x) (z:=z) (min1:=min1) 
        (max1:=max1) (q1:=q1) (min2:=min2) (max2:=max2) (q2:=q2) in Hyp5.
        decompose [or] Hyp5.
         tauto.
         repeat right. apply decompose_elt_list_member with (y:=z).
                assumption. left. Zbool_lia. lia.
                repeat right. apply decompose_elt_list_member with (y:=z).
                assumption. tauto.
                tauto.                
      Zbool_lia;lia. assumption. assumption.
      + Zbool_constructor.
      + Inv_monotony (max2+2).
      + reflexivity.
      + reflexivity.
      -  assumption.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.  
  rewrite <- decompose_elt_list_member with (y:=z). apply or_assoc.
  assert(Inv_elt_list z (elt_list_union (Cons min1 max2 q2, q1)))
  as Hyp6 by
      (apply elt_list_union_inv with (double:=(Cons min1 max2 q2, q1)) (s':=q1) (s:=Cons min1 max2 q2);[Zbool_constructor|Inv_monotony (max1+2)|reflexivity|reflexivity]).
  apply IHe with (s:=Cons min1 max2 q2) (s':=q1) (z:=z) in Hyp5.
  Zbool_lia.
      + case_eq(Zeq_bool max1 max2);intro Heq.
          - decompose [or] Hyp5.
              apply decompose_elt_list_member with (y:=z) in H.
              decompose [or] H.
              Zbool_lia. rewrite <- Heq in H0. tauto.
              repeat right. apply decompose_elt_list_member with (y:=z).
                assumption. tauto.
                Zbool_constructor. tauto.      
          - rewrite <- great_decompose with (x:=x) (z:=z) 
          (min2:=min1) (max2:=max1) (q2:=q1) 
          (min1:=min2) (max1:=max2) (q1:=q2) in Hyp5.
        decompose [or] Hyp5.
         tauto.
         right. right. apply decompose_elt_list_member with (y:=z).
                assumption. left. Zbool_lia;lia.
                repeat right. apply decompose_elt_list_member with (y:=z).
                assumption. tauto.
                tauto.               
      Zbool_lia;lia. assumption. assumption.
      + Zbool_constructor.
      + Inv_monotony (max1+2).
      + reflexivity.
      + reflexivity.
      + assumption.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst. 
  rewrite <- decompose_elt_list_member with (y:=z). apply or_assoc.
  apply IHe with (s:=Cons min2 max2 q2) (s':=q1) (z:=z) in Hyp5.
  tauto. assumption. Inv_monotony (max1+2).
  reflexivity. reflexivity. assumption.
Qed.

Theorem union_spec_1 : forall s s' x, 
Inv_t s ->
Inv_t s' ->
member x (union s s') = true -> member x s = true \/ member x s' = true.
Proof.
intros.
unfold member. unfold Inv_t in *.
decompose [and] H. decompose [and] H0. clear H. clear H0.
case_eq(min s <=? min s');intro Hyp1.
  * apply elt_list_union_spec_1 with (double:=(domain s, domain s')) (z:=min s) (l:=elt_list_union (domain s, domain s')).
       + assumption.
       + Zbool_lia. Inv_monotony (min s').
       + reflexivity.
       + reflexivity.
       + unfold member in H1. unfold union in H1. 
         case_eq (elt_list_union (domain s, domain s')) ; intros. 
           rewrite H in H1. inversion H1.
           rewrite H in H1. simpl in H1. auto.
  * apply elt_list_union_spec_1 with (double:=(domain s, domain s')) (z:=min s') (l:=elt_list_union (domain s, domain s')).
       + Zbool_lia. Inv_monotony (min s).
       + assumption.
       + reflexivity.
       + reflexivity.
       + unfold member in H1. unfold union in H1. 
         case_eq (elt_list_union (domain s, domain s')) ; intros. 
           rewrite H in H1. inversion H1.
           rewrite H in H1. simpl in H1. auto.
Qed.

Theorem elt_list_union_spec_2_3 : forall double s s' x z l,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
l = elt_list_union double ->
elt_list_member x s = true \/ elt_list_member x s' = true ->
elt_list_member x l = true.
Proof.
intro double.
functional induction (elt_list_union double) ; 
intros s s' x z l Hyp1 Hyp2 Hyp3 Hyp4 Hyp5 ; inversion Hyp3.
*  simpl in Hyp4. subst.
  decompose [or] Hyp5;[inversion H|assumption].
*  simpl in Hyp4. subst.
  decompose [or] Hyp5;[assumption|inversion H].
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  assert(Inv_elt_list z (elt_list_union (q1, Cons min2 max2 q2)))
    as Hyp6 by
      (apply elt_list_union_inv with (double:=(q1,Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2);
      (Inv_monotony (max1+2)||assumption||reflexivity)).
  rewrite <- decompose_elt_list_member with (y:=z).
    - rewrite <- decompose_elt_list_member with (y:=z) in Hyp5.
      + decompose [or] Hyp5;
            (tauto||right;apply IHe with (z:=z) (l:=elt_list_union (q1, Cons min2 max2 q2)) (s:=q1) (s':=Cons min2 max2 q2);
            (Inv_monotony (max1+2)||Zbool_constructor||reflexivity||right)).
      + assumption.
   - constructor ; try lia.
     apply elt_list_union_inv with (double:=(q1,Cons min2 max2 q2)) 
        (s:=q1) (s':=Cons min2 max2 q2);
           (reflexivity||assumption||Zbool_constructor).
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  apply decompose_elt_list_member with (y:=z).
     - constructor. lia. lia. Zbool_lia.
       apply elt_list_union_inv with (double:=(Cons min1 max1 q1, q2)) (s':=q2) (s:=Cons min1 max1 q1);(Zbool_constructor||assumption||reflexivity).     
     - decompose [or] Hyp5.
         + right. apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2);
             (assumption||Inv_monotony (max2+2)||reflexivity||tauto).
         + apply decompose_elt_list_member with (y:=z) in H; try Zbool_constructor. 
           decompose [or] H.
              tauto. 
              right. apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1,q2)) (s':=q2) (s:=Cons min1 max1 q1) ;
              (Zbool_constructor||Inv_monotony (max2+2)||reflexivity||right).
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.             
  assert(Z.min min1 min2 = min1 \/ Z.min min1 min2 = min2) as Hyp6 
    by (apply Zmin_irreducible with (n:=min1) (m:=min2)).
    decompose [or] Hyp6;rewrite H;rewrite H in IHe.
      - decompose [or] Hyp5.
         + apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1, q2))
           (s:=Cons min1 max1 q1) (s':=q2);
             (assumption||Inv_monotony (max2+2)||reflexivity||tauto).
         + apply decompose_elt_list_member with (y:=z) in H0; try assumption.
           decompose [or] H0.
               apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2);
             (assumption||Inv_monotony (max2+2)||reflexivity||left;apply decompose_elt_list_member with (y:=z)). 
               assumption. left. Zminmax_lia;lia.
              apply IHe with (z:=z) (l:=elt_list_union (Cons min1 max1 q1, q2)) (s:=Cons min1 max1 q1) (s':=q2);
             (assumption||Inv_monotony (max2+2)||reflexivity||tauto).         
      - decompose [or] Hyp5;
           apply IHe with (z:=z) (l:=elt_list_union (Cons min2 max1 q1, q2))
           (s:=Cons min2 max1 q1) (s':=q2);
           try (assumption||Inv_monotony (max2+2)||reflexivity||Zbool_constructor).
           + apply decompose_elt_list_member with (y:=z) in H0; 
             try assumption.
             decompose [or] H0;
                left;
                apply decompose_elt_list_member with (y:=z);
                    (Zbool_constructor||left;Zminmax_lia;lia||tauto).                
           + apply decompose_elt_list_member with (y:=z) in H0; 
             try assumption.
             decompose [or] H0;
                [left|tauto];
                apply decompose_elt_list_member with (y:=z);
                    (Zbool_constructor||left;Zminmax_lia;lia).
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst. 
  decompose [or] Hyp5;
      apply IHe with (z:=z) (s:=Cons min1 max2 q2) (s':=q1);
      try (assumption||Inv_monotony (max1+2)||reflexivity||Zbool_constructor);
      apply decompose_elt_list_member with (y:=z) in H; try assumption;
      decompose [or] H.
          + left. apply decompose_elt_list_member with (y:=z).
            Zbool_constructor. left. Zbool_lia;lia.
          + tauto.
          + left. apply decompose_elt_list_member with (y:=z).
            Zbool_constructor. left. Zbool_lia;lia.
          + left. apply decompose_elt_list_member with (y:=z).
            Zbool_constructor. tauto.
* rewrite <- H0 in Hyp1. rewrite <- H1 in Hyp2.
  inversion Hyp1. inversion Hyp2. subst.
  decompose [or] Hyp5;
      apply IHe with (z:=z) (s:=Cons min2 max2 q2) (s':=q1);
      try (assumption||Inv_monotony (max1+2)||reflexivity||Zbool_constructor);
      apply decompose_elt_list_member with (y:=z) in H; try assumption;
      decompose [or] H.
          + left. apply decompose_elt_list_member with (y:=z).
            Zbool_constructor. left. Zbool_lia;lia.
          + tauto.
Qed.

Theorem elt_list_union_spec : forall double s s' x z l,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
l = elt_list_union double ->
elt_list_member x (l) = true <-> elt_list_member x s = true \/ elt_list_member x s' = true.
Proof.
intros.
split.
  + apply elt_list_union_spec_1 with (double:=double) (z:=z);assumption.
  + apply elt_list_union_spec_2_3 with (double:=double) (z:=z);assumption.
Qed.
  
Lemma elt_list_union_not_Nil : forall double a b l l' x y,
Inv_elt_list x l ->
Inv_elt_list y l' ->
double = (Cons a b l, l') ->
elt_list_union double <> Nil.
Proof.
intro double.
apply elt_list_union_ind; simpl; intros.
* inversion H1.
* inversion H1. subst. simpl.
  intro HypFalse;inversion HypFalse.
* intro HypFalse;inversion HypFalse.
* intro HypFalse;inversion HypFalse.
* inversion H2. subst. 
  apply H with (a0:=Z.min a min2) (b0:=b) (l0:=l) (l':=q2) (x:=x) (y:=max2+2).
    + assumption.
    + inversion H1. assumption.
    + reflexivity.
* inversion H2. subst. 
  apply H with (a0:=a) (b:=max2) (l0:=q2) (l':=l) (x:=max2+2) (y:=x).
    + inversion H1. assumption.
    + assumption.
    + reflexivity.
* inversion H2. subst. 
  apply H with (a:=min2) (b:=max2) (l0:=q2) (l':=l) (x:=max2+2) (y:=x).
    + inversion H1. assumption.
    + assumption.
    + reflexivity.
Qed.
  
Theorem union_spec : forall s s' x, 
Inv_t s ->
Inv_t s' ->
member x (union s s') = true <-> member x s = true \/ member x s' = true.
Proof.
intros s s' x. intros Hs Hs'.
unfold Inv_t in Hs. decompose [and] Hs. clear Hs.
unfold Inv_t in Hs'. decompose [and] Hs'. clear Hs'.
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s)) as hyp1 by  
  (apply inv_elt_list_monoton with (y:=min s);auto; apply Z.le_min_l).
assert (Inv_elt_list (Z.min (min s) (min s'))
  (domain s')) as hyp2 by  
  (apply inv_elt_list_monoton with (y:=min s');auto; apply Z.le_min_r).
apply union_ind ; intros; unfold member.
  + simpl. split ; intro Hyp. inversion Hyp.
    apply elt_list_union_spec_2_3 with (z := Z.min (min s) (min s'))
      (l:=Nil) (double := (domain s, domain s')) in Hyp ; auto.
  + replace (domain
     {|domain := l;
     size := process_size l;
     structure.max := process_max l;
     min := get_min l min_int |}) with l ; auto.
     apply elt_list_union_spec 
          with (double:=(domain s, domain s')) (z:=(Z.min (min s) (min s'))) ; auto.
Qed.

(************************************)
(** * Some lemmas                   *)
(************************************)
Lemma elt_list_member_decreasing_true : forall x a b c d t y,
Inv_elt_list y (Cons a b t) ->
Inv_elt_list y (Cons c d t) ->
c <= a ->
b <= d ->
elt_list_member x (Cons a b t) = true ->
elt_list_member x (Cons c d t) = true.
Proof.
intros x a b c d t y Hyp1 Hyp2 Hyp3 Hyp4 Hyp5.
apply decompose_elt_list_member with (y:=y) in Hyp5.
 * decompose [or] Hyp5;
    apply decompose_elt_list_member with (y:=y);
       auto; (left;lia||tauto).
 * assumption.
Qed.

Lemma switch_in_elt_list_union : forall double a b q1 q2 x,
Inv_elt_list x (Cons a b q1) ->
Inv_elt_list x (Cons a b q2) ->
double = (Cons a b q1, q2) ->
elt_list_union double =
elt_list_union (q1, Cons a b q2).
Proof.
intros l1_l2 .
apply elt_list_union_ind; simpl; intros.
* inversion H1.
* inversion H1. subst. simpl.
  rewrite elt_list_union_equation.
  case_eq(q1);intros.
    - auto.
    - rewrite H2 in H. inversion H. inversion H9.
      assert(z0 <? a - 1 = false) as Hyp1 
        by (Zbool_lia_goal;lia);rewrite Hyp1.
      assert(b <? z - 1 = true) as Hyp2 
        by (Zbool_lia_goal;lia);rewrite Hyp2.
      rewrite elt_list_union_equation. simpl. reflexivity.
* inversion H2. subst.
  symmetry.
  rewrite elt_list_union_equation.
  case_eq(q0);intros.
    - auto.
    - rewrite H3 in H0. inversion H0. inversion H10.
      assert(z0 <? a - 1 = false) as Hyp1 
        by (Zbool_lia_goal;lia);rewrite Hyp1.
      assert(b <? z - 1 = true) as Hyp2 
        by (Zbool_lia_goal;lia);rewrite Hyp2.
      reflexivity.
* inversion H2. subst.
  inversion H1. inversion H9. Zbool_lia;lia.
* inversion H2. subst.
  inversion H1. inversion H9. Zbool_lia;lia.
* inversion H2. subst.
  inversion H1. inversion H9. Zbool_lia;lia.
* inversion H2. subst.
  inversion H1. inversion H9. Zbool_lia;lia.
Qed.

Lemma elt_list_union_symmetry : forall double s s' z,
Inv_elt_list z s ->
Inv_elt_list z s' ->
double = (s,s') ->
elt_list_union (s',s) = elt_list_union double.
Proof.
intros l1_l2 .
apply elt_list_union_ind; simpl; intros.
* inversion H1. rewrite e. rewrite elt_list_union_equation. 
  case_eq(s');intros;subst;reflexivity.
* inversion H1. rewrite e. rewrite elt_list_union_equation. 
  simpl. auto.
* inversion H2. subst. rewrite elt_list_union_equation.
  assert(max2 <? min1 - 1 = false).
  Zbool_lia_goal;Zbool_lia. inversion H0. inversion H1.
  lia. rewrite H3. rewrite e0.
  rewrite H with (s':=Cons min2 max2 q2) (s:=q1) (z:=z).
  reflexivity. inversion H0. Inv_monotony (max1+2). assumption.
  reflexivity.
* inversion H2. subst. rewrite elt_list_union_equation.
  assert(max2 <? min1 - 1 = true).
  Zbool_lia_goal;Zbool_lia. inversion H0. inversion H1.
  lia. rewrite H3.
  rewrite H with (s':=q2) (s:=Cons min1 max1 q1) (z:=z).
  reflexivity. assumption. inversion H1. Inv_monotony (max2+2).
  reflexivity.
* inversion H2. subst. rewrite elt_list_union_equation.
  rewrite e0. rewrite e1. 
  case_eq(max2 >? max1);intro Hyp1.
    + Zbool_lia;lia.
    + case_eq(min2 <=? min1);intro;Zminmax_lia_goal;reflexivity.
* inversion H2. subst. rewrite elt_list_union_equation.
  rewrite e1. rewrite e0.
  case_eq(max2 >? max1);intro Hyp1.
    + Zminmax_lia_goal. reflexivity.
    + case_eq(min2 <=? min1);intro.
       - Zbool_lia. 
         assert(max1 = max2) as Hyp2 by (lia). rewrite Hyp2.
         assert(min2 = min1) as Hyp3 by (lia). rewrite Hyp3.       
       assert(elt_list_union (q1, Cons min1 max2 q2) = elt_list_union (Cons min1 max2 q2, q1)).
       apply H with (z:=z).
          rewrite <- Hyp3. assumption.
          inversion H0. Inv_monotony (max1+2).
          reflexivity.          
          rewrite switch_in_elt_list_union with (a:=min1) (b:=max2) (q1:=q1) (q2:=q2) (x:=z).
          assumption.
          rewrite <- Hyp2. assumption.
          rewrite <- Hyp3. assumption.
          reflexivity.
        - Zbool_lia. 
         assert(max1 = max2) as Hyp2 by (lia). rewrite Hyp2.       
       assert(elt_list_union (q1, Cons min1 max2 q2) = elt_list_union (Cons min1 max2 q2, q1)). inversion H1. inversion H0.
       apply H with (z:=z).
           Zbool_constructor.
           Inv_monotony (max1+2).
           reflexivity.          
          rewrite switch_in_elt_list_union with (a:=min1) (b:=max2) (q1:=q1) (q2:=q2) (x:=z).
          assumption.
          rewrite <- Hyp2. assumption.
          inversion H1. inversion H0. Zbool_constructor. reflexivity.
* inversion H2. subst. rewrite elt_list_union_equation.
  rewrite e1. rewrite e0.
  case_eq(max2 >? max1);intro Hyp1.
    + Zminmax_lia_goal. reflexivity.
    + case_eq(min2 <=? min1);intro.
       - Zbool_lia. 
         assert(max1 = max2) as Hyp2 by (lia). rewrite Hyp2.       
       assert(elt_list_union (q1, Cons min2 max2 q2) = elt_list_union (Cons min2 max2 q2, q1)).
       apply H with (z:=z).
          assumption.
          inversion H0. Inv_monotony (max1+2).
          reflexivity.         
          rewrite switch_in_elt_list_union with (a:=min2) (b:=max2) (q1:=q1) (q2:=q2) (x:=z). 
          assumption.
          inversion H1. inversion H0. 
          constructor;[assumption|lia|Inv_monotony (max1+2)].
          assumption.
          reflexivity.
        - Zbool_lia;lia.
Qed.
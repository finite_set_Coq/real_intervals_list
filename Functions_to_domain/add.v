Require Import R.Prelude.structure R.FBool.member R.CD.create R.FBool.is_empty R.CD.empty R.FDomain.union R.Prelude.tactics.
Require Import List ZArith Recdef Bool.Bool Lia.
Import ListNotations.
Open Scope Z_scope.

(************************************)
(** * Definition                    *)
(************************************)
Definition add (x : Z) (d :t) := (union (create (cons x nil)) d).

(************************************)
(** * Preservation of the invariant *)
(************************************)
Theorem add_inv : 
forall d x, Inv_t d -> Inv_t (add x d).
Proof.
intros d x Hyp. unfold add. 
apply union_inv.
  + apply create_inv. 
  + assumption.
Qed.

(************************************)
(** * Specification                 *)
(************************************)
Lemma equiv_elt_list_union_Nil : forall l l' y z,
Inv_elt_list y l ->
Inv_elt_list z l' ->
elt_list_union (l, l') = Nil <->
l = Nil /\ l' = Nil.
Proof.
intros l l' y z Hyp1 Hyp2.
split;intro Hyp3.
  * case_eq(l).
      + intro Hyp4. rewrite Hyp4 in Hyp3.
        rewrite elt_list_union_equation in Hyp3.
        simpl in Hyp3. auto.
      + intros z0 z1 e Hyp4. rewrite Hyp4 in Hyp3. 
        rewrite Hyp4 in Hyp1. inversion Hyp1.
  assert(elt_list_union (Cons z0 z1 e, l') <> Nil).
  apply elt_list_union_not_Nil with (a:=z0) (b:=z1) (l:=e) (l':=l') (x:=z1+2) (y:=z);(assumption||reflexivity). contradiction.
  * decompose [and] Hyp3. rewrite H. rewrite H0.
    rewrite elt_list_union_equation. auto.
Qed.

Theorem add_spec_1 : forall x y d,
Inv_t d ->
x = y ->
member y (add x d) = true.
Proof.
intros x y d Hyp1 Hyp2.
unfold add. unfold member.
apply union_ind.
  + intro HypNil.
    unfold Inv_t in Hyp1. decompose [and] Hyp1. clear Hyp1.
    assert(Inv_t (create [x])) as Hyp3 by (apply create_inv). 
    unfold Inv_t in Hyp3. decompose [and] Hyp3. clear Hyp3.

    rewrite equiv_elt_list_union_Nil with (y:=x) (z:=min d) in HypNil.
       - decompose [and] HypNil. inversion H6.
       - assumption.
       - assumption.
  + intros z z0 e Hyp3 l. unfold domain.

    rewrite elt_list_union_spec with (double:=(domain (create [x]), domain d)) (s:=domain (create [x])) (s':=domain d) (x:=y) (z:=Z.min x (min d)) (l:=l).
       - left. subst. simpl. 
         assert(y <=? y = true) as Hyp5 
           by (Zbool_lia_goal;lia);rewrite Hyp5. 
         Zbool_lia_goal;Zbool_lia;lia.
       - case_eq(x <=? (min d));intro Hyp4;
            Zminmax_lia_goal; simpl; constructor;(lia||constructor).
       - unfold Inv_t in Hyp1. 
         case_eq(x <=? (min d));intro Hyp4.
            * Zminmax_lia_goal. 
              apply inv_elt_list_monoton with (y:=min d). 
              tauto. Zbool_lia;lia.
            * Zminmax_lia_goal. tauto.
       - reflexivity.
       - unfold l. auto.
Qed.
  
Theorem add_spec_2 : forall x y d,
Inv_t d ->
member y d = true -> 
member y (add x d) = true.
Proof.
intros x y d Hyp1 Hyp2.
unfold add. unfold member.
apply union_ind.
  + intro HypNil.
    unfold Inv_t in Hyp1. decompose [and] Hyp1.
    assert(Inv_t (create [x])) as Hyp3 by (apply create_inv). 
    unfold Inv_t in Hyp3. decompose [and] Hyp3. clear Hyp3.

    rewrite equiv_elt_list_union_Nil with (y:=x) (z:=min d) in HypNil.
       - decompose [and] HypNil. apply equiv_empty_Nil in H8.
         rewrite H8 in Hyp2. inversion Hyp2. auto.
       - assumption.
       - assumption.
  + intros z z0 e Hyp3 l. unfold domain.

    rewrite elt_list_union_spec with (double:=(domain (create [x]), domain d)) (s:=domain (create [x])) (s':=domain d) (x:=y) (z:=Z.min x (min d)) (l:=l).
       - right. assumption.
       - case_eq(x <=? (min d));intro Hyp4;
            Zminmax_lia_goal; simpl; constructor;(lia||constructor).
       - unfold Inv_t in Hyp1. 
         case_eq(x <=? (min d));intro Hyp4.
            * Zminmax_lia_goal. 
              apply inv_elt_list_monoton with (y:=min d). 
              tauto. Zbool_lia;lia.
            * Zminmax_lia_goal. tauto.
       - reflexivity.
       - unfold l. auto.
Qed.

Theorem add_spec_3 : forall x y d,
Inv_t d ->
x <> y ->
member y (add x d) = true ->
member y d = true.
Proof.
intros x y d Hyp1 Hyp2.
unfold add. unfold member.
apply union_ind.
  + intros HypNil Hypempty. inversion Hypempty.
  + intros z z0 e Hyp3 l Hyp4. unfold domain in Hyp4.
    rewrite elt_list_union_spec with (double:=(domain (create [x]), domain d)) (s:=domain (create [x])) (s':=domain d) (x:=y) (z:=Z.min x (min d)) (l:=l) in Hyp4.
       - decompose [or] Hyp4.
            * simpl in H. case_eq(y <=? x);intro Hyp5;rewrite Hyp5 in H;
                 [Zbool_lia;lia|inversion H]. 
            * assumption.
       - case_eq(x <=? (min d));intro Hyp5;
            Zminmax_lia_goal ; simpl; constructor;(lia||constructor).
       - unfold Inv_t in Hyp1. 
         case_eq(x <=? (min d));intro Hyp5.
            * Zminmax_lia_goal. 
              apply inv_elt_list_monoton with (y:=min d). 
              tauto. Zbool_lia;lia.
            * Zminmax_lia_goal. tauto.
       - reflexivity.
       - unfold l. auto.
Qed.